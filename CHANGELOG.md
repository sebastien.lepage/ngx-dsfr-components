# @edugouvfr/ngx-dsfr - Changelog

## 1.12.2 (2024-10-08)

- fix(card/tile): régression de la taille des titres cliquables
- fix(button): possibilité de définir l'attribut aria-pressed (accessibilité)
- fix(buttons-group): effet de bord de la surcharge du CSS pour la modal sur les dsfr-btns-group
- fix(form): affichage du conteneur de messages même vide et conditionnement de aria-describedby (accessibilité)
- fix(translate): correction documentation, le label de la langue ne doit pas contenir le code
- chore(i18n): permettre d'étendre les bundles d'internationalisation (notamment pour les extensions)

## 1.11.9 (2024-10-03)

- fix(buttons-group): effet de bord de la surcharge du CSS pour la modal sur les dsfr-btns-group

## 1.12.1 (2024-09-23)

- fix(card): régression interprétation du HTML sur l'input `heading`
- fix(consent-banner): l'attribut title du lien des "données personnelles et cookies" concatène le label et le target
- fix(consent-banner): les boutons radio 'consent-all' partagent désormais le même name (accessibilité)
- fix(consent-banner): application de la traduction du lien des données personnelles dans la modale
- fix(radio/radio-rich): les boutons radio sont désormais désactivables unitairement
- fix(share): application de la traduction du lien des données personnelles
- fix(stepper): impossible de changer le niveau de titre
- fix(tile): régression projection du slot `heading`

## 1.11.8 (2024-09-23)

- fix(stepper): impossible de changer le niveau de titre

## 1.12 (2024-09-13)

- feat: montée de version DSFR 1.12.1
- feat(alert): ajout du slot `message`
- feat(card/tile): gestion de la classe `enlargeButton`
- feat(card): propriété `enlargeLink=true` par défaut
- feat(cursor): ajout de la propriété `step`
- feat(header): permettre d'utiliser un type `DsfrNavigation` sur le lien du logo
- feat(i18n): possibilité d'ajouter un fichier de traduction de la librairie
- feat(input/select/password/upload): ajout de la propriété `labelSrOnly`
- feat(notice): évolutions suivant le DSFR 1.12 avec ajout des variations
- feat(radio): ajout de la propriété `legendSrOnly`
- feat(tab): ajout de la propriété `disabled`
- feat(table): nouveau composant de tableau avec pagination, sélection et tri
- feat(toggle): ajout des messages d'erreur et de validation
- fix(backtotop): l'ancrage fonctionne correctement
- fix(card/tile): `linkTarget` n'est pas utilisé dans le téléchargement
- fix(content): structure html modifiée pour respecter le DSFR
- fix(footer): changement d'ordres des liens institutionnels & ajout de l'attribut `title`
- fix(form): bloc message affiché seulement si il y a un message de renseigné
- fix(range): correctif accessibilité, le labelled-by de l'input ne pointait vers aucun label
- fix(tag): correctif accessibilité, en mode cliquable, le `disabled` est correctement propagé aux enfants du tag

## 1.11.7 (2024-09-11)

- fix(consent-banner): le texte du lien de la bannière de consentement se met bien à jour au changement de langage
- fix(footer): ajout de traductions manquante
- fix(header): slot `headerTools` et `headerToolsMobile` documentation manquante et régression mode mobile
- fix(modal): en mode mobile, les boutons prennent toute la largeur
- fix(stepper): ajout de traductions manquante

## 1.11.6 (2024-08-21)

- fix(header): correction de la fonctionnalité translate du menu en mode mobile
- fix(header): générer un identifiant unique pour la navigation `navigationId` et possibilité de le redéfinir
- fix(footer): coquille dans la traduction de "nos partenaires"
- fix(form-tel): formatage inattendu si le _pattern_ est spécifié

## 1.11.5 (2024-07-25)

- fix(link/card/tile/content/tag): fix accessibilité (navigation clavier), dépréciation de `routerLink` au profit de `routePath`

## 1.11.4 (2024-07-15)

- fix(segmented-control): correction des labels qui n'étaient pas pris en compte

## 1.11.3 (2024-07-08)

- fix(alert): problème d'internationalisation sur l'input `heading`
- fix(alert): la projection de contenu par défaut (titre) ne fonctionne pas

## 1.11.2 (2024-06-14)

- fix(address): correction syntaxe ngModel non compatible Angular 18
- fix(input): régression affichage du label en tant que slot
- fix(name): correction syntaxe ngModel non compatible Angular 18
- fix(tabs): (avec routes) désélection de l'onglet si navigation vers une sous-route dans l'onglet
- fix(tabs): ajout propriété manquante `routerLinkActiveOptions` sur interface `DsfrTabRoute`

## 1.11.1 (2024-06-03)

- fix(radio, radio-rich): navigation clavier non conforme (attribut name non répercuté sur les boutons radios)

## 1.10.4 (2024-06-03)

- fix(radio, radio-rich): navigation clavier non conforme (attribut name non répercuté sur les boutons radios)

## 1.9.9 (2024-06-03)

- fix(radio, radio-rich): navigation clavier non conforme (attribut name non répercuté sur les boutons radios)

## 1.11.0 (2024-05-31)

- break(chore): version minimale requise angular 16
- feat(input): événement `patternValueChange` si un `pattern` est défini et que la valeur change
- feat(input): niveau de message : `info`, `error` ou `valid`
- feat(skiplinks): ajout `DsfrAnchorLink#route` & ajout de l'output `linkSelect`
- feat(tag): possibilité de rédéfinir l'attribut `ariaLabel`
- fix(badge): si `severity` n'est pas renseignée, autoriser de passer une classe sytème avec `customClass`
- fix(checkbox): correctif accessiblité état indéterminé de la checkbox
- fix(footer): changement de l'url gouvernement.fr en info.gouv.fr
- fix(input): `placeHolder` déprécié en faveur de `placeholder`
- fix(input): la valeur `ariaExpanded` à false n'est pas valorisée
- fix(pagination): correctif accesibilité remplacement des `<a>` par des `<button>`
- fix(pagination): correctif accessibilité, ajout de la propriété `ariaLabel` pour contextualiser la pagination
- doc(input): commentaires sur les propriétés d'accessibilité revus
- chore: export des mixins sass
- chore: migration storybook 8

## 1.10.3 (2024-05-07)

- fix(date): report adapté du hotfix 1.9.8 relatif à la mauvaise gestion du required

## 1.9.8 (2024-05-07)

- fix(date): `required` non géré correctement

## 1.10.2 (2024-04-02)

- fix(alert): composant hôte non supprimé du DOM à la fermeture de l'alerte
- fix(alert): absence de notification à la fermeture (ajout output `conceal`)
- fix: intégration des correctifs 1.9.6 et 1.9.7

## 1.9.7 (2024-03-29)

- fix(header): `tooltipMessage` non traité sur les liens en mode 'button' des `headerToolsLinks`

## 1.9.6 (2024-03-28)

- fix(upload): pilotage via ReactiveForm incomplet (`setValue` et `reset` impossibles)
- fix(upload): impossible de ré-initialiser le champ après une sélection (ajout d'une fonction reset)

## 1.10.1 (2024-03-22)

- fix: mauvaise version de la dépendance `@gouvfr/dsfr` dans le package.json (1.11.0 au lieu de 1.11.2)

## 1.10.0 (2024-03-19)

- feat: montée de version DSFR 1.11.2
- feat(accordions-group): ajout de la propriété `ungroup`
- feat(date): accepte une date ISO courte telle que `2024-08-31`
- feat(segmented-control): ajout du composant `dsfr-segmented-control`
- feat(range): ajout du composant `dsfr-range`
- fix(accordions-group): retrait des ul li
- fix(card/tile): la hauteur du contenu du composant est forcée à la hauteur de son conteneur
- fix(date): les erreurs internes n'invalident pas le form parent
- fix(header): traductions manquantes sur boutons 'Fermer'
- fix(header): affichage lien rubrique mega-menu
- fix(select): fix accessibilité, ajout attribut `aria-selected` sur les options

## 1.9.5 (2024-03-15)

- fix(breadcrumb): régression sur la prise en charge de `routerLinkActiveOptions` (depuis 1.9.0)
- fix(radio): il manque les attributs disabled natifs sur les inputs radio lorsque le composant est désactivé
- fix(tabs): en mode 'routes' la navigation clavier n'est pas opérante

## 1.9.4 (2024-02-27)

- fix(tabs): dysfonctionnements du mode 'routes' en contexte dynamique et asynchrone (cf. doc storybook pour les impacts)

## 1.9.3 (2024-02-06)

- fix(modal): ajout des propriétés `disabled`, `type` et `tooltipMessage` sur les boutons d'actions

## 1.9.2 (2024-01-31)

- fix: le contrôle sur le nom des classes CSS RemixIcon empêche l'intégration d'icônes custom. (`DsfrIcon` déprécié, utiliser un `string`)
- fix(card/tile): l'attribut `downloadDirect` positionné à `false` empêche l'affichage du téléchargement
- fix(select): erreur à l'initialisation du select si la liste d'options est `undefined`
- fix(tile): la valeur de l'attribut `downloadDirect` n'est pas valorisée

## 1.8.3 (2024-01-31)

- fix(buttons-group): ne pas déclencher l'évènement click lors du clic légèrement à côté d'un bouton `disabled`
- fix(select): problème de performance lors de l'affichage d'un grand nombre d'options sur Firefox
- fix(select): suppression de la conversion automatique des values en string

## 1.9.1 (2024-01-23)

- feat(follow/share): ajout de la constante `twitter-x` qui remplace la constante `twitter`
- fix(table): nullInjectorError: no provider for KeyValuePipe

## 1.9.0 (2024-01-22)

- feat: permettre de configurer globalement la propriété `artworkDirPath`
- feat(alert): nouvel input `ariaRoleValue` permettant de positionner un rôle `status` plutôt que `alert`
- feat(badge): un libellé trop long de badge est coupé par `...` s'il dépasse la largeur du conteneur
- feat(display): ajout de l'output `displayChange` sur changement de mode
- feat(display): dépréciation de `pictoPath` en faveur de `artworkDirPath`
- feat(footer): dépréciation de `pictoPath` en faveur de `artworkDirPath`
- feat(header): dépréciation de `pictoPath` en faveur de `artworkDirPath`
- feat(header): possibilité de rendre les catégories du mega-menu non-cliquables
- feat(response): dépréciation de `pictoPath` en faveur de `artworkDirPath`
- feat(select): ajout de la propriété `compareWith` pour utiliser des objets comme valeurs des options
- feat(summary): possibilité d'avoir des sous-niveaux
- feat(summary): implémentation de `routerLink`
- feat(table): permettre de conserver l'ordre initial des colonnes
- feat(tabs): possibilité d'utiliser `router-outlet`
- feat(toggles-group): les toggles sont placés dans un fieldset
- feat(upload): ajout de la propriété `accept`, filtre pour les types de fichiers acceptés
- fix(badges-group): la taille d'un groupe de badges doit être définie au niveau du groupe
- fix(buttons-group): ne pas déclencher l'évènement click lors du clic légèrement à côté d'un bouton `disabled`
- fix(consent): i18n libellé erroné
- fix(header): la propriété `customClass` des items d'accès rapide n'est pas prise en compte
- fix(header): mauvais alignement vertical des liens rapides si utilisation de `display=true`
- fix(input): erreur js si usage dans une modale
- fix(select): suppression de la conversion automatique des values en string
- fix(select): problème de performance lors de l'affichage d'un grand nombre d'options sur Firefox
- fix(select): libellé `optgroup` tronqué sur Firefox
- fix(tile): dépréciation de `detail` en faveur de `detailBottom` pour homogénéiser avec la carte
- doc(card/tile): correction documentation detail carte et tuile de téléchargement
- doc(display): fix les modales non visibles dans la doc de Storybook

## 1.8.2 (2023-12-21)

- fix(select): retour arrière sur les modifications effectuées en 1.8.0 (désélection possible si champ non required)

## 1.8.1 (2023-12-18)

- fix(tabs): ExpressionChangedAfterItHasBeenCheckedError

## 1.8.0 (2023-12-12)

- feat(consent-banner): implémentation de `routerLink`
- feat(modal): ajout output `disclose` permettant de se mettre à l'écoute de l'ouverture de la modale
- feat(modal): ajout input `controlMode` afin de pallier l'absence de bouton d'ouverture
- feat(select): si le champ n'est pas requis, le placeholder à nouveau disponible
- fix(accordion): la propriété `expanded` peut se désynchroniser de l'état réel du DOM (intéraction script DSFR)
- fix(consent-banner): `heading` remplace `title` (déprécié)
- fix(consent-banner): `rgpdLink` remplace `rgpdUrl` (déprécié)
- fix(download): utiliser les options régionales pour l'affichage du détail
- fix(header): désynchronisation des tools links du menu mobile lors d'une mise à jour dynamique
- fix(header): erreurs 404 à l'initialisation du composant si le `display` n'est pas utilisé
- fix(header/searchbar): fix accessibilité searchbar (attribut aria-describedby)
- fix(input): suppression de l'attribut `aria-labelledby` redondant avec le label
- fix(modal): suppression du `role="dialog"` sur la balise `dialog`
- fix(tile): gestion de l'attribut `aria-label` sur la tuile cliquable.
- fix(DsfrHeadingLevel): `NONE` est déprécié au profit de `undefined`
- fix(DfsrLink): `target` est déprécié au profit de `linkTarget`
- chore: compatible `"strictNullChecks": true`
- chore: migration Angular 15
- doc: amélioration documentation Storybook, visibilité des propriétés dépréciées

## 1.7.1 (2023-11-13)

- fix(modal): affichage intempestif de la modale lorsqu'elle est embarquée dans un formulaire

## 1.7.0 (2023-11-13)

- feat(content): ajout de l'événement `linkSelect`
- feat(date): ajout de la propriété `resetOnNull` (réinitialise le composant si le modèle est valorisé à `null`)
- feat(date): ajout de la propriété `autocomplete` permettant de conditionner l'usage de l'autocomplétion
- feat(DsfrLink): ajout des propriétés `ariaLabel`, `ariaControls` et `mode`
- feat(DsfrNavigation): ajout de la propriété `routerLinkActiveOptions`
- feat(follow): ajout des propriétés `emailError` et `registered`
- feat(footer): ajout de l'évènement `mandatoryLinkSelect`
- feat(header): possibilité d'utiliser des boutons dans le lien du header
- feat(tabs): ajout de la propriété `fullViewport`, 100% largeur du viewport en mobile si vrai
- feat(tooltip): nouvelle directive
- feat(tooltip-button): nouveau composant
- fix(content): `tooltipMessage` est aussi utilisé pour les images
- fix(date): un message d'erreur est affiché à l'initialisation si le modèle est null
- fix(date): la propriété `id` est dépréciée car elle n'est plus utilisée
- fix(date): la date retournée par le composant ne devrait pas tenir compte du fuseau horaire courant
- fix(header): langue courante de l'interface `translate` non optionnelle
- fix(modal): perte du focus sur le bouton à la fermeture de la modale (dans le header)
- fix(pagination): moins de pages affichée, 1 seule ligne en mode mobile
- fix(pagination): le composant `pagination` remplace `previous-page` (déprécié)
- fix(pagination): l'événement `pageSelect` remplace `pageSelectEvent` (déprécié)
- fix(pagination): l'événement `backSelect` remplace `backEvent` (déprécié)
- fix(tab): ajout de la classe `fr-tabs__panel--selected`
- fix(tabs): la modification dynamique de l'input `selectedTabIndex` est sans effet
- fix(tabs): valeur de `tabindex` inversée
- fix(i18n): traductions manquantes
- doc(checkbox): ajout de la story _'Reactive Form'_
- doc(content): stories images et vidéos dans le même fichier
- chore: la version minimale requise de NodeJS est 18.18.2 (npm 9.8.1)
- chore: suppression du type `ID`, alias du type `string`
- chore(storybook): migration 7.5.1

## 1.6.2 (2023-10-25)

- fix(date): les erreurs de validation internes du composant n'affectent pas le statut du FormControl
- fix(modal): la modale ne peut être fermée manuellement après une ouverture programmatique (régression DSFR 1.10)

## 1.6.1 (2023-10-24)

- fix(card|tile): pas d'événement émis en mode download + route
- fix(download): passe en mode carte si lien non renseigné
- fix(modal): la méthode `open()` ne fonctionne plus si aucun bouton n'est lié à la modale (régression DSFR 1.10)
- fix(modal): `aria-labelledby` référence un identifiant `undefined`
- fix(tag): les changements sur l'input `selected` ne sont pas pris en compte

## 1.6.0 (2023-10-10)

- feat(content): implémentation de `routerLink`
- feat(content): ajout de la propriété `transcriptionHeading`
- feat(content): ajout d'un slot `transcription` pour insérer le composant de transcription
- feat(form-email): par défaut `autocomplete="email"`, `autocorrect="false"`
- feat(form-input): ajout de la propriété `autoCorrect`
- feat(form-password): ajout de la propriété `autocomplete="current-password"` par défaut
- feat(form-radio-rich): ajout du support des pictogrammes
- feat(link): ajout de la propriété `linkId`
- feat(login): nouveau modèle de page
- fix(buttons): type `button` par défaut, sauf exception, pour tous les boutons des composants
- fix(card/tile): permettre la gestion programmatique du download (via input route)
- fix(content): le slot pour les images svg est nommé `svg`, le slot par défaut est déprécié
- fix(content): `linkLabel` remplace `transcriptionLabel` (déprécié)
- fix(content): `transcriptionContent` remplace `transcription` (déprécié)
- fix(display): erreurs 404 au primo-chargement des icônes
- fix(form-checkbox): la `div` messages devrait toujours être présente
- fix(form-password): `aria-describedby` présent même en l'absence de message
- fix(form-password): `undefined` dans les `id`
- fix(form-password): quelques messages non traduits
- fix(franceconnect): pas de traduction 'en'
- fix(transcription): traduction du bouton 'Fermer'
- fix(response): `DsfrPageResponseModule` est déprécié en faveur de `DsfrResponseModule`
- fix(response): `DsfrPageResponseComponent` est déprécié en faveur de `DsfrResponseComponent`
- fix(response): le sélecteur `dsfr-page-response` est déprécié en faveur de `dsfr-response`
- fix(tag): erreur TS2416 si `strictNullChecks` est activé côté client
- fix(tag): l'attribut `id` est présent avec pour valeur 'null' lorsque la propriété n'est pas renseignée
- chore(storybook): migration 7.4.5

## 1.5.0 (2023-10-02)

- feat: montée de version DSFR 1.10.1
- feat(card): ajout de la version carte de téléchargement (DSFR 1.10)
- feat(card): ajout de la propriété `externalLink`
- feat(card): ajout de la propriété `disabled`
- feat(form-checkbox): ajout de `aria-checked`
- feat(form-input): ajout de la propriété `inputWrapMode`
- feat(link): ajout de la propriété `disabled`
- feat(link): `linkTarget` remplace `targetLink` (déprécié)
- feat(modal): ajout de la propriété `headingLevel`
- feat(patterns/name): ajout de l'évènement `deleteFirstNameSelect`.
- feat(tag): implémentation de routerLink (pour un mode `clickable`)
- fix(card): la classe du lien ne doit pas contenir `fr-link`
- feat(tile): ajout de la version tuile de téléchargement (DSFR 1.10)
- feat(tile): nouvelle version de la tuile (DSFR 1.10)
- feat(tile): ajout de la propriété `disabled`
- fix(date): disparition des erreurs affichées (#390)
- fix(download): composant `dsfr-download` déprécié au profit des propriétés `download` de la `dsfr-tile` et de la `dsfr-card`
- fix(form): sur les composants de formulaire, dépréciation de l'input `id` en faveur de `inputId` (évite la collision avec l'attribut natif)
- fix(form-checkbox): valorisation de `aria-controls`
- fix(form-input): focus sur tags et non sur input
- fix(header): ajout de l'évènement linkSelect sur les liens du menu mobile en cas d'utilisation de l'attribut route
- fix(header): affichage d'un lien désactivé dans les menus en l'absence de route, routerLink et link (pas de href)
- fix(patterns/date): préfixe des attributs id, dépréciation de l'attribut name et suppression du label
- fix(tabs): entêtes non réactives aux modifications dynamiques
- fix(tag): ajout de l'attribut tagId et dépréciation de id pour éviter la collision avec l'attribut natif
- fix(tag): la classe du lien (tag cliquable) ne doit pas contenir `fr-link`
- fix(tag): les boutons des modes `deletable` et `selectable` ne doivent pas être de type `submit`
- fix(tile): la classe du lien ne doit pas contenir `fr-link`
- fix(translate): soumission d'un formulaire inattendu

## 1.4.0 (2023-09-06)

- feat(card): implémentation de routerLink
- feat(checkbox): ajout de la propriété `indeterminate`
- feat(checkbox): ajout de stories pour les groupes de checkbox
- feat(password): implémentation de routerLink pour le recovery
- feat(input): nouvelles propriétés pour l'accessibilité
- feat(tag): un événement de type `DsfrTagEvent` est émis lors de la sélection si l'input `id` est renseigné
- fix(upload): le nouveau sélecteur est `dsfr-form-upload`, `dsfr-upload` est déprécié
- fix(card): ajout de la class `fr-card__title` sur la gestion des niveaux de titre
- fix(card): niveau de titre obligatoire, `h3` par défaut
- fix(card): la carte ne doit pas émettre d'événement autre que sur sélection d'un lien (d'une `route`)
- fix(checkbox): ne pas positionner l'attribut aria-describedby s'il n'y a pas de message
- fix(LinkTarget): dépréciation de `DsrfTargetLink` au profit de `DsfrLinkTarget`
- fix(upload): utilisation dans un formulaire

## 1.3.2 (2023-09-04)

- feat(tag): ajout d'un id (optionnel) pour un tag
- fix(form): prise en compte de l'attribut `disabled` lors de l'utilisation des reactive form pour tous les composants concernés
- fix(link/menu): gestion de l'attribut `aria-current` par routerLink lorsque cette directive est utilisée
- fix(tag): mise à jour de la propriété `selected`

## 1.3.1 (2023-08-29)

- fix(header): les icônes des liens rapides ne s'affichent pas
- fix(radio): avec plusieurs groupes de radios, la sélection sur un groupe se répercute sur tous les autres groupes
- fix(radio-rich): avec plusieurs groupes de radios, la sélection sur un groupe se répercute sur tous les autres groupes
- fix(input): ne pas positionner l'attribut aria-describedby s'il n'y a pas de message

## 1.3.0 (2023-06-22)

- feat(button): ajout des propriétés `id`, `customClass`
- feat(email): nouveau composant `dfsr-form-email`
- feat(fieldset): les 2 sélecteurs `dsfr-fieldset` et `dsfr-form-fieldset` peuvent être utilisés
- feat(input): ajout des propriétés `buttonTooltipMessage`, `buttonType`, `buttonIcon`, `buttonDisabled`
- feat(input): ajout de l'événement `buttonSelect`
- feat(link): ajout de la propriété `tooltipMessage`
- feat(link): lien inactif si ni `link`, ni `route` ni `routerLink`
- feat(patterns/date): nouveau modèle pour la saisie de date
- feat(patterns/page-response): nouveau bloc fonctionnel pour les pages d'erreur
- feat(select): ajout de la propriété `disabled`
- feat(tag): ajout de la propriété `disabled`
- feat(tel): nouveau composant `dfsr-form-tel`
- feat(translate): liste déroulante sans bordure avec `outline=false`
- fix(control): Error: No value accessor for form control with unspecified name attribute
- fix(franceconnect): il ne se produit rien quand on clique sur le logo
- fix(password): l'attribut `name` n'est pas valorisé
- fix(tag): l'attribut `aria-label` d'un tag supprimable n'est pas internationalisé
- fix(tag): première sélection d'un tag sélectionnable non fonctionnel
- fix(translate): Ne fonctionne pas dans un 'reactive form'

## 1.2.0 (2023-06-26)

- feat(accordion): le slot `[content]` est déprécié en faveur du slot par défaut
- feat(display): nouveau composant (paramètres d'affichage, modes clair ou sombre, à partir du header ou footer)
- feat(fieldset): sélecteur `dsfr-fieldset` déprécié au profit de `dsfr-form-fieldset`
- feat(fieldset): possibilité d'ajouter des classes CSS sur les éléments du fieldset
- feat(fieldset): ajout de la propriété `legendSrOnly` permettant de masquer visuellement la légende
- feat(fieldset): la propriété `error` peut à présent être valorisée avec un tableau d'erreurs
- feat(follow): nouveau composant réseaux sociaux
- feat(form-input): remplacement du boolean `textarea` par un type `textarea`
- feat(form-input): valorisation de `inputmode` selon le type de l'input
- feat(form-input): ajout de la propriété `pattern`
- feat(form-input): dépréciation de la propriété `width` au profit de `customClass`
- feat(form-input): ajout du type `password` (mais utiliser de préférence `dsfr-form-password`)
- feat(form-input): combo champs + bouton (nouvelle fonctionnalité)
- feat(form-input): ajout de l'attribut `spellcheck`
- feat(form-input): ajout des type `time` et `datetime-local`
- feat(form-select): une option peut être `disabled`
- feat(patterns/name): nouveau bloc fonctionnel _Nom et prénom_
- feat(patterns/address): nouveau bloc fonctionnel pour la saisie d'adresse
- fix(form-input): les classes du tag `input` incorrectes si erreur, validation ou disabled
- fix(form-input): le label de l'input a un id à null (code: `id="null"`)
- fix(form-input): le `for` du label ne référence aucun tag (code: `for="undefined"`)
- fix(form-input): différents attributs ont une valeur incorrecte (ex : `min="null"`)
- fix(form-input): ajout balise manquante `<div class="fr-messages-group">` autour des messages (erreur et validation)
- fix(form-select): les options regroupées ne fonctionne pas
- fix(form-select): le placeholder ne fonctionne pas si la valeur initiale est `undefined`
- fix(form-select): label non grisé quand l'input est `disabled`

## 1.1.3 (2023-06-14)

- fix: `select` l'attribut `for` du label ne référence pas l'id du select
- fix: `select` placeHolder non fonctionnel

## 1.1.2 (2023-06-09)

- fix: `modal` ne s'ouvre pas
- fix: `fieldset` erreur NG0100 (_Expression has changed after it was checked_)

## 1.1.1 (2023-06-08)

- fix: documentation version DSFR (changelog et readme)

## 1.1.0 (2023-06-08)

- feat: montée de version DSFR 1.9.3
- feat: `share` nouveau composant
- feat: `transcription` nouveau composant
- feat: `fieldset` nouveau composant permettant d'agencer les différents champs d'un formulaire
- feat: `logo` nouveau composant, bloc marque de l'état
- feat: `form-radio` ajout propriété `legendRegular` permettant de rétablir une graisse standard sur la légende
- feat: `form-radio` dépréciation propriété `radios` au profit de `options`
- feat: `form-radio-rich` ajout propriété `legendRegular` permettant de rétablir une graisse standard sur la légende
- feat: `form-radio-rich` dépréciation propriété `radioRich` au profit de `options`
- feat: les propriétés `*icon` sont à présent de type `DsfrIcon` (permet le contrôle des valeurs)
- fix: `form-checkbox` ajout du message de validation (Dsfr 1.8.2)

## 1.0.2 (2023-05-15)

- fix: `form-checkbox` pas de changement d'état au click
- fix: ajustements des `peerDependencies` et des `dependencies`

## 1.0.1 (2023-05-05)

- fix: compatibilité Angular > 14

## 1.0.0 (2023-05-04)

- break: le préfixe `ngx-` a été supprimé de tous les composants
- break: `alert` l'input `roleAlert` est renommé `hasAriaRole`
- break: `button` suppression du support des icônes FontAwesome
- break: `callout` l'input `iconClass` est renommé `icon`
- break: `card` suppression du support des icônes FontAwesome
- break: `card` l'input `useActionArea` est renommé `hasFooter`
- break: `card` le sélecteur ng-content `data-actions` est renommé `footer`
- break: `card` l'output `routeSelect` est renommé `cardSelect`
- break: `content` l'input `transcriptHref` est renommé `transcriptionLink`
- break: `consent` l'output `acceptAllEvent` est renommé `acceptAllSelect`
- break: `consent` l'output `refuseAllEvent` est renommé `refuseAllSelect`
- break: `consent` l'output `openModalEvent` est renommé `customizeSelect`
- break: `consent` l'output `confirmCustomizeEvent` est renommé `confirmCustomizeSelect`
- break: `consent` l'output `changeAcceptFinalityEvent` est renommé `finalityChange`
- break: `consent` le composant a été renommé `consent-banner`
- break: `content` l'input `transcriptLabel` est renommé `transcriptionLabel`
- break: `download`, `hreflang` est renommé `langCode`
- break: `download` l'input `fileHref` est renommé `link`
- break: `download` l'input `fileMimeType` est renommé `mimeType`
- break: `download` l'input `fileSizeBytes` est renommé `sizeBytes`
- break: `download` l'input `fileDescription` est renommé `description`
- break: `footer` l'input `logo.href` est renommé `logo.link`
- break: `form-password` l'input `recoveryPasswordUrl` est renommé `recoveryLink`
- break: `form-radio` suppression de la feature `addonTemplateRef`
- break: `header` l'input `blockMarkLabel` est renommé `logoLabel`
- break: `header` l'input `blockMarkTooltip` est renommé `logoTooltipMessage`
- break: `header` l'input `blockMarkHref` est renommé `logoLink`
- break: `header` l'input `imagePath` est renommé `operatorImagePath`
- break: `header` l'input `imageAlt` est renommé `operatorImageAlt`
- break: `header` l'input `verticalImage` est renommé `operatorImageVertical`
- break: `header` l'input `siteName` est renommé `serviceTitle`
- break: `header` l'input `baseline` est renommé `serviceTagline`
- break: `header` les inputs `showTranslate`,`languages`,`currentCode` sont remplacés par l'input `translate`
- break: `header` le modèle `DsfrMenuItemHeader` est renommé `DsfrHeaderMenuItem`
- break: `link` l'input `routerLinkActiveValue` est renommé `routerLinkActive`
- break: `link` l'input `position` est renommé `iconPosition`
- break: `search` l'input `isLarge` est renommé `large`
- break: `sidemenu` l'input `fullHeight` est renommé `stickyFullHeight`
- break: `sidemenu` l'input `onRight` est renommé `position` et est à présent de type `DsfrPosition`
- break: `search` le composant `search` a été renommé `search-bar`
- break: `tab` l'input `iconClass` est renommé `icon`
- break: `table` l'input `bottomCaption` est renommé `captionBottom`
- break: `table` l'input `customThemeClass` est renommé `customClass`
- break: `table` l'input `customHeaderClass` est renommé `headerCustomClass`
- break: `table` l'input `rowBorders` est renommé `bordered`
- break: `tile` l'output `routeSelect` est renommé `tileSelect`
- break: `translate` l'input `currentCode` est renommé `currentLangCode`
- feat: `quote` ajout de la propriété `textSize`
- feat: `tile` ajout de l'input `customBackground`
- fix: `link` si routerLink est utilisé la propriété `target` est valorisée au lieu de l'attribut [attr.target]

## 1.0.0-rc.5

- break: `select` est renommé `form-select`
- break: `radio` est renommé `form-radio`
- break: `radio-extended` est renommé `form-radio-rich`
- break: `checkbox` est renommé `form-checkbox`
- break: `password` est renommé `form-password`
- break: `alert` l'attribut `role='alert'`est à présent dynamique et piloté par l'input booléen `roleAlert`
- break: `alert` suppression de l'attribut `closeControlId` au profit de `closable` (cf. guide de migration)
- break: `buttons-group` l'input `breakpoint` est supprimé au profit d'une évolution de l'input `inline` (cf. migration)
- break: `buttons-group` l'input `inline` est à présent de type `DsfrInline` (cf. guide de migration)
- break: `buttons-group` le type `DsfrAlignment` a été renommé `DsfrAlign`
- break: `card` la propriété `href` est remplacée par `link`
- break: `password` homogénéisation avec les autres composants de formulaire, `description` est renommé `hint`
- break: `radio` suppression des inputs `bindLabel` `bindValue` `bindAideRadio` au profit d'un modèle (cf. guide de migration)
- break: `select`, `firstOption` est renommé `placeHolder`
- break: `summary` l'évènement `routeSelect` est renommé `entrySelect`
- break: `summary`, `skiplinks` : `DsfrSkipLink` est renommé `DsfrAnchorLink` et la propriété `anchorName` devient `link`
- break: `tag`, la propriété `href` est remplacée par `link` ou `routerLink`
- break: `tag`, suppression de la propriété `disabled`
- break: `tag` suppression input `noRedirect` (cf. guide de migration)
- break: `tile` suppression input `noRedirect` (cf. guide de migration)
- break: `tile` suppression input `href` au profit de `link`
- break: `toogle` l'input `defaultChecked` a été supprimé
- break: `toogle` l'input `showCheckedLabel` a été renommé `showCheckedHint`
- break: `toogle` l'input `dataLabelChecked` a été renommé `checkedHintLabel`
- break: `toogle` l'input `dataLabelUnchecked` a été renommé `uncheckedHintLabel`
- break: `toggles-group` la propriété `hideSeparators` est renommée `showSeparators` (et la fonction est inversée)
- feat: `accordion` un identifiant unique est généré pour l'index.
- feat: `buttons-group` ajout de la propriété `iconPosition`
- feat: `card` ajout propriété `route` permettant d'inhiber le href natif au profit d'une navigation programmatique
- feat: `DsfrLink` ajout de l'input `routerLinkExtras` (modèle utilisé dans `header`, `footer`, `breadcrump`).
- feat: `DsfrAnchorLink` ajout propriété `fragment` pour utiliser avec la directive routerLink (`summary`, `skiplinks)`)
- feat: `link` possibilité d'utiliser la directive routerLink d'angular à travers l'attribut `routerLink` (cf guide de migration).
- feat: `link` ajout de l'input `routerLinkExtras` pour pouvoir paramétrer la route
- feat: `sideMenu` Les `controlId` de chaque nœud sont générés automatiquement par défaut
- feat: `tile` ajout propriété `route` permettant d'inhiber le href natif au profit d'une navigation programmatique
- feat: `toogle` le slot `label` est à présent le slot par défaut (plus besoin de sélecteur)
- feat: `toggle` par défaut, l'`id` est généré automatiquement
- fix: `button` suppression de la valeur par défaut sur l'input `iconPosition` (cf. guide de migration)
- fix: `card` detail displays `detailBottom` value instead of `detail` value
- fix: `checkbox` la valeur de la checkbox peut être initialisée
- fix: `radio` correctifs d'accessibilité sur les labels et sélection au clavier

## 1.0.0-rc.4

- break: `card` suppression de la propriété `useHeadingAside` (cf. guide de migration)
- break: `DsfrImageRatioConst` les noms de constantes ont été renommées (cf. guide de migration)
- break: `input` `InputType` est renommé `DsfrInputType`
- break: `hightlight` suppression de la propriété `borderWidth`
- break: `hightlight` le slot déprécié avec pour sélecteur `role=content` est supprimé
- break: `hightlight` le slot déprécié avec pour sélecteur `[data-content]` est supprimé au profit su slot par défaut
- break: `input` (type=date) la largeur du champ n'est plus contrainte en dur à 10rem
- feat: ajout du composant `Notice`
- feat: `highlight` ajout de l'input `text` (même fonction que le slot par défaut)
- fix: report des correctifs effectués en [`0.1.0-beta.11`](#010-beta11)
- fix: `header` `footer` `content` `pagination` les tooltips affichaient `undefined` s'ils n'étaient pas renseignés
- chore: réorganisation de l'arborescence de composants

## 1.0.0-rc.3

- feat: `Callout` ajout propriété `headingLevel` pour contrôler le niveau titre
- feat: `Callout` l'input `icon:boolean` est remplacé par `iconClass:string` afin de pouvoir choisir l'icône à afficher
- feat: `Download` La taille peut être exprimée en `bytes` ou en `octets` avec la propriété `sizeUnit`
- feat: `Download` Les méta-data (zone de détail) sont gérées le DSFR, ou par la librairie si `dsfrMetadata==false`
- feat: `Download Multiple` on peut définir le niveau du titre de la liste
- feat: `ButtonsGroup` ajout propriété `buttonsEquisized` (force la largeur la plus élevée sur tous les boutons)
- fix: `Callout` refacto cf. guide de migration
- fix: `Tile` erreur _DOMException_ en mode cliquable
- fix: `Alert` lacune du guide de migration (rc.1 vers rc.2) concernant le format des sévérités (minuscules)

## 1.0.0-rc.2

- feat: Internationalisation de la librairie ('fr', 'en')
- feat: Ajout du composant `Password`
- feat: `Tile` ajout de l'évènement `linkSelect` au clic sur un lien
- fix: `Header` ajout de l'évènement `langChange` au changement de langue dans le sélecteur
- fix: `Toggle` meilleure intégration avec les Forms
- chore: ajout de la configuration playwright

## 1.0.0-rc.1

- break: suppression des propriétés dépréciées (cf. guide de migration)
- break: renommage des attributs aide en hint (cf. guide de migration)
- break: renommage des attributs tooltip en tooltipMessage (cf. guide de migration)
- feat: `Content` ajout des contenus médias (image, vidéo, svg)
- feat: `Translate` ajout du composant sélecteur de langue
- feat: `Header` possibilité d'afficher le sélecteur de langue dans le header
- feat: `Card` ajout de la propriété `size`
- feat: `Card` ajout des icônes des zones de détail
- feat: `Card` ajout d'un ratio sur l'image
- feat: `Card` les badges sur le média et dans le contenu sont exclusifs
- fix : `Footer` ajout du paramètre `accessibility` pour la déclaration de conformité
- fix : `Footer` correction style du pied de page complet

## 0.1.0-beta.11

- fix: (#174) `tile` le titre ne présente pas le bon style
- fix: (#175) `alert` le titre ne présente pas le bon style
- fix: (#176) `tile` le tooltip s'affiche à la place du titre
- fix: (#177) `radio-button` ne retourne pas la valeur d'origine, mais une chaîne de caractères
- fix: (#178) `input` propriété "disabled" inopérante
- fix: (#179) `button` tooltip undefined
- fix: (#180) `modal` le bouton déclenchant l'ouverture de la modale ne reprend pas le focus à la fermeture de la modale

## 0.1.0-beta.10

- feat: `Search` ajout du composant de barre de recherche
- fix: `Tabs` en vue mobile, pas de scroll horizontal sur la barre d'onglets

## 0.1.0-beta.9

- feat: `Accordion` ajout d'une propriété `content` (prioritaire sur le slot `content`)
- feat: `Consent` ajout de la fonctionnalité bannière de consentement
- feat: `DsfrSizeType` accepte aussi les minuscules `sm`, `md` et `lg` (simplicité d'écriture)
- feat: `Input` accepte le type `email`
- fix: `Accordion` dépréciation du slot `body` au profit du slot `content`
- fix: `SideMenu` restauration de la propriété `ariaLabelledby` mais dépréciée
- fix: `Tabs` l'événement `select` est déprécié au profit de `tabSelect`
- fix: ajout du module `DsfrAccordionsGroupModule`
- fix: suppression des enums `DsfrSize`, `DsfrPosition` en faveur des types
- fix: dépréciation de l'enum `TargetLink` en faveur des valeurs html
- chore : compatibilité avec node 16

## 0.1.0-beta.8

- feat: `FranceConnect` ajout du composant bouton franceconnect
- feat: `Input` ajout des type `search`, `password` (standard html)
- feat: `Header` ajout de la fonctionnalité menus déroulants
- feat: `Header` ajout du support pour les "méga-menus"
- feat: `Footer` la présentation peut être en HTML, elle sera insérée dans un tag `p`
- feat: `Tag` ajout des thèmes `DsfrTagColor` (cf. DSFR)
- feat: `Tag group` peut entourer des `tag`
- feat: `Table` affichage du message `noDataMessage` si aucune donnée
- feat: `Tile` ajout des propriétés `title` et `content` (ou slots éponymes)
- fix: `Button` l'événement `(clicked)` est déprécié au profit du `(click)` standard
- fix: `Button group` dépréciation du slot `[btns]` au profit du slot par défaut
- fix: `Header` linkEvent reçoit un item de type `DsfrLink` en paramètre
- fix: `Header` dans les items, `active` est utilisé à la place de `ariaCurrent`
- fix: `Input` type non renseigné par défaut (donc `text`), idem html
- fix: `Input` l'output `input` est déprécié en faveur de `(input)` et `(change)`
- fix: `Input` `aria-describedby` décrit dans tous les cas
- fix: `Link` est déprécié en faveur de `DsfrLink` (`label` à la place de `texte`)
- fix: `Radio-button` le `span` en trop autour de legend est supprimé
- fix: `Select` les groupes ne fonctionnent pas
- fix: `Select` `selectedOption` a été supprimé à tort
- fix: `Tabs` scroll horizontal
- fix: `Tag` conforme au DSFR pour les tags supprimables et sélectionnables (`button`)
- fix: `Tag` l'interface `Tag`, utilisée par `tag-group`, est déplacée dans un répertoire `model`
- fix: `Tile` le slot `desc` est déprécié en faveur du slot `content`
- chore: `Modal` la fonction callback sur les actions n'est plus "évaluée" mais juste "invoquée"
- chore: nom explicite pour les classes des stories

## 0.1.0-beta.7

- feat: `Breadcrumb`, `url` est déprécié en faveur de `href`
- feat: `Breadcrumb` événement `itemClick()` sur sélection d'un item
- feat: `Header` `fastAccessLinks` est déprécié en faveur de `headerToolsLinks`
- feat: `Select` affichage du label avec la propriété `label` (prioritaire sur les slots)
- fix: `Accordion` le titre de section `hx` n'encapsule pas le bouton
- fix: `Button` prise en compte de la propriété `type` (`submit`, `reset`)
- fix: `Button` la propriété `bubbling=true` par défaut (fonctionnement par défaut de button)
- fix: `Header` un warning apparait dans la console si le nombre de _tools links_ est > 3
- fix: `Input` affichage du label avec la propriété `label` (prioritaire sur les slots)
- fix: `Input` fonctionnement dans un formulaire
- fix: `Pagination` Défaut d'affichage du composant
- fix: `Select` donnée modifiée ne remonte pas dans le form control

## 0.1.0-beta.6

- feat: ajout des composants `pagination` (avec `previous-page`)
- feat: ajout de `DsfrBackgroundClass` (styles DSFR)
- feat: ajout de `DsfrTextClass` (styles DSFR)
- feat: ajout de `DsfrTableThemeClass` (styles DSFR)
- feat: `Table` `customHeaderClass` permet de personnaliser l'entête du tableau
- feat: `Table` `borders` est déprécié en faveur de `rowBorders`
- feat: `Header` la sélection d'un lien poste un événement
- feat: `Header`, `footer` possibilité de personnaliser `aria-current` dans les liens via la propriété `ariaCurrent`
- feat: `Accordion` la balise de du titre de l'accordéon peut être personnalisé entre `h2` et `h6`, `h3` par défaut
- feat: `Breadcrumb` l'événement `itemClick` reçoit l'item sélectionné en paramètre (contre `true` auparavant)
- feat: `Button` utiliser `'tertiary-no-outline'` pour un bouton `'tertiary'` sans bord
- fix: `Footer` `institutionelLinks` est déprécié en faveur de `institutionalLinks` (faute de frappe)

## 0.1.0-beta.5

- feat: `accordion` le titre peut maintenant être défini par une propriété. Le slot `heading` reste présent, la propriété est prioritaire sur le slot.
- feat: ajout de `summary`
- feat: `sidemenu` activation automatique de l'item sélectionné (peut être désactivée avec `autoSelect=false`)
- feat: `sidemenu` ajout de l'événement `selectItemEvent` qui transmet l'item sélectionné de type `MenuItem`
- fix: `download` la taille du fichier n'est pas obligatoire, dans ce cas seul le type est affiché
- fix: `footer` les liens externes fonctionnent avec `_blank`, correction de régression dans StoryBook
- fix: `link` suppression de l'icône en double dans un lien externe
- fix: `sidemenu` suppression de l'erreur dans la console lorsque `routerLinkValue` n'est pas défini
- fix: `stepper` l'étape courante est correctement indiquée, toujours à 1 auparavant

## 0.1.0-beta.4

- feat: `sidemenu` Possibilité d'ajouter une icône (remix et awasome) à gauche (uniquement) dans un item de
  menu.
- feat: `StoryBook` ajout des pages `Changelog` et `Read me`

## 0.1.0-beta.3

- feat: ajout `table`
- feat: ajout `download`, `download-multiple`
- feat: ajout `radiobutton-extended`
- fix: `header` changement des attributs title du bloc marque : `blockMarkTooltip`
- fix: `header` modification du href du bloc marque : `blockMarkHref`
- fix: `header` renommage de l'input `labelLogoMarianne` en `blockMarkLabel`
- fix: `footer` dispatch de l'input : `logo` en `blockMarkTooltip`, `blockMarkHref`, `blockMarkLabel`
- fix: `base-link` fix binding aria-current

## 0.1.0-beta.2

- fix: `base-link` changed `href`, `routerLinkValue`, `routerLinkActiveValue` default values

## 0.1.0-beta.1

- fix: `routerLinkValue` fonctionne dans `side-menu` et les liens réglementaires de `footer`

## 0.1.0-alpha.9

- chore: migration Angular 13.3.11 vers 14.2.12
- chore: migration de Storybook 6.4.14 vers 6.5.14
- feat: ajout de `accordions-group`
- feat: ajout du composant `breadcrumb`
- feat: `buttons-group` encadre automatiquement les éléments `button` par des éléments `<li`
- feat: ajout `stepper`
- feat: ajout `upload`
- feat: `sidemenu` `routerLink` dans les items
- feat: `modal` ajout propriété `icon` optionnelle
- feat: `card` ajout propriété `customBackground` dépréciant `useGreyBackground`

## 0.1.0-alpha.8

- chore: migration Angular 13
