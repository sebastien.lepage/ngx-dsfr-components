import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { DefaultControlComponent, DsfrSeverity, DsfrSeverityConst, newUniqueId } from '../../shared';
import { DsfrInputMode, DsfrInputType, DsfrInputTypeConst } from './form-input.model';

const INPUT_TYPE_TO_MODE = {
  'date': '',
  'datetime-local': '',
  'email': 'email',
  'number': 'numeric',
  'password': '',
  'search': 'search',
  'tel': 'tel',
  'text': '',
  'textarea': '',
  'time': '',
};

/**
 * BaseInputComponent n'embarque que les propriétés d'un input Html
 */
@Component({ template: '' })
export abstract class BaseInputComponent extends DefaultControlComponent<string> {
  /**
   * L'attribut HTML de saisie semi-automatique permet aux développeurs Web de spécifier l'éventuelle autorisation
   * dont dispose l'agent utilisateur pour fournir une assistance automatisée dans le remplissage des valeurs des
   * champs du formulaire, ainsi que des conseils au navigateur quant au type d'informations attendues dans le champ.
   * Ex : email, name, etc.
   */
  @Input() autocomplete: string | undefined;

  /**
   * 👓 Correspond à l'attribut `aria-autocomplete`.<br />
   * L'attribut `aria-autocomplete` indique si la saisie de texte peut déclencher l'affichage d'une ou plusieurs
   * prédictions de la valeur prévue par l'utilisateur pour une zone de liste déroulante, une zone de recherche ou
   * une zone de texte et spécifie comment les prédictions seront présentées si elles sont effectuées.
   * Ex : `ariaAutocomplete="list"` lorsque la saisie d'une valeur fait apparaitre une liste de suggestions.<br />
   * Valeur par défaut : `undefined` (n'apparait pas comme attribut dans l'html).
   * @since 1.4
   */
  @Input() ariaAutocomplete: string;

  /**
   * 👓 Correspond à l'attribut `aria-expanded`.<br />
   * L'attribut `aria-expanded` est défini sur un élément pour indiquer si un contrôle est développé ou réduit,
   * et si les éléments contrôlés sont affichés ou masqués. <br />
   * Valeur par défaut : `undefined` (n'apparait pas comme attribut dans l'html).
   * @since 1.4
   */
  @Input() ariaExpanded: boolean | undefined;

  /**
   * 👓 Correspond à l'attribut `aria-label`.<br />
   * L'attribut `aria-label` est utilisé pour définir une légende non visible associée à un élément HTML dont le
   * sens est transmis uniquement par le visuel.<br />
   * Valeur par défaut : `undefined` (n'apparait pas comme attribut dans l'html).
   */
  @Input() ariaLabel: string;

  /**
   * 👓 Les [rôles ARIA](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles) fournissent une
   * signification sémantique au contenu, permettant aux lecteurs d'écran et à d'autres outils de présenter et
   * de prendre en charge l'interaction avec un objet d'une manière cohérente avec les attentes des utilisateurs
   * concernant ce type d'objet. Les rôles ARIA peuvent être utilisés pour décrire des éléments qui n'existent
   * pas nativement en HTML ou qui existent, mais ne bénéficient pas encore d'une prise en charge complète par
   * le navigateur. Ex : `toolbar`, `tabpanel`, etc.<br />
   * Valeur par défaut : `undefined` (n'apparait pas comme attribut dans l'html).
   * @since 1.4
   */
  @Input() role: string;

  /**
   * Une chaîne de caractères, on ou off, qui indique si la correction automatique est activée.
   * [Safari uniquement](https://developer.mozilla.org/fr/docs/Web/HTML/Element/input#autocorrect).
   * @since 1.6
   */
  @Input() autoCorrect = true;

  /**
   * Indique si le champ est obligatoire ou non, faux par défaut.
   */
  @Input() required = false;

  /**
   * Placeholder de l'input.
   */
  @Input() placeholder: string;

  /**
   * Attribut min de l'input.
   */
  @Input() min: string;

  /**
   * Attribut max de l'input.
   */
  @Input() max: string;

  /**
   * Attribut minLength de l'input.
   */
  @Input() minLength: number;

  /**
   * Attribut maxLength de l'input.
   */
  @Input() maxLength: number;

  /**
   * Désactive la correction orthographique sur les champs relatifs au nom et aux prénoms.
   */
  @Input() spellCheck = true;

  /**
   * Ajoute un icon à droite dans le champ de saisie.
   */
  @Input() icon: string;

  /**
   * Pattern de l'input.
   */
  @Input() pattern: string;

  /**
   * Ajout d'un style spécifique, permet par exemple de la limiter la largeur d'un input.
   */
  @Input() customClass: string;

  /**
   * Change la mise en page d'un input accompagné d'un bouton.
   */
  @Input() inputWrapMode: 'addon' | 'action' = 'addon';

  /**
   * Message d'information lié au composant
   */
  @Input() message: string | undefined = undefined;

  /**
   * Représente la sévérité du message. 🔥 `WARNING` n'est pas géré dans cette version.
   */
  @Input() messageSeverity: DsfrSeverity;

  /**
   * Émet un événement, si la propriété `pattern` est définie, à chaque changement de la valeur de l'input avec
   * une valeur booléenne indiquant si le `pattern` est validé ou non.
   */
  @Output() patternValueChange = new EventEmitter<boolean>();

  /**
   * L'input mode est initialisé par défaut en fonction du type de l'input, 'decimal', 'url' ne sont pas traité pour
   * l'instant.
   * @internal
   */
  inputMode: DsfrInputMode;

  /**
   * Id de la div affichant les messages d'erreur ou de validation.
   * @internal
   */
  readonly messagesGroupId = newUniqueId();

  private _type: DsfrInputType = DsfrInputTypeConst.TEXT;

  get type(): DsfrInputType {
    return this._type;
  }

  /** @deprecated since 1.12.0 use messageSeverity instead */
  get severity(): DsfrSeverity {
    return this.messageSeverity;
  }

  /** @deprecated since 1.11.0 use placeholder instead (all lowercase) */
  get placeHolder(): string {
    return this.placeholder;
  }

  /**
   * Permet de récupérer le message d'erreur s'il existe
   * @since 1.11
   */
  get error(): string | undefined {
    return this.messageSeverity === DsfrSeverityConst.ERROR ? this.message : undefined;
  }

  /**
   * Permet de récupérer le message de validation s'il existe
   * @since 1.11
   */
  get valid(): string | undefined {
    return this.messageSeverity === DsfrSeverityConst.SUCCESS ? this.message : undefined;
  }

  /**
   * Type de l'input, 'text' par défaut.
   */
  @Input() set type(value: DsfrInputType) {
    this._type = value;
    this.inputMode = <DsfrInputMode>INPUT_TYPE_TO_MODE[this._type] || undefined;
    // TODO 2.0 A Supprimer le pattern par défaut
    if (this._type === DsfrInputTypeConst.NUMBER && !this.pattern) {
      this.pattern = '[0-9]*';
    }
  }

  /** @deprecated since 1.12.0 use messageSeverity instead */
  @Input() set severity(value: DsfrSeverity) {
    this.messageSeverity = value;
  }

  /**
   * Positionne un message d'erreur
   * @since 1.11
   */
  @Input() set error(value: string | undefined) {
    this.message = value;
    this.messageSeverity = DsfrSeverityConst.ERROR;
  }

  /**
   * Positionne un message de validation
   * @since 1.11
   */
  @Input() set valid(value: string | undefined) {
    this.message = value;
    this.messageSeverity = DsfrSeverityConst.SUCCESS;
  }

  /** @deprecated since 1.11.0 use placeholder instead (all lowercase) */
  @Input() set placeHolder(value: string) {
    this.placeholder = value;
  }

  /**
   * À l'écoute de la valeur afin d'émettre un événement dans le cas où on a un pattern.
   * @internal
   */
  @HostListener('input')
  onPatternValueChange() {
    if (!this.pattern) return;
    const isValid = new RegExp('^' + this.pattern + '$').test(this.value!);
    this.patternValueChange.emit(isValid);
  }

  /** @internal */
  isNumber() {
    return this.type === DsfrInputTypeConst.NUMBER;
  }

  /** @internal */
  hasMessages(): boolean {
    return !!this.message && this.severity !== undefined;
  }
}
