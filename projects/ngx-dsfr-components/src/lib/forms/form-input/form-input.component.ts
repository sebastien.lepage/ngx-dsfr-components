import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { isStringEmptyOrNull } from '../../shared';
import { BaseInputComboComponent } from './base-input-combo.component';
import { DsfrInputTypeConst } from './form-input.model';

@Component({
  selector: 'dsfr-form-input',
  templateUrl: './form-input.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormInputComponent),
      multi: true,
    },
  ],
})
export class DsfrFormInputComponent extends BaseInputComboComponent {
  /**
   * Type 'textarea' : nombre de lignes.
   */
  @Input() rows: number;

  /**
   * Cache le label visuellement en le laissant disponible aux lecteurs d'écran.
   */
  @Input() labelSrOnly = false;

  /**
   * @deprecated (since 1.2) utiliser `customClass` à la place.
   * Largeur de l'input, `'100%'` par défaut, `'10rem'` par défaut pour un type date.
   */
  @Input() width: string;

  /** @deprecated since 1.2 use `type` instead. */
  get textarea(): boolean {
    return this.isTextArea();
  }

  /** @deprecated (@since 1.2) utiliser `type` à la place. */
  @Input() set textarea(value: boolean) {
    this.type = value ? DsfrInputTypeConst.TEXTAREA : DsfrInputTypeConst.TEXT;
  }

  /** @internal */
  isTextArea() {
    return this.type === DsfrInputTypeConst.TEXTAREA;
  }

  /** @internal */
  hasInputWrap(): boolean {
    return this.hasButton() || !isStringEmptyOrNull(this.icon);
  }

  /** @internal */
  getWrapClasses(): string[] {
    let classes = ['fr-input-wrap'];
    if (this.hasButton() && this.inputWrapMode === 'addon') classes.push('fr-input-wrap--addon');
    if (this.hasButton() && this.inputWrapMode === 'action') classes.push('fr-input-wrap--action');
    if (this.icon) classes.push(this.icon);

    return classes;
  }

  /** @internal */
  onButtonClick(event: Event) {
    if (this.buttonType != 'submit') this.buttonSelect.emit(event);
  }
}
