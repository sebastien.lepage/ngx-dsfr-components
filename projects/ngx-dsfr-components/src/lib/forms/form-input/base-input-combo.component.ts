import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DsfrButtonType, DsfrButtonVariant, isStringEmptyOrNull } from '../../shared';
import { BaseInputComponent } from './base-input.component';

/**
 * BaseInputComboComponent ajoute la possibilité d'avoir un bouton sur le côté de l'input
 */
@Component({ template: '' })
export abstract class BaseInputComboComponent extends BaseInputComponent {
  /** [accessibilité] Spécifie le libellé qui sera retranscrit par les narrateurs d'écran. */
  @Input() buttonAriaLabel: string;

  /**
   * Permet de désactiver le bouton d'action, 'false' par défaut.
   */
  @Input() buttonDisabled = false;

  /**
   * Crée un combo champ + bouton si buttonIcon est renseigné
   */
  @Input() buttonIcon: string | undefined; // undefined car peut être créé à partir de DsfrButton (composant name par exemple)

  /**
   * Crée un combo champ + bouton si buttonLabel est renseigné
   */
  @Input() buttonLabel: string;

  /**
   * Tooltip message sur le bouton s'il y a lieu.
   */
  @Input() buttonTooltipMessage: string;

  /**
   * Type du button,'submit' par défaut.
   */
  @Input() buttonType: DsfrButtonType;

  /** Style du bouton, 'primary' par défaut. */
  @Input() buttonVariant: DsfrButtonVariant = 'primary';

  /**
   * Emission de l'événement si le type du bouton est != de `submit`.
   */
  @Output() buttonSelect = new EventEmitter<Event>();

  /** @internal */
  hasButton(): boolean {
    return !isStringEmptyOrNull(this.buttonLabel) || this.buttonIcon !== undefined;
  }
}
