import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../../components';
import { DsfrFormInputModule } from '../form-input.module';
import { DemoInputPatternComponent } from './demo-input-pattern.component';

const meta: Meta = {
  title: 'FORMS/Input',
  component: DemoInputPatternComponent,
  decorators: [moduleMetadata({ imports: [DsfrFormInputModule, FormsModule, DsfrButtonModule] })],
};
export default meta;
type Story = StoryObj<DemoInputPatternComponent>;

export const Pattern: Story = {
  args: {
    value: '999',
    pattern: '[A-Za-z0-9]{4}',
    hint: 'Saisir un code sur 4 caractères',
  },
};
