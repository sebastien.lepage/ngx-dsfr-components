import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DemoInputReactiveComponent } from './demo-input-reactive.component';

const meta: Meta = {
  title: 'FORMS/Input',
  component: DemoInputReactiveComponent,
  decorators: [moduleMetadata({ imports: [DemoInputReactiveComponent] })],
};
export default meta;
type Story = StoryObj<DemoInputReactiveComponent>;

export const ReactiveForm: Story = {
  args: {
    pattern: '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
    value: 'Abcdef78',
    hint: 'Doit contenir au moins 1 chiffre et 1 lettre majuscule et minuscule, et au moins 8 caractères ou plus',
  },
};
