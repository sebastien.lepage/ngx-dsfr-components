import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DemoToolbarComponent } from '../../../shared/components/demo/demo-toolbar.component';
import { DsfrFormInputModule } from '../form-input.module';

@Component({
  selector: 'demo-input-reactive',
  templateUrl: './demo-input-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrButtonModule, DsfrFormInputModule, DemoToolbarComponent],
})
export class DemoInputReactiveComponent implements OnChanges {
  @Input() value: string;
  @Input() pattern: string;
  @Input() hint: string;

  /** @internal */
  formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      data: this.value,
    });
  }

  /** @internal */
  ngOnChanges(changes: SimpleChanges) {
    if (changes['value']) this.formGroup.controls['data'].setValue(this.value);
  }
}
