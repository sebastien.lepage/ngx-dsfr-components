import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DsfrFormInputModule } from '../form-input.module';

@Component({
  selector: 'demo-input-date',
  templateUrl: './demo-input-date.component.html',
  standalone: true,
  imports: [CommonModule, DsfrFormInputModule, FormsModule, DsfrButtonModule],
})
export class DemoInputDateComponent {
  @Input() value: any;
  @Input() error: string;

  get type() {
    return typeof this.value;
  }

  /** @internal */
  onInput() {
    this.error = '';
  }

  /** @internal */
  onSubmit() {
    if (!this.value) this.error = 'Date incorrecte';
  }
}
