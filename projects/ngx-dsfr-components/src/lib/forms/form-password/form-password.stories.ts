import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrLinkModule } from '../../components';
import { DsfrFormPasswordComponent } from './form-password.component';

const meta: Meta = {
  title: 'FORMS/Password',
  component: DsfrFormPasswordComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrLinkModule, RouterTestingModule] })],
  argTypes: {
    recoveryRouteSelect: { control: argEventEmitter },
    value: { control: { type: 'text' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormPasswordComponent>;

const DefaultValues: Story = {
  args: {
    // DefaultValueAccessorComponent
    value: '',
    disabled: false,
    // DefaultControlComponent
    ariaControls: '',
    hint: '',
    label: '',
    name: '',
    // Password
    autocomplete: 'current-password',
    recoveryLink: '',
    recoveryRoute: '',
    recoveryRouterLink: '',
    recoveryRouterLinkActive: '',
    validationRules: [],
  },
};
const label = 'Mot de passe';
const hint = 'Texte de description additionnel';

export const Default: Story = {
  decorators: dsfrDecorator('Mot de passe de connexion'),
  args: {
    ...DefaultValues.args,
    label: label,
    recoveryRouterLink: '/recovery',
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Mot de passe avec description'),
  args: {
    ...DefaultValues.args,
    label: label,
    hint: hint,
  },
};
