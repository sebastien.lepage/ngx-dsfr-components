import { expect, FrameLocator, Locator, test } from '@playwright/test';

test('checkbox.default', async ({ page }) => {
  await page.goto('?path=/story/forms-checkbox--default');
  const frameLocator: FrameLocator = page.frameLocator('#storybook-preview-iframe');

  const inputLocator: Locator = await frameLocator.locator('input[type=checkbox]');

  expect(inputLocator).toBeDefined();

  const checkedAttr = await inputLocator.getAttribute('checked');
  expect(checkedAttr).toBeNull();

  const idAttr = await inputLocator.getAttribute('id');
  expect(idAttr?.length).toBeDefined();
});

test('checkbox.click', async ({ page }) => {
  await page.goto('?path=/story/forms-checkbox--default');
  // Switch the context to the iframe by its name or CSS selector
  const frame = page.frameLocator('#storybook-preview-iframe');
  // Perform actions within the iframe
  const input = await frame.locator('dsfr-form-checkbox input[type=checkbox]');
  await expect(input).not.toBeNull();
  await expect(input).not.toHaveAttribute('checked');
  await expect(input).not.toBeChecked();
  // check the box
  // await input.click(); // timout... but why ??
  // await input.check(); // timout... but why ??
  await frame.locator('dsfr-form-checkbox label').click();
  await expect(input).not.toHaveAttribute('checked');
  await expect(input).toBeChecked();
});

test('checkbox.checked', async ({ page }) => {
  await page.goto('?path=/story/forms-checkbox--checked');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const input = await frame.locator('input[type=checkbox]');
  await expect(input).toBeDefined();

  const attribute = await input.getAttribute('checked');
  expect(attribute).toBeDefined();
});
