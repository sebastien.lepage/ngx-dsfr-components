import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrFormToggleModule } from '../form-toggle';
import { DsfrFormTogglesGroupComponent } from './form-toggles-group.component';

@NgModule({
  declarations: [DsfrFormTogglesGroupComponent],
  exports: [DsfrFormTogglesGroupComponent],
  imports: [CommonModule, DsfrFormToggleModule],
})
export class DsfrFormTogglesGroupModule {}
