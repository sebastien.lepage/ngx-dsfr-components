import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  Input,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { DomUtils } from '../../shared';
import { BaseFieldsetComponent } from '../fieldset/base-fieldset.component';
import { DsfrFormToggleComponent } from '../form-toggle';

@Component({
  selector: 'dsfr-form-toggles-group',
  templateUrl: './form-toggles-group.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFormTogglesGroupComponent extends BaseFieldsetComponent implements AfterViewInit {
  /** @internal */
  @ContentChildren(DsfrFormToggleComponent) toggles: QueryList<DsfrFormToggleComponent>;

  private _showSeparators = true;

  constructor(private _elementRef: ElementRef) {
    super();
  }

  get showSeparators(): boolean {
    return this._showSeparators;
  }

  /** Permet de masquer la bordure horizontale séparant les toggles dans le groupe. */
  @Input() set showSeparators(value: boolean) {
    this._showSeparators = value;
    this.updateTogglesBorderBottom();
  }

  ngAfterViewInit() {
    // Ajoute des tags <li> autour des toggles
    DomUtils.surroundChildWithli(this._elementRef, 'dsfr-form-toggle');

    // Gestion des bordures de séparation
    this.updateTogglesBorderBottom();
  }

  private updateTogglesBorderBottom() {
    if (this.toggles) {
      this.toggles.forEach((toggle) => (toggle.showSeparator = this.showSeparators));
    }
  }
}
