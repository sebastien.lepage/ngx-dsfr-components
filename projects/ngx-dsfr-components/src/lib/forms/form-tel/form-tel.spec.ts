import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrFormTelComponent } from './form-tel.component';

describe('DsfrFormTelComponentTest', () => {
  let telComponent: DsfrFormTelComponent;
  let fixture: ComponentFixture<DsfrFormTelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrFormTelComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DsfrFormTelComponent);
    telComponent = fixture.componentInstance;
    // Valeurs par défaut
    // Détection du changement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(telComponent).not.toBeNull();
  });

  it('should be of type tel', () => {
    const inputElt = fixture.nativeElement.querySelector('.fr-input');
    expect(inputElt).not.toBeNull();
    const inpuType = inputElt.getAttribute('type');
    expect(inpuType).toEqual('tel');
  });
});
