import { CommonModule } from '@angular/common';
import { Component, forwardRef, HostListener, OnInit } from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DsfrButtonModule } from '../../components';
import { I18nService, InputGroupComponent } from '../../shared';
import { DsfrFormInputComponent } from '../form-input';
import { DsfrInputTypeConst } from '../form-input/form-input.model';
import { formatPhoneNumber, isPhoneNumberValid } from './phone-number-utils';

@Component({
  selector: 'dsfr-form-tel',
  templateUrl: '../form-input/form-input.component.html',
  standalone: true,
  imports: [CommonModule, FormsModule, InputGroupComponent, DsfrButtonModule],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormTelComponent),
      multi: true,
    },
  ],
})
/**
 * Saisie d'un n° de téléphone et reformatage (sauf dans le cas d'utilisation d'un pattern)
 * @since 1.3.0
 */
export class DsfrFormTelComponent extends DsfrFormInputComponent implements OnInit {
  /** @internal */ readonly errorMessage = this.i18n.t('tel.error');

  /** @internal */
  constructor(private i18n: I18nService) {
    super();
  }

  /**
   * Au fil de la saisie, on ne vérifie le n° de téléphone que s'il y avait déjà une erreur
   */
  @HostListener('input') onValueChange() {
    if (this.error) this.validate();
  }

  /**
   * Vérification quand on sort du champ
   */
  @HostListener('change') onFocusOut() {
    if (!this.pattern && this.validate()) this.value = formatPhoneNumber(this.value);
  }

  /** @internal*/
  ngOnInit() {
    super.ngOnInit();
    this.type = DsfrInputTypeConst.TEL;
    this.spellCheck = false;
    this.label ??= this.i18n.t('tel.label');
    this.hint ??= this.i18n.t('tel.hint');
  }

  /**
   * Méthode validation, positionne la propriété error s'il y a lieu.
   */
  private validate(): boolean {
    const isValid = isPhoneNumberValid(this.value, this.pattern);
    this.error = isValid ? '' : this.errorMessage;
    return isValid;
  }
}
