import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormRadioModule } from '../form-radio.module';
import { DemoRadioGroupsComponent } from './demo-radio-groups.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Radio',
  component: DemoRadioGroupsComponent,
  decorators: [moduleMetadata({ imports: [DsfrFormRadioModule, FormsModule] })],
};
export default meta;
type Story = StoryObj<DemoRadioGroupsComponent>;

// issue #360
export const Groups: Story = {
  decorators: dsfrDecorator('2 goupes de boutons radio avec les mêmes valeurs'),
  args: {
    name1: 'radio1',
    options1: [
      { label: 'Label 1', value: '1' },
      { label: 'Label 2', value: '2' },
    ],

    name2: 'radio2',
    options2: [
      { label: 'Label 1', value: '1' },
      { label: 'Label 2', value: '2' },
    ],
  },
};
