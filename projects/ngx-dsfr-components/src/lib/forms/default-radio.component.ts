import { Component, Input, OnInit } from '@angular/core';
import { DefaultControlComponent, newUniqueId } from '../shared';

@Component({ template: '' })
export abstract class DefaultRadioComponent extends DefaultControlComponent<any> implements OnInit {
  /*** Bascule l'affichage des radio-buttons en ligne. */
  @Input() inline = false;

  /** Message d'erreur, quand il est présent les couleurs changent. */
  @Input() error: string;

  /** Message de validation, quand il est présent les couleurs changent. */
  @Input() valid: string;

  /** Attribut required du radio-button. */
  @Input() required = false;

  /** Permet de rétablir une graisse standard sur la légende. */
  @Input() legendRegular = false;

  get fieldsetLabelledBy(): string {
    return this.legendId + ' ' + this.inputId + '-messages';
  }

  get hintId(): string {
    return this.inputId + '-hint';
  }

  get legendId(): string {
    return this.inputId + '-legend';
  }

  /** Légende du groupe de boutons radios (alias sur la propriété label). */
  @Input() set legend(legend: string) {
    this.label = legend;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.name ??= newUniqueId();
  }

  /** @internal */
  getRadioId(index: number): string {
    return `${this.name}_${index + 1}`;
  }
}
