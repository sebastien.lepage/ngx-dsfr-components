import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrFormFieldsetElementDirective } from './form-fieldset-element.directive';
import { DsfrFormFieldsetComponent } from './form-fieldset.component';

@NgModule({
  imports: [CommonModule],
  declarations: [DsfrFormFieldsetComponent, DsfrFormFieldsetElementDirective],
  exports: [DsfrFormFieldsetComponent, DsfrFormFieldsetElementDirective],
})
export class DsfrFormFieldsetModule {}
