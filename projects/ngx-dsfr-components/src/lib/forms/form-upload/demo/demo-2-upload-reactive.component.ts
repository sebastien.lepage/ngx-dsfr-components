import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DsfrUploadModule } from '../upload.module';

@Component({
  selector: 'demo-upload-reactive',
  template: `
    <form [formGroup]="formGroup">
      <dsfr-form-upload
        formControlName="files"
        [label]="label"
        [hint]="hint"
        [error]="error"
        [multiple]="multiple"
        [accept]="accept"
        (fileSelect)="onFileSelect()">
      </dsfr-form-upload>

      <div class="sb-smaller">Valeur du contrôle : {{ getControlValue() | json }}</div>
      <div class="sb-smaller">Fichier(s) : {{ getFilesNames() }}</div>
      <div class="sb-smaller">Type : {{ getControlType() }}</div>
      <dsfr-button *ngIf="reactiveFormReset" label="Reset" (click)="reset()"></dsfr-button>
    </form>
  `,
  styles: [
    `
      dsfr-button {
        display: inline-block;
        margin-top: 30px;
      }
    `,
  ],
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrUploadModule, DsfrButtonModule],
})
export class DemoUploadReactiveComponent implements OnChanges {
  @Input() label: string;
  @Input() hint: string | undefined;
  @Input() error: string;
  @Input() multiple: boolean;
  @Input() accept: string;
  @Input() disabled: boolean;
  @Input() withDefaultValue = false;
  @Input() reactiveFormReset = false;

  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      files: '1234',
    });
  }

  ngOnChanges({ withDefaultValue }: SimpleChanges): void {
    if (withDefaultValue) {
      this.formGroup = this.fb.group({
        files: this.getDefaultFileList(),
      });
      // this.formGroup.controls['files'].setValue(this.getDefaultFileList());
    }
  }

  /** @internal */
  getControlValue() {
    return this.formGroup.controls['files']?.value;
  }

  /** @internal */
  getControlType(): string {
    const value = this.getControlValue();
    return Object.prototype.toString.call(value);
  }

  /** @internal */
  getFilesNames(): string {
    const fileList = this.formGroup.controls['files']?.value;

    if (fileList && fileList.length > 0 && fileList.item) {
      return [...fileList].map((f) => f.name).join(', ');
    } else {
      return fileList;
    }
  }

  /**
   * Perform reset action.
   * @internal
   */
  reset() {
    this.formGroup.controls['files'].reset();
  }

  /** @internal */
  onFileSelect() {
    var empty = this.formGroup.controls['files']?.value?.length === 0;

    if (empty) this.formGroup.controls['files'].reset();
  }

  private getDefaultFileList(): FileList | '' {
    if (this.withDefaultValue) {
      const myFile1 = new File(['Hello World!'], 'myFile1.txt', {
        type: 'text/plain',
        lastModified: new Date().getTime(),
      });
      const myFile2 = new File(['Hello World!'], 'myFile2.txt', {
        type: 'text/plain',
        lastModified: new Date().getTime(),
      });

      // Now let's create a DataTransfer to get a FileList
      const dataTransfer = new DataTransfer();
      dataTransfer.items.add(myFile1);
      // dataTransfer.items.add(myFile2);
      return dataTransfer.files;
    }
    return '';
  }
}
