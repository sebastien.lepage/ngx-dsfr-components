import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrUploadComponent } from './upload.component';

@NgModule({
  declarations: [DsfrUploadComponent],
  exports: [DsfrUploadComponent],
  imports: [CommonModule, FormsModule],
})
export class DsfrUploadModule {}
