import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { Component, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormSelectComponent } from './form-select.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-form-select
    [hint]="'texte description'"
    [options]="options"
    [(ngModel)]="val"
    [inputId]="'test'"></dsfr-form-select>`,
})
class TestHostComponent {
  @ViewChild(DsfrFormSelectComponent)
  public selectComponent: DsfrFormSelectComponent;

  val: 'valeur';
  options = [
    { label: 'Option 1', value: 1, disabled: true },
    { label: 'Option 2', value: 2 },
    { label: 'Option 3', value: 3 },
    { label: 'Option 4', value: 4 },
  ];
}

describe('DsfrFormSelectComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [DsfrFormSelectComponent, TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should generate id', () => {
    testHostComponent.selectComponent.inputId = '';
    testHostComponent.selectComponent.ngOnInit();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('select').getAttribute('id')).toBeDefined();
    expect(fixture.nativeElement.querySelector('select').getAttribute('aria-disabled')).toBeFalsy();
    expect(fixture.nativeElement.disabled).toBeFalsy();
  });

  it('should select second option', () => {
    testHostComponent.selectComponent.value = 2;
    testHostComponent.selectComponent.ngOnInit();
    fixture.detectChanges();
    const selectEl: HTMLElement = fixture.nativeElement.querySelector('select');
    expect(selectEl.children.length).toEqual(4);
    expect(selectEl.children[2].getAttribute('aria-selected')).toEqual('false');
    expect(selectEl.children[1].getAttribute('aria-selected')).toEqual('true');
  });

  it('should not select second option', () => {
    testHostComponent.selectComponent.value = '2';
    fixture.detectChanges();
    const selectEl: HTMLElement = fixture.nativeElement.querySelector('select');
    expect(selectEl.children[2].getAttribute('aria-selected')).toEqual('false');
    expect(selectEl.children[1].getAttribute('aria-selected')).toEqual('false');
  });

  it('should have placeholder as selected option', () => {
    testHostComponent.selectComponent.value = undefined;
    testHostComponent.selectComponent.placeholder = 'Sélectionner';
    fixture.detectChanges();
    const selectEl: HTMLElement = fixture.nativeElement.querySelector('select');
    expect(selectEl.children.length).toEqual(5);
    expect(selectEl.children[0].getAttribute('selected')).toBeDefined();
    expect(selectEl.children[0].textContent).toEqual('Sélectionner');
  });

  it('should select third option after click and emit selectChange', fakeAsync(() => {
    testHostComponent.selectComponent.value = 1;
    fixture.detectChanges();
    const selectEl = fixture.nativeElement.querySelector('select');
    const mockOutput = jest.spyOn(testHostComponent.selectComponent.selectChange, 'emit');
    selectEl.value = selectEl.options[2].value;
    selectEl.dispatchEvent(new Event('change'));
    tick();
    fixture.detectChanges();
    expect(mockOutput).toHaveBeenCalledWith(3);
    expect(testHostComponent.selectComponent.value).toEqual(3);
    expect(selectEl.children[2].getAttribute('aria-selected')).toEqual('true');
  }));
});
