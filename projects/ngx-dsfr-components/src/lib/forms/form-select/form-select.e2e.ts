import { expect, test } from '@playwright/test';

test('select.default', async ({ page }) => {
  await page.goto('?path=/story/forms-select--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const option = await frame.locator('select > option[selected]').first();
  await expect(option).toBeDefined();

  const texte = await option.innerText();
  expect(texte).toBe('Sélectionnez une option');
});

test('select.initial-value', async ({ page }) => {
  await page.goto('?path=/story/forms-select--initial-value');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const option = await frame.locator(`select > option[aria-selected="true"]`).first();
  await expect(option).toBeDefined();

  const texte = await option.innerText();
  expect(texte.trim()).toBe('Option 2');
});
