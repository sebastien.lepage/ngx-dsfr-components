import { DsfrOption } from '../../shared';

export interface DsfrSelectOption extends DsfrOption {
  disabled?: boolean;

  /**
   * Options regroupées.
   */
  options?: DsfrSelectOption[];
}
