import { Component, forwardRef, Input, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  DefaultControlComponent,
  DsfrPosition,
  DsfrPositionConst,
  DsfrSeverity,
  I18nService,
  newUniqueId,
} from '../../shared';

@Component({
  selector: 'dsfr-form-toggle',
  templateUrl: './form-toggle.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormToggleComponent),
      multi: true,
    },
  ],
})
export class DsfrFormToggleComponent extends DefaultControlComponent<boolean> implements OnChanges {
  /** Position du libellé, à droite par défaut. */
  @Input() labelPosition: DsfrPosition = DsfrPositionConst.RIGHT;

  /**
   * Permet d'afficher le libellé, court, décrivant l'état de l’interrupteur (activé / désactivé), placé en dessous du
   * switch. Il est affiché par défaut, il est conseillé de le mettre afin de faciliter la compréhension de
   * l’utilisateur - Optionnel.
   */
  @Input() showStatusLabel = true;

  /**
   * Propriété permettant de surcharger le libellé, court, dénotant l'état checked du toggle, sans avoir à passer
   * par le fichier d'internationalisation.
   */
  @Input() checkedStatusLabel: string;

  /**
   * Propriété permettant de surcharger le libellé court dénotant l'état checked du toggle, sans avoir à passer
   * par le fichier d'internationalisation.
   */
  @Input() uncheckedStatusLabel: string;

  /**
   * Affiche un séparateur horizontal sous le composant.
   * Ne devrait être utilisé que dans le cadre d'un toggles-group.
   */
  @Input() showSeparator: boolean;

  /**
   * Symbolise l'erreur du toggle
   * @deprecated since 1.12 use `message` and `severity` instead
   */
  @Input() error: boolean;

  /**
   * Symbolise le succès du toggle
   * @deprecated since 1.12 use `message` and `severity` instead
   */
  @Input() valid: boolean;

  /**
   * Message d'information lié au composant @since 1.12
   */
  @Input() message: string | undefined = undefined;

  /**
   * Représente la sévérité du message, `warning` n'est pas supporté
   */
  @Input() messageSeverity: DsfrSeverity;

  /**
   * Id de la div affichant les messages d'erreur ou de validation.
   * @internal
   */
  @Input() messagesGroupId = newUniqueId();

  protected ariaDescribedbyValue: string | undefined;

  constructor(private i18n: I18nService) {
    super();
    //FIXME: Au niveau de `DefaultControlComponent`, déplacer l'initialisation de `inputId` dans le constructor.
    this.inputId = this.inputId || newUniqueId();
  }

  get hintId() {
    return this.inputId + '-hint';
  }

  /** @deprecated since 1.12 use showStatus instead */
  get showCheckedHint(): boolean {
    return this.showStatusLabel;
  }

  /** @deprecated since 1.12 use checkedLabel instead */
  get checkedHintLabel(): string {
    return this.checkedStatusLabel;
  }

  /** @deprecated since 1.12 use uncheckedLabel instead */
  get uncheckedHintLabel(): string {
    return this.uncheckedStatusLabel;
  }

  /** @deprecated since 1.12 use showStatus instead */
  @Input() set showCheckedHint(value: boolean) {
    this.showStatusLabel = value;
  }

  /** @deprecated since 1.12 use checkedLabel instead */
  @Input() set checkedHintLabel(value: string) {
    this.checkedStatusLabel = value;
  }

  /** @deprecated since 1.12 use uncheckedLabel instead */
  @Input() set uncheckedHintLabel(value: string) {
    this.uncheckedStatusLabel = value;
  }

  ngOnChanges({ message, hint }: SimpleChanges): void {
    if (message || hint) {
      if (this.hint && this.message) {
        this.ariaDescribedbyValue = this.messagesGroupId + ' ' + this.hintId;
      } else if (this.message) {
        this.ariaDescribedbyValue = this.messagesGroupId;
      } else if (this.hint) {
        this.ariaDescribedbyValue = this.hintId;
      } else {
        this.ariaDescribedbyValue = undefined;
      }
    }
  }

  /**
   * Permet d'initialiser de forcer la valeur initiale à une valeur booléenne
   * @internal
   */
  writeValue(value: boolean | undefined) {
    super.writeValue(value ?? false);
  }

  /**
   * @ignore
   */
  getDataLabelChecked(): string | null {
    return this.showStatusLabel ? this.checkedStatusLabel || this.i18n.t('toggle.dataLabelChecked') : null;
  }

  /**
   * @ignore
   */
  getDataLabelUnchecked(): string | null {
    return this.showStatusLabel ? this.uncheckedStatusLabel || this.i18n.t('toggle.dataLabelUnchecked') : null;
  }

  /** @internal */
  protected hasError(): boolean {
    return this.error || (this.messageSeverity === 'error' && !!this.message);
  }

  /** @internal */
  protected hasSuccess(): boolean {
    return this.valid || (this.messageSeverity === 'success' && !!this.message);
  }

  /** @internal */
  protected hasMessage(severity: DsfrSeverity): boolean {
    return !!this.message && severity === this.messageSeverity;
  }
}
