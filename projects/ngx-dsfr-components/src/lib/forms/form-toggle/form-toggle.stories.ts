import { dsfrDecorator, optionsPosition } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrSeverityConst } from '../../shared';
import { DsfrFormToggleComponent } from './form-toggle.component';

const meta: Meta = {
  title: 'FORMS/Toggle',
  component: DsfrFormToggleComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  argTypes: {
    labelPosition: { control: 'inline-radio', options: optionsPosition },
    messageSeverity: { control: 'inline-radio', options: Object.values(DsfrSeverityConst) },
    value: { control: { type: 'boolean' } },
    id: { table: { disable: true } },
    error: { table: { disable: true } },
    valid: { table: { disable: true } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormToggleComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Toggle simple'),
  args: {
    label: "Libellé de l'interrupteur",
    labelPosition: 'right',
    value: false,
    hint: '',
    disabled: false,
    message: '',
    showSeparator: false,
    checkedStatusLabel: '',
    uncheckedStatusLabel: '',
    showStatusLabel: false,
    ariaControls: '',
    name: '',
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Toggle + texte d’aide'),
  args: {
    ...Default.args,
    hint: 'Texte de description additionnel',
    showSeparator: false,
  },
};

export const Status: Story = {
  decorators: dsfrDecorator('Toggle + état'),
  args: {
    ...Hint.args,
    showStatusLabel: true,
  },
};

export const Checked: Story = {
  decorators: dsfrDecorator('Toggle pré-coché'),
  args: {
    ...Status.args,
    value: true,
  },
};

export const Separator: Story = {
  decorators: dsfrDecorator('Toggle + séparateur'),
  args: {
    ...Checked.args,
    showSeparator: true,
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Toggle disabled'),
  args: {
    ...Checked.args,
    disabled: true,
  },
};

export const Error: Story = {
  decorators: dsfrDecorator('Toggle en erreur'),
  args: {
    ...Default.args,
    messageSeverity: 'success',
    message: 'Texte d’erreur obligatoire',
  },
};

export const Valid: Story = {
  decorators: dsfrDecorator('Toggle en erreur'),
  args: {
    ...Default.args,
    messageSeverity: 'success',
    message: 'Texte de validation',
    inputId: 'valid-toggle',
  },
};

export const Info: Story = {
  decorators: dsfrDecorator('Toggle en erreur'),
  args: {
    ...Default.args,
    messageSeverity: 'info',
    message: "Texte d'information",
  },
};

export const NgModel: Story = {
  name: 'NgModel',
  decorators: dsfrDecorator('Utilisation de ngModel'),
  args: {
    ...Checked.args,
  },
  render: (args) => ({
    props: args,
    template: `
    <dsfr-form-toggle [(ngModel)]="value" [label]="label"></dsfr-form-toggle>
    <div class="sb-smaller">Valeur du modèle : {{ value }}</div>
  `,
  }),
};
