import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrFormToggleComponent } from './form-toggle.component';
import { DsfrFormToggleModule } from './form-toggle.module';

const DEFAULT_LABEL = "Libellé de l'interrupteur";

describe('DsfrFormToggleComponentTest', () => {
  let fixture: ComponentFixture<DsfrFormToggleComponent>;
  let toggleComponent: DsfrFormToggleComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrFormToggleModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DsfrFormToggleComponent);
    toggleComponent = fixture.componentInstance;
    // Valeurs par défaut
    toggleComponent.label = DEFAULT_LABEL;
    toggleComponent.showStatusLabel = true;
    // Prise en compte du changement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(toggleComponent).not.toBeNull();
  });

  it('default', () => {
    const labelElt = fixture.nativeElement.querySelector('.fr-toggle__label');
    expect(labelElt).not.toBeNull();
    expect(labelElt.textContent).toEqual(DEFAULT_LABEL);

    // 4 attributs : class, data-fr-checked-label, data-fr-unchecked-label
    let attribute = labelElt.getAttribute('for');
    expect(attribute).toEqual(toggleComponent.inputId);
    attribute = labelElt.getAttribute('data-fr-checked-label');
    expect(attribute).toEqual('Activé');
    attribute = labelElt.getAttribute('data-fr-unchecked-label');
    expect(attribute).toEqual('Désactivé');
  });

  it('no status', () => {
    toggleComponent.showStatusLabel = false;
    fixture.detectChanges();

    const labelElt = fixture.nativeElement.querySelector('.fr-toggle__label');
    // 2 attributs : class, for
    let attribute = labelElt.getAttribute('data-fr-checked-label');
    expect(attribute).toBeNull();
    attribute = labelElt.getAttribute('data-fr-unchecked-label');
    expect(attribute).toBeNull();
  });

  it('with hint and attribute aria-describedby', () => {
    toggleComponent.hint = 'Texte de description additionnel';
    toggleComponent.inputId = 'foobar';
    toggleComponent.ngOnChanges({ hint: new SimpleChange(undefined, toggleComponent.hint, true) });
    fixture.detectChanges();

    const hintElt = fixture.nativeElement.querySelector('.fr-hint-text');
    expect(hintElt).not.toBeNull();
    expect(hintElt.tagName).toEqual('P');

    const inputElt = fixture.nativeElement.querySelector('input');
    expect(inputElt).not.toBeNull();
    let attribute = inputElt.getAttribute('aria-describedby');
    expect(attribute).not.toBeNull();
    expect(attribute).toEqual(toggleComponent.inputId + '-hint');
  });

  it('error', () => {
    toggleComponent.message = 'Texte d’erreur obligatoire';
    toggleComponent.messageSeverity = 'error';
    fixture.detectChanges();

    const divElt = fixture.nativeElement.querySelector('.fr-toggle');
    expect(divElt).not.toBeNull();
    expect(divElt.classList).toContain('fr-toggle--error');

    const messageElt = fixture.nativeElement.querySelector('.fr-message');
    expect(messageElt).not.toBeNull();
    expect(messageElt.classList).toContain('fr-message--error');
  });

  it('valid', () => {
    toggleComponent.message = 'Texte de validation';
    toggleComponent.messageSeverity = 'success';
    fixture.detectChanges();

    const divElt = fixture.nativeElement.querySelector('.fr-toggle');
    expect(divElt.classList).toContain('fr-toggle--valid');

    const messageElt = fixture.nativeElement.querySelector('.fr-message');
    expect(messageElt.classList).toContain('fr-message--valid');
  });

  it('labeled left', () => {
    toggleComponent.labelPosition = 'left';
    fixture.detectChanges();

    const divElt = fixture.nativeElement.querySelector('.fr-toggle');
    expect(divElt.classList).toContain('fr-toggle--label-left');
  });
});
