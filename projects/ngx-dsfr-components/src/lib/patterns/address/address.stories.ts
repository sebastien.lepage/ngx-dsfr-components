import { dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormInputModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrInputText } from '../../shared/models/input.model';
import { DsfrAddressComponent, DsfrAddressDetails } from './address.component';

const meta: Meta = {
  title: 'PATTERNS/Address',
  component: DsfrAddressComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrFormInputModule, DsfrFormFieldsetModule, FormsModule],
    }),
  ],
};
export default meta;

type Story = StoryObj<DsfrAddressComponent>;

let myAddress: DsfrInputText = { name: '', label: '', value: "Rue de l'europe" };
let myZipCode: DsfrInputText = { name: '', label: '', value: '' };
let myCity: DsfrInputText = { name: '', label: '', value: '' };

export const Default: Story = {
  args: {
    address: myAddress,
    zipCode: myZipCode,
    city: myCity,
  },
  decorators: dsfrDecorator('Demande d’une adresse postale nationale'),
  render: (args) => ({
    props: { ...args, myAddress, myZipCode, myCity },
    template: `
      <dsfr-address
        [address]="myAddress"
        [zipCode]="myZipCode"
        [city]="myCity">
      </dsfr-address>
      <div>myAddress.value : <code>{{ myAddress.value }}</code></div>
      <div>myZipCode.value : <code>{{ myZipCode.value }}</code></div>
      <div>myCity.value : <code>{{ myCity.value }}</code></div>
      `,
  }),
};

/** Avec champ supplémentaire */
let myAddressSupplement: DsfrInputText = { name: '', label: '', value: '' };

export const WithSupplement: Story = {
  args: {
    ...Default.args,
    addressSupplement: myAddressSupplement,
  },
  decorators: dsfrDecorator('Demande d’une adresse postale avec complément optionnel'),
};

/** Avec lieux-dit */
let myAddressLocality: DsfrInputText = { name: '', label: '', value: '' };

export const WithLocality: Story = {
  args: {
    ...Default.args,
    addressSupplement: myAddressSupplement,
    locality: myAddressLocality,
  },
  decorators: dsfrDecorator('Demande d’une adresse postale nationale + Lieu-dit, commune déléguée ou boîte postale'),
};

/** Avec compléments d'adresse */
let myAddressDetails: DsfrAddressDetails = { number: { label: '', name: '', value: '' } };

export const WithAddressDetails: Story = {
  args: {
    ...Default.args,
    addressDetails: myAddressDetails,
  },
  decorators: dsfrDecorator("Demande d’une adresse postale nationale + Complément d'adresse"),
};
