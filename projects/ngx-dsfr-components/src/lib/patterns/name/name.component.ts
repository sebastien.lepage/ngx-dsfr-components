import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrInputText } from '../../shared/models/input.model';
import { DsfrSelect } from '../../shared/models/select.model';

@Component({
  selector: 'dsfr-name',
  templateUrl: './name.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrNameComponent {
  /**
   * Légende du fieldset.
   */
  @Input() legend: string;

  /**
   * Data du champ name.
   */
  @Input({ required: true }) lastName: DsfrInputText;

  /**
   * Permet d'indiquer un nom d'usage (optionnel).
   */
  @Input() usualName: DsfrInputText;

  /**
   * Data du champ firstname.
   */
  @Input({ required: true }) firstNames: DsfrInputText[];

  /**
   * Permet d'afficher l'option "Je n'ai pas de prénom".
   */
  @Input() noFirstName = false;

  /**
   * Permet d'afficher un bouton qui ajoute un nouveau champ de de saisi pour un prénom additionnel.
   */
  @Input() addFirstName = false;

  /**
   * Data du selecteur de pays (optionnel).
   */
  @Input() country: DsfrSelect;

  /**
   * Notifie que l'utilisateur a cliqué sur le bouton d'ajout de prénom.
   */
  @Output() addFirstNameSelect: EventEmitter<string> = new EventEmitter();

  /**
   * Notifie la suppression d'un prénom additionnel (le tableau 'firstnames' devrait être mis à jour en conséquence).
   */
  @Output() deleteFirstNameSelect: EventEmitter<number> = new EventEmitter();

  /** @internal */
  noFirstNameModel: boolean;

  /** @internal */
  onAddFirstNameInput(): void {
    this.addFirstNameSelect.emit();
  }

  /** @internal */
  onDeleteFirstNameInput(index: number): void {
    this.deleteFirstNameSelect.emit(index);
  }
}
