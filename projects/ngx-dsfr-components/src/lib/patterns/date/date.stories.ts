import { argEventEmitter } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormFieldsetModule, DsfrFormInputModule } from '../../forms';
import { DsfrDateComponent } from './date.component';

const meta: Meta = {
  title: 'PATTERNS/Date',
  component: DsfrDateComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrFormFieldsetModule, DsfrFormInputModule] })],
  argTypes: {
    value: { control: { type: 'date' } },
    dateChange: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrDateComponent>;

const template = `
<dsfr-date
  [disabled]="disabled"
  [autocomplete]="autocomplete"
  [error]="error"
  [hint]="hint"
  [legend]="legend"
  [required]="required"
  [valid]="valid"
  [(ngModel)]="value"
></dsfr-date>`;
const modelTemplate = template + `<div class="sb-smaller">Valeur du modèle : {{ value?.toISOString() }}</div>`;

export const Default: Story = {
  args: {
    value: undefined,
    legend: "Date de l'événement",
    disabled: false,
    autocomplete: false,
    error: '',
    hint: '',
    required: false,
    valid: '',
  },
  render: (args) => ({
    props: args,
    template: modelTemplate,
  }),
};
