import { LoggerService } from '../../shared/services/logger.service';
import { DateUtils } from '../../shared/utils/date-utils';

export const DATE_ERROR = {
  INVALID_FORMAT: 'err_invalid_format',
  INVALID_DAY: 'err_invalid_day',
  INVALID_MONTH: 'err_invalid_month',
  INVALID_DATE: 'err_invalid_date',
  REQUIRED: 'err_required',
};

/**
 * Cette classe représente les valeurs saisies par un utilisateur sur 3 champs distincts, jour, mois année.
 * 🔥 Bien que ces propriétés soient en théorie des nombres, à l'exécution, on reçoit des strings.
 * - Chaque valeur est initialisée à `undefined` mais peut aussi avoir la valeur "" (chaine vide).
 * 👆 Par défaut la date est une date UTC.
 * L'objectif de la classe est de :
 * - Gérer des dates saisies par l'utilisateur, potentiellement avec des valeurs `undefined` ou ""
 * - Manipuler les mois de 1 à 12 (et non de 0 à 11)
 * - D'encapsuler l'api [Date](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date) et plus tard l'API Temporal
 */
export class DateModel {
  // Une date est le nombre de millisecondes écoulées depuis le premier janvier 1970 sur l'échelle UTC (idem epoch UNIX)
  // On distingue les dates relatives au temps universal coordonné (UTC) du temps de la machine de l'utilisateur.
  fullYear: number | undefined;
  monthNum: number | undefined;
  dayNum: number | undefined;
  private isDateUtc = true;

  constructor(fullYear?: number, monthNum?: number, dayNum?: number) {
    this.fullYear = fullYear;
    this.monthNum = monthNum;
    this.dayNum = dayNum;

    // Les valeurs de 0 à 99 correspondent aux années 1900 à 1999 [MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/Date)
    if (this.fullYear && 0 <= this.fullYear && this.fullYear < 100) this.fullYear += 1900;
  }

  private get month(): number {
    return this.monthNum ? this.monthNum - 1 : -1;
  }

  /**
   * @param value : 3 formes basiques pour utiliser la méthode
   * - string : Une chaîne de caractères qui représente une date, selon le format reconnu par la méthode `Date.parse()`
   * - number : Une valeur entière qui représente le nombre de millisecondes depuis le premier janvier 1970
   */
  static of(value: string | number | Date | undefined | null, loggerService: LoggerService): DateModel {
    const date = DateUtils.dateUtcOf(value);
    if (!date && typeof value === 'string') loggerService.warn(`La date '${value}' n'est pas valide`);
    return !date ? new DateModel() : new DateModel(date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate());
  }

  isValid(): boolean {
    return this.toDate() !== undefined;
  }

  toDate(): Date | undefined {
    let date = undefined;
    let fullYear = Number(this.fullYear);
    let monthNum = Number(this.monthNum);
    let dayNum = Number(this.dayNum);
    // on vérifie quand même qu'on a bien affaire à des nombres
    if (
      fullYear &&
      monthNum &&
      dayNum &&
      !isNaN(fullYear) &&
      !isNaN(monthNum) &&
      !isNaN(dayNum) &&
      1 <= monthNum &&
      monthNum <= 12 &&
      1 <= dayNum &&
      dayNum <= 31
    ) {
      date = this.isDateUtc ? new Date(Date.UTC(fullYear, this.month, dayNum)) : new Date(fullYear, this.month, dayNum);

      // Date inexistante
      if (date.getFullYear() !== fullYear || date.getMonth() !== this.month || date.getDate() !== dayNum)
        date = undefined;
    }
    return date;
  }

  /**
   * Valide le model.
   * @param required indique si la date est requise ou non
   * @return une liste de codes d'erreur ou tableau vide
   */
  validate(required = false): string[] {
    const errors = [];
    const dayNum = this.dayNum;
    const monthNum = this.monthNum;
    const fullYear = this.fullYear;

    // Format
    if (!this.isNumber(dayNum) || !this.isNumber(monthNum) || !this.isNumber(fullYear)) {
      errors.push(DATE_ERROR.INVALID_FORMAT);
    }

    // Erreur sur le jour, le mois
    else {
      if (dayNum && (dayNum < 1 || dayNum > 31)) errors.push(DATE_ERROR.INVALID_DAY);
      if (monthNum && (monthNum < 1 || monthNum > 12)) errors.push(DATE_ERROR.INVALID_MONTH);
    }

    // All touched : erreur sur la date elle-même
    if (errors.length === 0 && this.allTouched()) {
      // Tous les champs sont remplis, on vérifie la date
      if (this.allFilled()) {
        const value = this.toDate();
        if (!value) errors.push(DATE_ERROR.INVALID_DATE);
      }
      // Au moins 1 champ n'est pas rempli
      else {
        if (required) errors.push(DATE_ERROR.REQUIRED);
      }
    }

    return errors;
  }

  private allTouched(): boolean {
    return this.fullYear !== undefined && this.monthNum !== undefined && this.dayNum !== undefined;
  }

  private allFilled(): boolean {
    return !!this.dayNum && !!this.monthNum && !!this.fullYear;
  }

  /** @return true si undefined ou number */
  private isNumber(value: any) {
    return value === undefined || !isNaN(value);
  }
}
