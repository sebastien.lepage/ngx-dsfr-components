import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DateUtils } from '../../../shared/utils/date-utils';
import { DsfrDateModule } from '../date.module';

@Component({
  selector: 'demo-date-iso',
  templateUrl: './demo-date-iso.component.html',
  styles: ['#isostr {background: rgb(238, 238, 238)}'],
  standalone: true,
  imports: [CommonModule, FormsModule, DsfrDateModule],
})
export class DemoDateIsoComponent {
  @Input() legend: string;
  @Input() hint: string;
  @Input() required: boolean;
  /** @internal */ isoStr = '2024-08-31T23:00:00.000Z';
  /** @internal */ value: any = this.isoStr;
  /** @internal */ error = '';

  /** @internal */
  onIsoChange(isoStr: string) {
    this.error = '';
    this.isoStr = isoStr;
  }

  /** @internal */
  onIsoFocusOut() {
    if (!DateUtils.parseDateIso(this.isoStr)) this.error = 'iso string invalide';
    this.value = this.isoStr;
  }

  /** @internal */
  onModelChange() {
    if (this.value) this.isoStr = (<Date>this.value).toISOString();
  }
}
