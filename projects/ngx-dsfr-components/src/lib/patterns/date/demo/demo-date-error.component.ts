import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrDateModule } from '../date.module';

@Component({
  selector: 'demo-date-error',
  templateUrl: './demo-date-error.component.html',
  standalone: true,
  imports: [CommonModule, FormsModule, DsfrDateModule],
})
export class DemoDateErrorComponent implements OnInit {
  @Input() value: Date;
  @Input() legend: string;
  @Input() hint: string;
  @Input() required: boolean;
  /** @internal */ error: string;

  /** @internal */
  ngOnInit(): void {
    this.validate();
  }

  /** @internal */
  onModelChange() {
    this.validate();
  }

  /** @internal */
  onDateChange(value: Date) {
    // console.log(`onDateChange value: ${value}, type: ${typeof value}`);
  }

  /** @internal */
  getTypeOfValue(): string {
    return typeof this.value;
  }

  /** @internal */
  toIsoString(): string {
    return typeof this.value === 'object' ? (<Date>this.value).toISOString() : this.value;
  }

  /** @internal */
  protected validate() {
    if (!this.value) return;

    this.error = '';
    if (this.value && this.value > new Date(Date.now())) {
      this.error = 'La date doit être inférieure ou égale à la date du jour';
    }
  }
}
