import { Component, EventEmitter, forwardRef, Input, Output, ViewEncapsulation } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator } from '@angular/forms';
import { DefaultValueAccessorComponent, I18nService } from '../../shared';
import { LoggerService } from '../../shared/services/logger.service';
import { DateModel } from './date.model';

const PATH_ERR_I18N = {
  err_invalid_format: 'date.error.invalid.format',
  err_invalid_day: 'date.error.invalid.day',
  err_invalid_month: 'date.error.invalid.month',
  err_invalid_date: 'date.error.invalid.date',
  err_required: 'date.error.required',
};

@Component({
  selector: 'dsfr-date',
  templateUrl: './date.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrDateComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DsfrDateComponent),
      multi: true,
    },
  ],
})
export class DsfrDateComponent extends DefaultValueAccessorComponent<Date> implements Validator {
  /**
   * Indique si les champs de saisie doivent être auto-complétés à partir de la date de naissance de l'utilisateur.
   */
  @Input() autocomplete = false;

  /**
   * Modèle de présentation de données internes.
   */
  @Input() error: string | string[];

  /**
   * Texte de description additionnel du fieldset.
   */
  @Input() hint: string;

  /**
   * Légende du fieldset.
   */
  @Input() legend: string;

  /**
   * Indique si la date est obligatoire.
   */
  @Input() required = false;

  /**
   * Texte de succès.
   */
  @Input() valid: string;

  /**
   * Signale le changement de date.
   */
  // Certainement inutile vs ngModelChange, à voir !
  @Output() dateChange: EventEmitter<Date> = new EventEmitter();

  /** @internal */ dateModel = new DateModel(); // View-Model
  /** @internal */ validationErrors: ValidationErrors | null = null;

  /** @internal */
  constructor(
    public i18n: I18nService,
    private logger: LoggerService,
  ) {
    super();
  }

  get errors(): string | string[] {
    const internalError =
      this.validationErrors === null ? [] : this.validationErrors.map((err: {}) => Object.values(err)[0]);
    return internalError.length > 0 ? internalError : this.error;
  }

  // Sur changement de la date, on réécrit jour mois année
  /** @internal */
  writeValue(value: Date | undefined) {
    // Bien que la value soit de type date, le développeur peut, à notre insu, nous envoyer une chaine ou un nombre (comme Storybook), ex : Date.now()
    this.dateModel = DateModel.of(value, this.logger);
    super.writeValue(this.dateModel.toDate());
    this.dateChange.emit(this.value);
  }

  // Sur changement du modèle, c.-à-d. jour, mois ou année, on récrit la date
  /** @internal */
  onFocusOut() {
    this.validationErrors = this.internalValidate();
    this.value = this.dateModel.toDate();
    this.dateChange.emit(this.value);
  }

  /** @internal */
  registerOnValidatorChange(fn: () => void): void {
    this.fnOnValidatorChange = fn;
  }

  /** @internal */
  validate(control: AbstractControl): ValidationErrors | null {
    return this.internalValidate();
  }

  /** Permet de stocker la fonction pouvant être appelée pour forcer une revalidation. */
  private fnOnValidatorChange = () => {};

  /**
   * Valide le composant et retourne ValidationErrors
   */
  private internalValidate(): ValidationErrors | null {
    const errors = this.dateModel.validate(this.required);

    // Pas d'erreur, on retourne null
    if (errors.length === 0) return null;

    // On transforme la liste des codes d'erreur en ValidationErrors
    return errors.map((codeErr) => {
      // @ts-ignore
      const pathErr = PATH_ERR_I18N[codeErr];
      const labelErr = this.i18n.t(pathErr);
      const validationErr = {};
      // @ts-ignore
      validationErr[codeErr] = labelErr;
      return validationErr;
    });
  }
}
