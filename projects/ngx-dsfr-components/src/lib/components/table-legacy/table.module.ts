import { CommonModule, KeyValuePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrTableLegacyComponent } from './table-legacy.component';

/**
 * @deprecated utiliser le nouveau composant de table
 */
@NgModule({
  declarations: [DsfrTableLegacyComponent],
  exports: [DsfrTableLegacyComponent],
  imports: [CommonModule],
  providers: [KeyValuePipe],
})
export class DsfrTableModule {}
