import { KeyValuePipe } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { I18nService } from '../../shared';
import { DsfrDataTable, EduTableModel } from './table-legacy.model';

/**
 * @deprecated utiliser le nouveau composant de table
 */
@Component({
  selector: 'dsfr-table',
  templateUrl: './table-legacy.component.html',
  styleUrls: ['./table-legacy.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrTableLegacyComponent {
  /** Message si aucune donnée. */
  @Input() noDataMessage: string;

  /**
   * Permet d'empêcher le scroll horizontal. Si le tableau no-scroll comporte trop de colonnes le tableau est coupé
   * sur la droite (overflow hidden).
   */
  @Input() noScroll = false;

  /**
   * Permet de désactiver le scroll en fixant la largeur des colonnes du tableau. Les colonnes sont alors toutes de
   * la même taille quelque soit leur contenu.
   */
  @Input() fixedLayout = false;

  /** Titre au-dessus du tableau. */
  @Input() caption: string;

  /** Permet de masquer (visuellement) le caption afin de l’utiliser uniquement dans le cadre de l’accessibilité. */
  @Input() noCaption = false;

  /** Permet de placer le caption en dessous du tableau plutôt qu’en haut. */
  @Input() captionBottom = false;

  /**
   * Classes de personnalisation du tableau (ex : `fr-table--green-emeraude`).
   *
   * @link https://gouvfr.atlassian.net/wiki/spaces/DB/pages/312016971/Tableau+-+Table#Personnalisation
   */
  @Input() customClass: string;

  /** Classe(s) de personnalisation de l'entête. */
  @Input() headerCustomClass: string;

  /** Permet d'ajouter des bordures aux lignes du tableau.  */
  @Input() bordered = false;

  /** @internal */
  dataModel: EduTableModel = { columns: [], rows: [] };

  constructor(
    private i18n: I18nService,
    private keyValuePipe: KeyValuePipe,
  ) {
    this.noDataMessage = this.i18n.t('table.noData');
  }

  /** Modèle de données.  */
  @Input()
  public set data(data: DsfrDataTable) {
    this.dataModel = new EduTableModel(data, this.keyValuePipe);
  }

  /** @internal */
  hasRows(): boolean {
    return !!this.dataModel && !!this.dataModel.rows && this.dataModel.rows.length > 0;
  }
}
