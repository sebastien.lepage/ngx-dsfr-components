import { dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormRadioModule } from '../../../forms';
import { DsfrButtonModule } from '../../button';
import { DsfrModalModule } from '../modal.module';
import { DemoModalActionsComponent } from './demo-modal-actions.component';

const meta: Meta = {
  title: 'COMPONENTS/Modal',
  component: DemoModalActionsComponent,
  decorators: [moduleMetadata({ imports: [DsfrModalModule, DsfrButtonModule, DsfrFormRadioModule, FormsModule] })],
};
export default meta;
type Story = StoryObj<DemoModalActionsComponent>;

export const Actions: Story = {
  decorators: dsfrDecorator('Modale avec actions'),
  args: { toogleActionResetDisabled: true },
};
