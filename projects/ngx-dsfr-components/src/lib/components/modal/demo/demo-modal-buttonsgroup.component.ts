import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrModalModule } from '../modal.module';

@Component({
  selector: 'demo-modal-buttonsgroup',
  template: `
    <dsfr-buttons-group inline="always">
      <dsfr-button label="Bouton 1"></dsfr-button>
      <dsfr-button label="Bouton 2" ariaControls="myModalId"></dsfr-button>
    </dsfr-buttons-group>

    <dsfr-modal dialogId="myModalId" titleModal="Titre de la modale">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien
        pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis
        volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.
      </p>
    </dsfr-modal>
  `,
  // template: `
  //   <div>
  //     <ul class="fr-btns-group fr-btns-group--inline">
  //       <li>
  //         <div>
  //           <button class="fr-btn" id="button-1381">Label bouton 1</button>
  //         </div>
  //       </li>
  //       <li>
  //         <div>
  //           <button class="fr-btn" id="button-1382" data-fr-opened="false" aria-controls="modal-5447">
  //             Label bouton 2
  //           </button>
  //         </div>
  //       </li>
  //     </ul>
  //   </div>
  //   <dialog id="modal-5447" class="fr-modal" aria-labelledby="modal-5447-title">
  //     <div class="fr-container fr-container--fluid fr-container-md">
  //       <div class="fr-grid-row fr-grid-row--center">
  //         <div class="fr-col-12 fr-col-md-8 fr-col-lg-6">
  //           <div class="fr-modal__body">
  //             <div class="fr-modal__header">
  //               <button class="fr-btn--close fr-btn" aria-controls="modal-5447" id="button-5449" title="Fermer">
  //                 Fermer
  //               </button>
  //             </div>
  //             <div class="fr-modal__content">
  //               <h1 id="modal-5447-title" class="fr-modal__title">
  //                 <span class="fr-icon-arrow-right-line fr-icon--lg" aria-hidden="true"></span>
  //                 Titre de la modale
  //               </h1>
  //               <p>
  //                 Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.
  //                 Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan
  //                 lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet
  //                 consectetur adipiscing elit ut.
  //               </p>
  //             </div>
  //           </div>
  //         </div>
  //       </div>
  //     </div>
  //   </dialog>
  // `,
  standalone: true,
  imports: [CommonModule, DsfrModalModule, DsfrButtonModule, DsfrButtonsGroupModule],
})
export class DemoModalButtonsGroupComponent {}
