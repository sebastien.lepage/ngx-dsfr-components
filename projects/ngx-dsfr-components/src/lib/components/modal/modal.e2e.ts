import { expect, test } from '@playwright/test';

test('modal.btn-close--focused', async ({ page }) => {
  await page.goto('?path=/story/components-modal--default');
  const frame = page.frameLocator('#storybook-preview-iframe');
  const dialog = await frame.locator('dialog');
  await expect(dialog).toBeDefined();
  await expect(dialog).toHaveClass('fr-modal');
  await expect(dialog).toBeHidden();
  const openBtn = await frame.locator('dsfr-button > button[aria-controls="modalDefault"]');
  await expect(openBtn).toBeDefined();
  await expect(openBtn).toHaveAttribute('data-fr-js-modal-button', 'true');
  await openBtn.press('Enter');
  await expect(dialog).toBeVisible();
  await expect(dialog).toHaveClass(/fr-modal--opened/);
  await expect(frame.locator('button.fr-btn--close')).toBeFocused();
});

test('modal.btn-close--focused_programmatic', async ({ page }) => {
  await page.goto('?path=/story/components-modal--programmatic');
  const frame = page.frameLocator('#storybook-preview-iframe');
  const dialog = await frame.locator('dialog');
  await expect(dialog).toBeDefined();
  await expect(dialog).toHaveClass('fr-modal');
  await expect(dialog).toBeHidden();
  const openBtn = await frame.locator('dsfr-button > button');
  await expect(openBtn).toBeDefined();
  await expect(openBtn).not.toHaveAttribute('data-fr-js-modal-button');
  await openBtn.press('Enter');
  await expect(dialog).toBeVisible();
  await expect(dialog).toHaveClass(/fr-modal--opened/);
  await expect(frame.locator('button.fr-btn--close')).toBeFocused();
});
