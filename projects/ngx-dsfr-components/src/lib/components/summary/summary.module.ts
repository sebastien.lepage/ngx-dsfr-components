import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeadingModule } from '../../shared';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrSummaryComponent } from './summary.component';

@NgModule({
  declarations: [DsfrSummaryComponent],
  exports: [DsfrSummaryComponent],
  imports: [CommonModule, RouterModule, HeadingModule, ItemLinkComponent],
})
export class DsfrSummaryModule {}
