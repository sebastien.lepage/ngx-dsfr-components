import { MARIANNE_SVG } from '.storybook/assets/marianne.svg';
import { argEventEmitter, gridDecoratorLG, titleDecorator } from '.storybook/storybook-utils';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrSizeConst } from '../../shared';
import { DsfrLinkModule } from '../link';
import { DsfrTranscriptionComponent } from '../transcription';
import { DsfrContentComponent } from './content.component';
import { DsfrMediaConst } from './content.model';

const meta: Meta = {
  title: 'COMPONENTS/Content',
  component: DsfrContentComponent,
  decorators: [moduleMetadata({ imports: [DsfrTranscriptionComponent, DsfrLinkModule, RouterTestingModule] })],
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
    ratio: { control: { type: 'inline-radio' }, options: ['16:9/2', '16:9', '3:2', '4:3', '1:1', '3:4', '2:3'] },
    type: { control: { type: 'inline-radio' }, options: Object.values(DsfrMediaConst) },
    linkSelect: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrContentComponent>;

const loremIpsum =
  'Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.';
const video = 'https://www.youtube.com/embed/HyirpmPL43I';

export const Default: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image édito')],
  args: {
    type: 'image',
    ratio: '16:9',
    size: 'MD',
    source: 'img/placeholder.16x9.png',
    legend: 'Description / Source',
    link: '?path=/story/components-content--default',
    linkLabel: 'Libellé lien',
  },
};

export const Small: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image édito, petite taille')],
  args: {
    ...Default.args,
    size: 'SM',
    route: 'maRoute',
    linkLabel: "route='maRoute'",
  },
};

export const Large: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image édito, grande taille')],
  args: {
    ...Default.args,
    size: 'LG',
    routePath: '/path',
    linkLabel: "routerLink='/path'",
  },
};

export const Ratio: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image edito ratio 32x9')],
  args: {
    ...Default.args,
    ratio: '16:9/2',
    route: 'maRoute',
  },
};

export const SVG: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image en svg, porteur d’information')],
  args: {
    ...Default.args,
    route: 'maRoute',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-content [legend]="legend" [link]="transcriptionLink" [linkLabel]="transcriptionLinkLabel">
  <ng-container svg>${MARIANNE_SVG}</ng-container>
</dsfr-content>`,
  }),
};

export const Transcription: Story = {
  decorators: [
    gridDecoratorLG,
    titleDecorator(
      'Média image avec une transcription (définie par des propriétés)',
      "La transcription est affichée via les propriétés 'transcriptionContent' et 'transcriptionHeading'",
    ),
  ],
  args: {
    ...Default.args,
    route: 'maRoute',
    transcriptionContent: loremIpsum,
    transcriptionHeading: "Titre de l'image",
  },
};

export const SlotTranscription: Story = {
  decorators: [
    gridDecoratorLG,
    titleDecorator(
      'Média image avec une transcription (projetée)',
      "Composant 'dsfr-transcription' projeté dans le slot 'transcription'",
    ),
  ],
  args: {
    ...Default.args,
    route: 'maRoute',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-content
      [type]="type"
      [legend]="legend"
      [ratio]="ratio"
      [source]="source"
      [linkLabel]="linkLabel"
      [link]="link"
      [linkTarget]="linkTarget"
      [route]="route"
      [routePath]="routePath"
      [transcriptionContent]="transcriptionContent"
      [transcriptionHeading]="transcriptionHeading"
      [alternate]="alternate"
      [tooltipMessage]="tooltipMessage">
  <dsfr-transcription transcription heading="Titre de l'image">
    <div>
      <p>${loremIpsum}</p>
      <ul>
        <li>list item</li>
        <li>list item</li>
        <li>list item</li>
      </ul>
    </div>
  </dsfr-transcription>
</dsfr-content>`,
  }),
};

export const Video: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média video édito, ratio d‘aspect 16/9 par défaut')],
  args: {
    ...Default.args,
    type: 'video',
    route: 'maRoute',
    source: video,
    tooltipMessage: 'tooltip message',
  },
};

export const VideoRatio: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média video édito, ratio d‘aspect 4x3')],
  args: {
    ...Default.args,
    type: 'video',
    ratio: '4:3',
    route: 'maRoute',
    source: video,
    tooltipMessage: 'tooltip message',
  },
};
