import { Meta, StoryObj } from '@storybook/angular';
import { DsfrSizeConst } from '../../shared';
import { DsfrLogoComponent } from './logo.component';

const meta: Meta = {
  title: 'COMPONENTS/Logo',
  component: DsfrLogoComponent,
  argTypes: {
    size: { control: 'inline-radio', options: Object.values(DsfrSizeConst) },
  },
};
export default meta;

type Story = StoryObj<DsfrLogoComponent>;

export const Default: Story = {
  args: {
    label: 'Intitulé<br/>officiel',
  },
};

export const Long: Story = {
  args: {
    label: 'Ministère<br/>de la transition<br/>écologique<br/>et solidaire',
  },
};

export const Slot: Story = {
  render: (args) => ({
    props: args,
    template: `
    <dsfr-logo [size]="size">
        Ministère
        <br>de la transition
        <br>écologique
        <br>et solidaire
    </dsfr-logo>
    `,
  }),
};
