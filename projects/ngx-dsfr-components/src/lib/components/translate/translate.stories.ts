import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj } from '@storybook/angular';
import { DsfrTranslateComponent } from './translate.component';

const meta: Meta = {
  title: 'COMPONENTS/Translate',
  component: DsfrTranslateComponent,
  argTypes: {
    currentLangCode: { control: { type: 'inline-radio' }, options: ['fr', 'en'] },
    langChange: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrTranslateComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Sélecteur de langue'),
  args: {
    languages: [
      { value: 'fr', label: 'Français' },
      { value: 'en', label: 'English' },
    ],
    outline: true,
  },
};

export const NoOutline: Story = {
  decorators: dsfrDecorator('Sélecteur de langue sans bordure'),
  args: {
    ...Default.args,
    outline: false,
  },
};

export const MultipleColumns: Story = {
  decorators: dsfrDecorator('Sélecteur de langue sur plusieurs colonnes'),
  args: {
    ...Default.args,
    languages: [
      { value: 'fr', label: 'Français' },
      { value: 'en', label: 'English' },
      { value: 'es', label: 'Español' },
      { value: 'de', label: 'Deutsch' },
      { value: 'tr', label: 'Türkçe' },
      { value: 'ro', label: 'Română' },
      { value: 'el', label: 'Ἑλληνική' },
      { value: 'zh', label: '國語' },
      { value: 'uk', label: 'Українська' },
    ],
  },
};

export const Link: Story = {
  decorators: dsfrDecorator('Sélecteur de langue avec liens href'),
  args: {
    ...Default.args,
    languages: [
      { value: 'fr', label: 'Français', link: '?path=/story/components-translate--link&args=currentCode:fr' },
      { value: 'en', label: 'English', link: '?path=/story/components-translate--link&args=currentCode:en' },
      { value: 'it', label: 'Italian', link: '?path=/story/components-translate--link&args=currentCode:it' },
    ],
  },
};
