import { argEventEmitter } from '.storybook/storybook-utils';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrPositionConst } from '../../shared';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrMenu } from './menu.model';
import { DsfrSidemenuComponent } from './sidemenu.component';

const meta: Meta = {
  title: 'COMPONENTS/SideMenu',
  component: DsfrSidemenuComponent,
  decorators: [
    moduleMetadata({
      imports: [RouterTestingModule, ItemLinkComponent],
    }),
  ],
  argTypes: {
    itemSelect: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrSidemenuComponent>;

const data: DsfrMenu = {
  title: 'Titre du menu',
  items: [
    {
      label: 'Niveau 1 actif avec sous-menus niv 2 et 3 actifs',
      link: '#',
      active: true,

      subItems: [
        { label: 'Niveau 2 + routerLink', routerLink: '/', active: false },
        {
          label: 'Niveau 2 actif + icône',
          link: '#',
          icon: 'fr-icon-error-line',
          active: true,
          subItems: [
            { label: 'Niveau 3 actif externe', link: '#', active: true, target: '_blank' },
            { label: 'Niveau 3', link: '#' },
          ],
        },
      ],
    },
    {
      label: 'Autre niveau 1 externe',
      target: '_blank',
    },
    {
      label: 'Niveau 1 avec 2 sous-niveaux',
      subItems: [
        { label: 'Niveau 2-A', link: '#' },
        {
          label: 'Niveau 2-B avec titre de rubrique',
          title: 'Titre de rubrique niveau 3',
          subItems: [
            {
              label: 'Niveau 3-A',
              link: '#',
              subItems: [{ label: 'Niveau 4' }],
            },
            { label: 'Niveau 3-B', link: '#' },
            { label: 'Niveau 3-C', link: '#' },
          ],
        },
      ],
    },
  ],
};

export const Default: Story = {
  args: {
    menu: data,
  },
};

export const StickyFullHeightRight: Story = {
  args: {
    position: DsfrPositionConst.RIGHT,
    stickyFullHeight: true,
    menu: data,
  },
};

export const WithIcons: Story = {
  args: {
    menu: {
      items: [
        {
          label: 'Icône Remix',
          routerLink: '/',
          icon: 'fr-icon-checkbox-circle-line',
          active: true,
          subItems: [
            { label: 'Sub item', link: '#' },
            { label: 'Sub item with route', route: 'foo' },
          ],
        },
        {
          label: "En essayant avec une icône FontAwesome cela ne fonctionne pas du coup, c'est normal",
          link: '#',
          icon: 'ri-arrow-right-fill',
        },
      ],
    },
  },
};

export const Active: Story = {
  args: {
    menu: {
      items: [
        {
          label: 'Menu 1',
          route: 'menu1',
          subItems: [
            { label: 'Sub item 1-1', route: 'subitem1-1' },
            { label: 'Sub item 1-2', route: 'subitem1-2' },
          ],
        },
        {
          label: 'Menu 2',
          route: 'menu2',
          active: true,
          subItems: [
            { label: 'Sub item 2-1', route: 'subitem2-1' },
            { label: 'Sub item 2-2', route: 'subitem2-2', active: true },
          ],
        },
      ],
    },
  },
};
