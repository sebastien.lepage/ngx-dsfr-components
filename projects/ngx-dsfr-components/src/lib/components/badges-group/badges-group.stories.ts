import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrBadgeModule } from '../badge';
import { DsfrBadgesGroupComponent } from './badges-group.component';

const meta: Meta = {
  title: 'COMPONENTS/Badges group',
  component: DsfrBadgesGroupComponent,
  decorators: [moduleMetadata({ imports: [DsfrBadgeModule] })],
};
export default meta;
type Story = StoryObj<DsfrBadgesGroupComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Groupe de badges'),
  args: {
    small: false,
    badges: [
      { label: 'Label badge 1' },
      { label: 'Label badge 2' },
      { label: 'Label badge 3' },
      { label: 'Label badge 4' },
      { label: 'Label badge 5', severity: 'error' },
    ],
  },
};

export const Small: Story = {
  decorators: dsfrDecorator('Groupe de badges SM'),
  args: {
    ...Default.args,
    small: true,
  },
};
