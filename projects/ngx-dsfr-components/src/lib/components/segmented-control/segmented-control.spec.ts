import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrSegmentedControlComponent } from './segmented-control.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-segmented-control
    [segments]="segments"
    [legend]="'Légende'"
    [name]="'mySegmentedControl'"
    (segmentedControlSelect)="segmentedControlSelect($event)"></dsfr-segmented-control>`,
})
class TestHostComponent {
  @ViewChild(DsfrSegmentedControlComponent)
  public segmentedControlComponent: DsfrSegmentedControlComponent;
  segments: "[{label: 'Libelle 1', value: '1'}, {label: 'Libelle 2', value: '2', checked: true}, {label: 'Libelle 3', value: '3'}]";
}

describe('DsfrSegmentedControlComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DsfrSegmentedControlComponent, TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(testHostComponent).toBeDefined();
  });

  it('should have label ', () => {
    const segments = [
      { label: 'Libelle 1', value: '1' },
      { label: 'Libelle 2', value: '2', checked: true },
      { label: 'Libelle 3', value: '3' },
    ];
    testHostComponent.segmentedControlComponent.segments = segments;
    fixture.detectChanges();
    const labelEl = fixture.nativeElement.querySelector('.fr-label');
    expect(labelEl?.textContent).toEqual('Libelle 1');
  });

  it('should have hint ', () => {
    testHostComponent.segmentedControlComponent.hint = 'Texte d aide';
    fixture.detectChanges();
    const hintElement: HTMLElement = fixture.nativeElement;
    const p = hintElement.querySelector('.fr-hint-text');
    expect(p?.textContent).toEqual('Texte d aide');
  });
});
