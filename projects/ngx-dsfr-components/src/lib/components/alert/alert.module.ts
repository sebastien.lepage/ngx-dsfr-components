import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrAlertComponent } from './alert.component';

@NgModule({
  declarations: [DsfrAlertComponent],
  exports: [DsfrAlertComponent],
  imports: [CommonModule],
})
export class DsfrAlertModule {}
