import { argEventEmitter, dsfrDecorator, optionsHeadingLevel } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrSeverityConst } from '../../shared';
import { DsfrAlertSizeConst } from './alert.size';
import { DsfrAlertComponent } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Alert',
  component: DsfrAlertComponent,
  decorators: [moduleMetadata({ imports: [] })],
  argTypes: {
    headingLevel: { control: 'inline-radio', options: optionsHeadingLevel },
    hasAriaRole: { control: { type: 'boolean' } },
    severity: { control: 'inline-radio', options: Object.values(DsfrSeverityConst) },
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrAlertSizeConst) },
    closable: { table: { disable: true } }, // @deprecated remplacé par closeable
    conceal: { control: { control: argEventEmitter } },
  },
};
export default meta;
type Story = StoryObj<DsfrAlertComponent>;

const description = 'Description';

export const Default: Story = {
  decorators: dsfrDecorator('Alerte info'),
  args: {
    heading: 'Information Covid',
    message: description,
    severity: 'info',
    size: 'MD',
    closeable: false,
    hasAriaRole: false,
    ariaRoleValue: 'alert',
    headingLevel: undefined,
  },
};

export const Success: Story = {
  decorators: dsfrDecorator('Alerte succès'),
  args: {
    ...Default.args,
    severity: DsfrSeverityConst.SUCCESS,
    heading: "Succès de l'envoi",
    message: description,
  },
};

export const Error: Story = {
  decorators: dsfrDecorator('Alerte erreur'),
  args: {
    ...Default.args,
    severity: DsfrSeverityConst.ERROR,
    heading: 'Erreur détectée dans le formulaire',
    message: description,
  },
};

export const Warning: Story = {
  decorators: dsfrDecorator('Alerte attention'),
  args: {
    ...Default.args,
    severity: DsfrSeverityConst.WARNING,
    heading: 'Attention',
    message: description,
  },
};

export const Closeable: Story = {
  decorators: dsfrDecorator('Alerte avec bouton fermer'),
  args: {
    ...Default.args,
    heading: 'Information Covid',
    message: "Cliquer sur la croix pour fermer l'alerte",
    closeable: true,
  },
};

export const Small: Story = {
  decorators: dsfrDecorator('Alerte taille SM'),
  args: {
    ...Default.args,
    heading: '',
    size: 'SM',
    message: "Information : titre de l'information",
  },
};

export const AriaRoleStatus: Story = {
  decorators: dsfrDecorator(`ARIA <code>role=status</code>`),
  args: {
    ...Default.args,
    heading: 'Statut',
    message: 'Le statut a été modifié',
    hasAriaRole: true,
    ariaRoleValue: 'status',
  },
};

export const TitleSlot: Story = {
  decorators: dsfrDecorator(`Alerte avec titre projeté`),
  args: {
    ...Default.args,
    heading: undefined,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-alert
   [heading]="heading"
   [headingLevel]="headingLevel"
   [message]="message"
   [severity]="severity"
   [ariaRoleValue]="ariaRoleValue"
   [size]="size"
   [closeControlLabel]="closeControlLabel"
   [hasAriaRole]="hasAriaRole"
   [closeable]="closeable"
   >Titre de l'alerte (projeté dans le slot par défaut)</dsfr-alert>`,
  }),
};

export const MessageSlot: Story = {
  decorators: dsfrDecorator(`Alerte avec message projeté`),
  args: {
    ...Default.args,
    message: undefined,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-alert
   [heading]="heading"
   [headingLevel]="headingLevel"
   [severity]="severity"
   [ariaRoleValue]="ariaRoleValue"
   [size]="size"
   [closeControlLabel]="closeControlLabel"
   [hasAriaRole]="hasAriaRole"
   [closeable]="closeable"
   ><p message>Ceci est un message <b>projeté</b></p></dsfr-alert>`,
  }),
};
