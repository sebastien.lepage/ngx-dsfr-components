import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrSkipLinksComponent } from './skiplinks.component';

@NgModule({
  declarations: [DsfrSkipLinksComponent],
  exports: [DsfrSkipLinksComponent],
  imports: [CommonModule, RouterModule],
})
export class DsfrSkiplinksModule {}
