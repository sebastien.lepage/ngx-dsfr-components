import { optionsHeadingLevel } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrDownload, DsfrDownloadModule } from '../download';
import { DsfrDownloadsGroupComponent } from './downloads-group.component';

const meta: Meta = {
  title: 'COMPONENTS/Downloads group',
  component: DsfrDownloadsGroupComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrDownloadModule],
    }),
  ],
  parameters: {},
  argTypes: {
    headingLevel: { control: 'inline-radio', options: optionsHeadingLevel },
  },
};
export default meta;

type Story = StoryObj<DsfrDownloadsGroupComponent>;

const listDownload: DsfrDownload[] = [
  {
    fileName: '1er fichier',
    link: '/img/champollion.jpg',
    mimeType: 'image/jpeg',
    sizeBytes: 1000000,
  },
  {
    fileName: '2e fichier',
    link: '/files/lorem-ipsum.pdf',
    mimeType: 'application/pdf',
    sizeBytes: 100000,
    langCode: 'en',
  },
];

export const Default: Story = {
  args: {
    heading: 'Titre facultatif',
    downloads: listDownload,
  },
};
