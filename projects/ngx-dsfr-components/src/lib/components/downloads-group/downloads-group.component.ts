import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrDownload } from '../download';

@Component({
  selector: 'dsfr-downloads-group',
  templateUrl: './downloads-group.component.html',
  styleUrls: ['./downloads-group.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
/**
 * Ce composant permet de proposer une liste de plusieurs téléchargements. Les téléchargements sont des liens
 * présentés sous forme de liste avec des puces. Un lien ne peut pas être un bloc. La description des liens est faite
 * sous forme de tableau.
 */
export class DsfrDownloadsGroupComponent {
  /** Titre du groupe de fichiers (facultatif). */
  @Input() heading: string;

  /** Niveau de titre, '<h4>' par défaut. */
  @Input() headingLevel: string;

  /** Tableau des fichiers à lister. */
  @Input() downloads: DsfrDownload[] = [];
}
