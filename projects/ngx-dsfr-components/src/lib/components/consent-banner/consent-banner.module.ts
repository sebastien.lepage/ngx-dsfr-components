import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrLinkModule } from '../link';
import { DsfrConsentBannerComponent } from './consent-banner.component';
import { DsfrConsentManagerModule } from './consent-manager/consent-manager.module';

@NgModule({
  declarations: [DsfrConsentBannerComponent],
  exports: [DsfrConsentBannerComponent],
  imports: [CommonModule, FormsModule, DsfrConsentManagerModule, DsfrLinkModule],
})
export class DsfrConsentBannerModule {}
