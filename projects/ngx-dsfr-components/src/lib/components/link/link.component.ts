import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { IsActiveMatchOptions, NavigationExtras } from '@angular/router';
import {
  DsfrLink,
  DsfrLinkTarget,
  DsfrLinkTargetConst,
  DsfrPosition,
  DsfrPositionConst,
  DsfrSize,
  DsfrSizeConst,
  I18nService,
} from '../../shared';

@Component({
  selector: 'dsfr-link',
  templateUrl: './link.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrLinkComponent implements Omit<DsfrLink, 'active' | 'target'>, OnInit {
  /** Permet d'ajouter un attribut `aria-current` sur le lien.*/
  @Input() ariaCurrent: string | undefined;

  /** @since 1.4.0 La propriété ariaLabel définit une valeur de chaîne qui étiquette un élément interactif. */
  @Input() ariaLabel: string | undefined;

  /** @since 1.7 Identifie l'élément (ou les éléments) contrôlé par cet élément. */
  @Input() ariaControls: string | undefined;

  /** Pour ajouter des classes CSS custom. */
  @Input() customClass: string | undefined;

  /**
   * Permet de désactiver le composant.
   */
  @Input() disabled = false;

  /** Classe de l'icône. */
  @Input() icon: string | undefined;

  /** Position de l'icône. À droite par défaut. */
  @Input() iconPosition: DsfrPosition | undefined = DsfrPositionConst.RIGHT;

  /** @since 1.6 */
  @Input() linkId: string | undefined;

  /** Texte du lien. Peut être indiqué via cet input ou via un slot [label].*/
  @Input() label: string;

  /** Lien href externe, exclusif avec route et routePath. */
  @Input() link: string | undefined;

  /** Attribut target du lien. */
  // Le nom 'target' crée un problème avec le CSS DSFR, puisque du coup, on a target="_blank" dans le host et un
  // style s'applique, ce qui provoque un doublement de l'icône lien externe.
  @Input() linkTarget: DsfrLinkTarget | undefined;

  /** Path interne. Exclusif avec link et routePath, prioritaire sur link. */
  @Input() route: string | undefined;

  /** Path angular géré en tant que directive routerLink. Prioritaire et exclusif avec link et route. */
  @Input() routePath: string | string[] | undefined;

  /** Classe CSS utilisée pour la directive routerLink active. */
  @Input() routerLinkActive: string | string[] | undefined;

  /** RouterLink : options additionnelles pour le routerLinkActive (exact). */
  @Input() routerLinkActiveOptions: { exact: boolean } | IsActiveMatchOptions | undefined;

  /** RouterLink : options additionnelles ppour le routerLink (queryParams, state, etc.) */
  @Input() routerLinkExtras: NavigationExtras | undefined;

  /** Taille du lien. */
  @Input() size: DsfrSize = DsfrSizeConst.MD;

  /** Message du tooltip (attribut title). @since 1.3.0 */
  @Input() tooltipMessage: string | undefined;

  /** @since 1.7.0 Transforme un lien en bouton si `button`, `link` par défaut. */
  @Input() mode: 'link' | 'button' | undefined;

  /**
   * Si l'input 'route' est renseigné, sa valeur sera émise lorsque le lien est sélectionné.
   */
  @Output() linkSelect = new EventEmitter<string>();

  constructor(private i18n: I18nService) {}

  /** @deprecated @since 1.5 utiliser `linkTarget` à la place. */
  get targetLink(): DsfrLinkTarget | undefined {
    return this.linkTarget;
  }

  /** @deprecated @since 1.5 utiliser `linkTarget` à la place. */
  @Input() set targetLink(value: DsfrLinkTarget | undefined) {
    this.linkTarget = value;
  }

  /**
   * @deprecated (@since 1.11.5) utiliser `routePath` à la place.
   * routerLink provoque un bug accessibilité sur la navigation au clavier (ajout d'un tabindex=0)
   **/
  @Input() set routerLink(value: string | string[] | undefined) {
    if (value) this.routePath = value;
  }

  ngOnInit(): void {
    if (this.linkTarget === DsfrLinkTargetConst.BLANK) {
      this.tooltipMessage = this.tooltipMessage
        ? ` ${this.tooltipMessage} - ${this.i18n.t('link.newWindow')}`
        : `${this.i18n.t('link.newWindow')}`;
    }
  }

  /** @internal */
  getHref(): string | undefined {
    return this.disabled ? undefined : this.link || this.route;
  }

  /** @internal */
  hasRouterLink(): boolean {
    return !this.disabled && !!this.routePath;
  }

  /** @internal */
  /*
   * DsfrLink est partagé par plusieurs composants, 'tag', 'card' et 'tile' à la place d'un simple lien.
   * Cela permet de bénéficier des nombreuses caractéristiques du composant DsfrLink.
   * @since 1.5 Cependant, la classe 'fr-link' ne doit pas cohabiter les classes de ces composants ('fr-tag', ...)
   */
  getClass() {
    return {
      'fr-link': !this.customClass && !this.disabled,
      'fr-link--icon-right': this.icon && this.iconPosition === DsfrPositionConst.RIGHT,
      'fr-link--icon-left': this.icon && this.iconPosition === DsfrPositionConst.LEFT,
      'fr-link--sm': this.size === DsfrSizeConst.SM,
      'fr-link--lg': this.size === DsfrSizeConst.LG,
    };
  }

  /** @internal */
  getButtonClasses(): string {
    const classes = ['fr-btn', 'fr-btn--tertiary-no-outline'];
    if (this.customClass) classes.push(this.customClass);

    if (this.icon) {
      classes.push(this.icon);
      classes.push(this.iconPosition === DsfrPositionConst.RIGHT ? 'fr-btn--icon-right' : 'fr-btn--icon-left');
    }

    return classes.join(' ');
  }

  /** @internal */
  onLink(event: Event) {
    if (this.route && !this.routePath) {
      event.preventDefault();
      this.linkSelect.emit(this.route);
    } else if (this.link) {
      this.linkSelect.emit(this.link);
    }
  }
}
