import { argEventEmitter, dsfrDecorator, optionsPosition, optionsSize } from '.storybook/storybook-utils';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrLinkTargetConst } from '../../shared';
import { DsfrTooltipDirective } from '../tooltip';
import { DsfrLinkComponent } from './link.component';

const meta: Meta = {
  title: 'COMPONENTS/Link',
  component: DsfrLinkComponent,
  decorators: [moduleMetadata({ imports: [RouterTestingModule, DsfrTooltipDirective] })],
  parameters: {},
  argTypes: {
    size: { control: 'inline-radio', options: optionsSize },
    iconPosition: { control: 'inline-radio', options: optionsPosition },
    targetLink: { control: { type: 'select' }, options: [''].concat(Object.values(DsfrLinkTargetConst)) },
    linkSelect: { control: { control: argEventEmitter } },
  },
};
export default meta;
type Story = StoryObj<DsfrLinkComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Lien seul'),
  args: {
    label: 'Label lien',
    link: '#',
    size: 'MD',
    disabled: false,
    iconPosition: 'right',
    tooltipMessage: 'Lien dans le même onglet',
  },
};

export const IconOnLeft: Story = {
  name: 'Icon on left',
  decorators: dsfrDecorator('Lien avec icône à gauche'),
  args: {
    ...Default.args,
    icon: 'fr-icon-arrow-left-line',
    iconPosition: 'left',
  },
};

export const IconOnRight: Story = {
  name: 'Icon on right',
  decorators: dsfrDecorator('Lien avec icône à droite'),
  args: {
    ...Default.args,
    icon: 'fr-icon-arrow-right-line',
  },
};

export const Small: Story = {
  decorators: dsfrDecorator('Lien seul SM'),
  args: {
    ...Default.args,
    size: 'SM',
  },
};

export const Large: Story = {
  decorators: dsfrDecorator('Lien seul LG'),
  args: {
    ...Default.args,
    size: 'LG',
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Lien seul désactivé'),
  args: {
    ...Default.args,
    disabled: true,
  },
};

export const External: Story = {
  decorators: dsfrDecorator('Lien externe'),
  args: {
    ...Default.args,
    label: 'Lien externe',
    link: 'https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/liens',
    linkTarget: '_blank',
    tooltipMessage: 'Opens the linked document in a new window or tab',
  },
};

export const Tooltip: Story = {
  decorators: dsfrDecorator('Directive tooltip', 'Depuis DSFR 1.10'),
  args: {
    ...External.args,
    route: '/test',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-link [route]="route" [tooltip]="tooltipMessage" [label]="label"></dsfr-link>`,
  }),
};

export const RouterLink: Story = {
  decorators: dsfrDecorator('Lien avec routerLink'),
  args: {
    ...Default.args,
    tooltipMessage: 'Lien avec routerLink',
    routePath: '/test',
    routerLinkActive: 'active',
    routerLinkExtras: {
      queryParams: { page: 1, id: 15 },
      queryParamsHandling: 'preserve',
    },
  },
};

export const Route: Story = {
  decorators: dsfrDecorator('Lien avec route'),
  args: {
    ...Default.args,
    route: '/test',
    tooltipMessage: 'Lien avec route',
  },
};
