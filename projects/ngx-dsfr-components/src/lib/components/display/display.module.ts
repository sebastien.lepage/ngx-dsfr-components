import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrDisplayComponent } from './display.component';

@NgModule({
  declarations: [DsfrDisplayComponent],
  exports: [DsfrDisplayComponent],
  imports: [CommonModule, FormsModule],
})
export class DsfrDisplayModule {}
