import { argEventEmitter, titleDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { LABELS_BUNDLE_EN, LABELS_BUNDLE_FR, LocalStorage, jsonPath2Value } from '../../shared';
import { DISPLAY_MODAL_ID, DsfrDisplayComponent } from './display.component';

const meta: Meta = {
  title: 'COMPONENTS/Display',
  component: DsfrDisplayComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  parameters: {
    docs: {
      story: {
        inline: false,
        height: 600,
      },
    },
  },
  argTypes: {
    displayChange: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrDisplayComponent>;

const codeLang = LocalStorage.get('lang');
const label = jsonPath2Value(codeLang === 'en' ? LABELS_BUNDLE_EN : LABELS_BUNDLE_FR, 'display.heading');

const buttonDecorator = componentWrapperDecorator(
  (story) => `
    <button class="fr-link fr-fi-theme-fill fr-link--icon-left" 
            aria-controls="${DISPLAY_MODAL_ID}" 
            data-fr-opened="false">
      ${label}
    </button>
    ${story}
  `,
);

/** Default */
export const Default: Story = {
  decorators: [buttonDecorator, titleDecorator("Lien d'accès au paramètrage")],
  args: {},
  render: (args) => ({
    props: args,
    template: `<dsfr-display></dsfr-display>`,
  }),
};

/** ArtworkDirPath */
export const ArtworkDirPath: Story = {
  name: 'Custom artworkDirPath',
  decorators: [buttonDecorator, titleDecorator('Surcharge de la propriété <code>artworkDirPath</code>')],
  args: {
    ...Default.args,
    pictoPath: 'artwork',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-display [artworkDirPath]="artworkDirPath"></dsfr-display>`,
  }),
  parameters: Default.parameters,
};

/** PictoPath */
export const PictoPath: Story = {
  name: 'Legacy pictoPath',
  decorators: [buttonDecorator, titleDecorator('Utilisation de la propriété <code>pictoPath</code> (dépréciée)')],
  args: {
    ...Default.args,
    pictoPath: 'artwork/pictograms/environment',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-display [pictoPath]="pictoPath"></dsfr-display>`,
  }),
  parameters: Default.parameters,
};
