import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrLinkTarget, I18nService } from '../../../shared';
import { DsfrFollowName, DsfrFollowNameConst } from '../follow.model';

@Component({
  selector: 'dsfr-follow-link',
  templateUrl: './follow-link.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFollowLinkComponent {
  /** Lien obligatoire. */
  @Input() link: string;

  /** Target optionnelle, target html par défaut si non renseigné. */
  @Input() target?: DsfrLinkTarget;

  private _name: DsfrFollowName;

  constructor(public i18n: I18nService) {}

  get name(): DsfrFollowName {
    return this._name;
  }

  /** La propriété 'name' est obligatoire et doit être sélectionnée dans une liste énumérée. */
  @Input() set name(value: DsfrFollowName) {
    if (value === DsfrFollowNameConst.TWITTER) {
      this._name = DsfrFollowNameConst.X;
    } else {
      this._name = value;
    }
  }
}
