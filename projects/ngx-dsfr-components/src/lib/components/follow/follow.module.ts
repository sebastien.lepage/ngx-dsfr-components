import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormEmailModule } from '../../forms/form-email';
import { InputGroupComponent } from '../../shared';
import { DsfrFollowLinkComponent } from './follow-link/follow-link.component';
import { DsfrFollowComponent } from './follow.component';

@NgModule({
  declarations: [DsfrFollowComponent, DsfrFollowLinkComponent],
  exports: [DsfrFollowComponent, DsfrFollowLinkComponent],
  imports: [CommonModule, FormsModule, DsfrFormEmailModule, InputGroupComponent],
})
export class DsfrFollowModule {}
