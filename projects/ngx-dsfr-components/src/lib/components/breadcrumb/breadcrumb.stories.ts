import { argEventEmitter } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrLink } from '../../shared';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrBreadcrumbComponent } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Breadcrumb',
  component: DsfrBreadcrumbComponent,
  decorators: [moduleMetadata({ imports: [ItemLinkComponent] })],
  argTypes: { linkSelect: { control: argEventEmitter } },
};
export default meta;
type Story = StoryObj<DsfrBreadcrumbComponent>;

const breadcrumbUrl = '?path=/story/components-breadcrumb--default';

const data: DsfrLink[] = [
  { label: 'Accueil', link: '/', icon: 'fr-icon-home-4-line', ariaLabel: 'home' },
  { label: 'item 2', link: breadcrumbUrl },
  { label: 'item 3', link: breadcrumbUrl },
  { label: 'item 4 (no href)' },
  { label: 'Page actuelle', link: breadcrumbUrl, ariaLabel: 'current page' },
];

export const Default: Story = {
  args: {
    items: data,
    ariaLabel: 'vous êtes ici :',
  },
};
