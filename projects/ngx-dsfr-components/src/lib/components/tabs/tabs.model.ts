import { IsActiveMatchOptions, NavigationExtras } from '@angular/router';

export const BUTTON_TAB_ID_PREFIX = 'buttontab-';

/**
 * Description d'un onglet du composant.
 */
export interface EduTabHeader {
  /**
   * Label de l'onglet, obligatoire
   */
  label: string;

  /**
   * Lien composant vers onglet. Permet de sélectionner un onglet lorsqu'on sélectionne le path correspondant.
   */
  tabId: string;

  /**
   * Icône optionnelle de l'onglet.
   */
  icon?: string;

  /**
   * Permet de désactiver l'onglet.
   */
  disabled?: boolean;
}

/**
 * Description d'un onglet du composant.
 */
export interface DsfrTabRoute extends EduTabHeader {
  /**
   * Lien onglet vers composant. Path invoqué lorsque l'on clique sur un onglet.
   */
  path: string;

  /** Options additionnelles de navigation pour le routerLink (queryParams, state etc.). */
  routerLinkExtras?: NavigationExtras;

  /** RouterLink : options qui déterminent si le lien est actif en cas de directive routerLink. */
  routerLinkActiveOptions?: { exact: boolean } | IsActiveMatchOptions;
}
