import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrCardComponent } from './card.component';
import { DsfrCardModule } from './card.module';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-card [link]="'#'">
    <ng-container heading>Slot heading</ng-container>
    <ng-container desc>
      <div><em>Slot description</em></div>
    </ng-container>
    <ng-container detail> Slot détail</ng-container>
    <ng-container footer> Slot footer</ng-container>
  </dsfr-card>`,
})
class TestHostComponent {
  @ViewChild(DsfrCardComponent)
  public cardComponent: DsfrCardComponent;
}

describe('DsfrCardComponentTest', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrCardModule],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(testHostComponent.cardComponent).not.toBeNull();
  });

  it("should have 'fr-enlarge-link' !", () => {
    const carElt = fixture.nativeElement.querySelector('.fr-card');
    expect(carElt).not.toBeNull();
    expect(carElt.classList).toContain('fr-enlarge-link');
  });

  it("should have 'fr-enlarge-button' !", () => {
    const carElt = fixture.nativeElement.querySelector('.fr-card');
    expect(carElt).not.toBeNull();
    testHostComponent.cardComponent.enlargeButton = true;
    fixture.detectChanges();
    expect(carElt.classList).toContain('fr-enlarge-button');
  });

  it('should have heading as slot without link', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-card__title');
    testHostComponent.cardComponent.link = '';
    fixture.detectChanges();
    expect(tileElt.querySelector('a')).toBeNull();
    expect(tileElt.textContent).toContain('Slot heading');
  });

  it('should have heading as slot and link', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-card__title');
    fixture.detectChanges();
    expect(tileElt.querySelector('a').textContent).toContain('Slot heading');
  });

  it('should set heading as input', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-card__title');
    testHostComponent.cardComponent.heading = 'Mon heading';
    fixture.detectChanges();
    expect(tileElt.textContent).toContain('Mon heading');
  });

  it('should set slot footer if hasFooter is true', () => {
    let tileElt = fixture.nativeElement.querySelector('.fr-card__footer');
    expect(tileElt).toBeNull();
    testHostComponent.cardComponent.hasFooter = true;
    fixture.detectChanges();
    tileElt = fixture.nativeElement.querySelector('.fr-card__footer');
    expect(tileElt.textContent).toContain('Slot footer');
  });

  it('should set detail as input', () => {
    testHostComponent.cardComponent.detail = 'Mon détail';
    fixture.detectChanges();
    const tileElt = fixture.nativeElement.querySelector('.fr-card__detail');
    expect(tileElt.textContent).toContain('Mon détail');
  });

  it('should have link as title', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-card__title');
    fixture.detectChanges();
    expect(tileElt.querySelector('a').classList).toContain('fr-card__title');
    expect(tileElt.querySelector('a').classList).not.toContain('fr-link');
  });
});
