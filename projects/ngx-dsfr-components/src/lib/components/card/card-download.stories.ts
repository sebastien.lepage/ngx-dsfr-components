import { optionsHeadingLevel, titleDecorator } from '.storybook/storybook-utils';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import {
  DsfrFileSizeUnitConst,
  DsfrLinkTargetConst,
  DsfrSizeConst,
  LinkDownloadComponent,
  SvgIconModule,
} from '../../shared';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrButtonModule } from '../button';
import { DsfrButtonsGroupModule } from '../buttons-group';
import { DsfrMimeTypeConst } from '../download';
import { DsfrLinkModule } from '../link';
import { DsfrTagsGroupModule } from '../tags-group';
import { DEFAULT_HEADING_LEVEL } from './base-panel.component';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from './base-panel.model';
import { DsfrCardComponent } from './card.component';
import { DsfrImageFitConst, DsfrImageRatioConst, DsfrImageTypeConst } from './card.model';

const meta: Meta = {
  title: 'COMPONENTS/Card/Download',
  component: DsfrCardComponent,
  decorators: [
    moduleMetadata({
      imports: [
        DsfrBadgesGroupModule,
        DsfrTagsGroupModule,
        SvgIconModule,
        DsfrLinkModule,
        DsfrButtonsGroupModule,
        DsfrButtonModule,
        LinkDownloadComponent,
      ],
    }),
  ],
  parameters: {
    actions: false,
  },
  argTypes: {
    customBackground: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBackgroundConst) },
    customBorder: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBorderConst) },
    downloadAssessFile: { control: { type: 'inline-radio' }, options: [false, true, 'bytes'] },
    downloadLangCode: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadMimeType: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeBytes: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    headingLevel: { control: 'inline-radio', options: optionsHeadingLevel },
    imageFit: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageFitConst) },
    imageRatio: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageRatioConst) },
    imageType: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageTypeConst) },
    linkTarget: { control: { type: 'inline-radio' }, options: Object.values(DsfrLinkTargetConst) },
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
  },
};
export default meta;

type Story = StoryObj<DsfrCardComponent>;

const heading = 'Intitulé de la carte (sur lequel se trouve le lien)';
const image16x9 = 'img/placeholder.16x9.png';
const gridDecorator = componentWrapperDecorator(
  (story) =>
    `<div class="fr-mb-6v">
  <div class="fr-grid-row fr-grid-row--gutters">
      <div class="fr-col-12 fr-col-md-8 fr-col-lg-6">
      ${story}
    </div>
  </div>
</div>`,
);

// -- Download ---------------------------------------------------------------------------------------------------------

const downloadPath = 'files/lorem-ipsum.pdf';

export const Default: Story = {
  decorators: [
    gridDecorator,
    titleDecorator('Carte de téléchargement avec lien', 'Le fonctionnement dépend du navigateur'),
  ],
  args: {
    // Téléchargement
    heading: 'Télécharger le document XX',
    description: 'Description (optionnelle)',
    download: true,
    link: downloadPath,
    downloadSizeBytes: 78971,
    downloadMimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    downloadAssessFile: false,
    downloadDirect: true,
    enlargeLink: true,
    // Autres
    badgesOnMedia: false,
    customBackground: 'default',
    customBorder: 'default',
    disabled: false,
    hasFooter: false,
    headingLevel: DEFAULT_HEADING_LEVEL,
    horizontal: false,
    imageType: 'img',
    size: 'MD',
    imagePath: image16x9,
  },
};

export const Auto: Story = {
  decorators: [gridDecorator, titleDecorator('Carte de téléchargement avec détail renseigné automatiquement')],
  args: {
    ...Default.args,
    downloadSizeBytes: 0,
    downloadAssessFile: true,
  },
};

export const LoadDocLang: Story = {
  decorators: [gridDecorator, titleDecorator('Carte de téléchargement avec fichier en langue étrangère')],
  args: {
    ...Default.args,
    downloadDirect: 'newFileName',
    downloadAssessFile: true,
    downloadLangCode: 'la',
  },
};
