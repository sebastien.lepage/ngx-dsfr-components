import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrMimeTypeConst } from '../download';
import { DsfrCardComponent } from './card.component';
import { DsfrCardModule } from './card.module';

describe('Download DsfrCardComponentTest', () => {
  const TITRE = 'Télécharger le document XX';

  let fixture: ComponentFixture<DsfrCardComponent>;
  let cardComponent: DsfrCardComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrCardModule],
    }).compileComponents();
    fixture = TestBed.createComponent(DsfrCardComponent);
    cardComponent = fixture.componentInstance;
    cardComponent.heading = TITRE;
    cardComponent.download = true;
    cardComponent.link = 'files/lorem-ipsum.pdf';
    cardComponent.downloadSizeBytes = 77824;
    cardComponent.downloadMimeType = DsfrMimeTypeConst.APPLICATION_PDF;
    cardComponent.downloadAssessFile = false;
    cardComponent.downloadDirect = true; // Indique que le clic sur le lien déclenche le téléchargement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(cardComponent).toBeDefined();
  });

  it('default', () => {
    const anchorElt = fixture.nativeElement.querySelector('.fr-card__title a');
    expect(anchorElt).not.toBeNull();
    expect(anchorElt.textContent).toEqual(TITRE);

    const detailElt = fixture.nativeElement.querySelector('.fr-card__detail');
    expect(detailElt.textContent).toContain('PDF - 76 ko'); // 77,12 ko en mode auto ???
  });

  it('target attribute', () => {
    const anchorElt = fixture.nativeElement.querySelector('a');
    let attribute = anchorElt.getAttribute('target');
    expect(attribute).toBeNull();

    cardComponent.linkTarget = '_blank';
    fixture.detectChanges();
    attribute = anchorElt.getAttribute('target');
    expect(attribute).not.toBeNull();
    expect(attribute).toEqual('_blank');
  });
});
