import { CommonModule } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { I18nService, newUniqueId } from '../../shared';

@Component({
  selector: 'dsfr-tooltip-button',
  templateUrl: './tooltip-button.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule],
})
export class DsfrTooltipButtonComponent {
  /**
   * La valeur du tooltip.
   */
  @Input() tooltip: string;

  /** @internal */
  tooltipId = newUniqueId();

  constructor(/** @internal */ public i18n: I18nService) {}
}
