import { DsfrSize } from '../../shared';

/**
 * Les tailles sous forme énumérées.
 */
export namespace DsfrCellSizeConst {
  export const XS = 'XS';
  export const SM = 'SM';
  export const MD = 'MD';
  export const LG = 'LG';
}

/**
 * Les tailles DSFR pour les cellules: XS, SM, MD, LG.
 */
export type DsfrCellSize = (typeof DsfrCellSizeConst)[keyof typeof DsfrCellSizeConst];

/** Modèle de tri d'une colonne */
export interface DsfrSortColumn {
  order: 'desc' | 'asc';
  field: string;
}

/** Etat courant de la table (pagination et tri) */
export interface DsfrTableState {
  page: number;
  rowsPerPage: number;
  sort: DsfrSortColumn | null;
}

/** Modèle de présentation des options d'une cellule */
export interface DsfrCellOptions {
  /** Réactiver le retour à la ligne  */
  allowWrap?: boolean;
  /** La colonne est fixe */
  fixed?: boolean;
  /** Alignement horizontal des cellules de la colonne, à gauche par défaut */
  textAlign?: 'right' | 'center';
  /** Alignement vertical des cellules de la colonne, centré par défaut */
  verticalAlign?: 'top' | 'bottom';
}

/**
 * Modèle de présentation d'une colonne.
 */
export interface DsfrColumn extends DsfrCellOptions {
  /** Propriété représentée par la colonne */
  field: string;
  /** Libellé affiché */
  label?: string;
  /** La colonne est triable */
  sortable?: boolean;
  /** Attribut colspan */
  colspan?: number;
  /** Largeur minimale de la colonne, MD par défaut */
  minWidth?: DsfrCellSize;
  /** Ne pas afficher le libellé de la colonne */
  labelSrOnly?: boolean;
  /* Fonction personnalisée de tri */
  sortFunction?: (a: any, b: any, order: 'asc' | 'desc') => number;
}

/** Modèle de présentation des options d'une ligne */
export interface DsfrRowOptions {
  id: string | number;
  /** La ligne n'est pas sélectionnable */
  disableSelect?: boolean;
}

/**
 * Modèle de présentation des options d'une table.
 */
export interface DsfrTableOptions {
  /** Pas de scroll horizontal sur le tableau */
  noScroll?: boolean;
  /** Lignes sélectionnables  */
  selectable?: boolean;
  /** Afficher une checkbox de sélection de toutes les lignes */
  showSelectAll?: boolean;
  /** Titre invisible (sr-only) */
  captionSrOnly?: boolean;
  /** Titre en bas */
  bottomCaption?: boolean;
  /** Taille des cellules du tableau (SM par défaut) */
  cellSize?: DsfrSize;
  /** Afficher des bordures verticales */
  bordered?: boolean;
  /** Activer le retour à la ligne automatique des éléments */
  allowWrap?: boolean;
  /** Elément correspondant au titre du tableau si caption n'est pas renseigné */
  ariaLabelledBy?: string;
  /** Ajouter l'attribut aria rôle à la table (ex. "grid") */
  role?: string;
}
