export { DsfrTableFooterComponent } from './component/table-footer.component';

export { DsfrTableComponent } from './table.component';

export { DsfrDataTableService } from './service/datatable.service';

export { DsfrCellDirective } from './directive/cell.directive';
export { DsfrColumnDirective } from './directive/column.directive';
export { DsfrSelectAllDirective } from './directive/header-select.directive';
export { DsfrSelectRowDirective } from './directive/row-select.directive';
export { DsfrSortColumnDirective } from './directive/sort-column.directive';
export * from './table.model';
