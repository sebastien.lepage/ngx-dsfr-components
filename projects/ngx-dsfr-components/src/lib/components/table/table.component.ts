import { CommonModule } from '@angular/common';
import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  EventEmitter,
  forwardRef,
  inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  TemplateRef,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject, combineLatest, map, Observable, skip, Subject, takeUntil } from 'rxjs';
import { I18nService, newUniqueId } from '../../shared';
import { LoggerService } from '../../shared/services/logger.service';
import { DsfrSegmentedControl } from '../segmented-control/segmented-control.model';
import { BasePaginatedTableComponent } from './component/paginated-table.component';
import { DsfrTableFooterComponent } from './component/table-footer.component';
import { EdutableHeaderComponent } from './component/table-header.component';
import { DsfrCellDirective } from './directive/cell.directive';
import { DsfrColumnDirective } from './directive/column.directive';
import { DsfrSelectAllDirective } from './directive/header-select.directive';
import { DsfrSelectRowDirective } from './directive/row-select.directive';
import { DsfrSortColumnDirective } from './directive/sort-column.directive';
import { DsfrDataTableService } from './service/datatable.service';
import { DsfrColumn, DsfrRowOptions, DsfrSortColumn, DsfrTableOptions, DsfrTableState } from './table.model';

@Component({
  selector: 'dsfr-datatable',
  templateUrl: './table.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  styleUrls: ['table.component.scss'],
  providers: [DsfrDataTableService],
  imports: [
    CommonModule,
    forwardRef(() => DsfrColumnDirective),
    forwardRef(() => DsfrSortColumnDirective),
    forwardRef(() => DsfrCellDirective),
    forwardRef(() => DsfrSelectRowDirective),
    forwardRef(() => DsfrSelectAllDirective),
    DsfrTableFooterComponent,
    EdutableHeaderComponent,
  ],
})
export class DsfrTableComponent
  extends BasePaginatedTableComponent
  implements OnInit, OnChanges, AfterContentInit, AfterViewInit, OnDestroy
{
  /** Template pour les groupes de boutons d'actions du header */
  @ContentChild('headerActionsTemplate', { static: true }) headerActionsTemplate: TemplateRef<any>;

  /** Template du résumé dans le cas d'un tableau complexe */
  @ContentChild('headerSummaryTemplate', { static: true }) headerSummaryTemplate: TemplateRef<any>;

  /** Template de l'en-tête des colonnes du tableau. (contenu du thead) */
  @ContentChild('tableHeaderTemplate', { static: true }) tableHeaderTemplate: TemplateRef<any>;

  /** Template des lignes, row en paramètre (contenu du tbody) */
  @ContentChild('tableBodyTemplate', { static: true }) tableBodyTemplate: TemplateRef<any>;

  /** Template du pied du tableau (contenu du tfoot) */
  @ContentChild('tableFooterTemplate', { static: true }) tableFooterTemplate: TemplateRef<any>;

  /** Template personnalisé pour afficher le total de lignes */
  @ContentChild('footerResultsTemplate', { static: true }) footerResultsTemplate: TemplateRef<any>;

  /** Template pour les groupes de boutons d'actions du footer */
  @ContentChild('footerActionsTemplate', { static: true }) footerActionsTemplate: TemplateRef<any>;

  /** Template à afficher pendant le chargement des données */
  @ContentChild('loadingTemplate', { static: true }) loadingTemplate: TemplateRef<any>;

  /** Template pour une vue alternative au tableau (ex. liste) */
  @ContentChild('alternativeViewTemplate', { static: true }) alternativeViewTemplate: TemplateRef<any>;

  /** @internal éléments checkbox enfant pour gérer la sélection si tableBodyTemplate */
  @ContentChildren(DsfrSelectRowDirective, { descendants: true }) cellCheckboxes: QueryList<DsfrSelectRowDirective>;

  /**
   * @internal éléments checkbox enfant pour gérer la sélection si tableBodyTemplate non défini.
   * Dans le cas du template par défaut on ne peut pas utiliser ContentChildren.
   * ( cf. issues https://github.com/angular/angular/issues/14842 et https://github.com/angular/angular/issues/16299 ) **/
  @ViewChildren(DsfrSelectRowDirective) cellCheckboxesDefault: QueryList<DsfrSelectRowDirective>;

  /** Identifiant de la table */
  @Input() tableId: string;

  /** Titre du tableau. Obligatoire sauf si labelledBy est renseigné. */
  @Input() caption: string;

  /** Données */
  @Input({ required: true }) data: any[] = [];

  /** Propriété correspondant à l'identifiant unique de chaque donnée (recommandé) */
  @Input() dataKey: string;

  /** Options d'affichage du tableau  */
  @Input() tableOptions: DsfrTableOptions = {};

  /** Options d'affichage des lignes. dataKey doit être défini */
  @Input() rowsOptions: DsfrRowOptions[];

  /** Définition des colonnes */
  @Input({ required: true }) columns: DsfrColumn[];

  /** Afficher le nombre de résultat total dans le footer */
  @Input() showFooterResult = true;

  /** Message affiché en cas de tableau vide ('Aucun résultat' par défaut) */
  @Input() emptyResultsMessage: string;

  /** Indiquer que les données sont en cours de chargement */
  @Input() loading = false;

  /** Header: afficher une barre de recherche */
  @Input() showSearch = false;

  /** Header: afficher le changement de type de vue (tableau / liste) */
  @Input() showHeaderViews = false;

  /** Header: redéfinir les vues du contrôle segmenté (si changeDisplayType). Par défaut tableau/liste */
  @Input() headerViewsOptions: DsfrSegmentedControl[] = [
    {
      label: 'Tableau',
      value: 'TABLE',
      checked: true,
      icon: 'fr-icon-table-line',
    },
    {
      label: 'Liste',
      value: 'LIST',
      icon: 'fr-icon-list-unordered',
    },
  ];

  /** Liste des identifiants des lignes initialement sélectionnées */
  @Input() initialSelection: any[] = [];

  /** Renvoie la colonne triée au changement de tri de la table */
  @Output() readonly sortChange: EventEmitter<DsfrSortColumn> = new EventEmitter<DsfrSortColumn>();

  /** Renvoie la vue sélectionnée au changement de vue (displayModes) */
  @Output() readonly viewSelect: EventEmitter<string> = new EventEmitter<string>(); // ou displayList

  /** Renvoie la valeur de l'input de la barre de recherche au changement.  */
  @Output() readonly searchChange: EventEmitter<string> = new EventEmitter();

  /** Renvoie la valeur de l'input de la barre de recherche au clic sur rechercher.  */
  @Output() readonly searchSelect: EventEmitter<string> = new EventEmitter();

  /** A la sélection d'une ligne, émet toutes les lignes sélectionnées et la ligne sélectionnée */
  @Output() selectionChange: EventEmitter<{ row: any; selectedRows: any[] }> = new EventEmitter<{
    row: any;
    selectedRows: any[];
  }>();

  /** Case à cocher tout sélectionner cochée ou décochée */
  @Output() showSelectAllChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  /** @internal pour permettre la mise a jour des directives sort */
  public activeSort$ = new BehaviorSubject<DsfrSortColumn | undefined>(undefined);

  /** @internal pour permettre la mise a jour du select all en header */
  public isAllChecked$ = new BehaviorSubject<boolean>(false);

  /** @internal lignes affichées (pagination) */
  displayedRows$: Observable<any[]>;

  /** @internal */
  public i18n = inject(I18nService);

  /** @internal lignes sélectionnées */
  public selectedRows: any[] = [];

  /**@internal pas de choix de vue, ou vue table sélectionnée */
  public showTableView: boolean = true;

  /** @internal liste des différentes classes de la table */
  public tableClasses: string[];

  // données en entrée modifiées
  private dataChanged$ = new BehaviorSubject<any[]>([]);
  private notifyOnDestroy = new Subject<void>();

  private readonly logger = inject(LoggerService);
  private readonly cd = inject(ChangeDetectorRef);

  /** @internal */
  public get activeSort(): DsfrSortColumn | undefined {
    return this.activeSort$?.value;
  }

  public ngOnInit(): void {
    this.tableId ??= newUniqueId();
    this.tableClasses = this.setTableClasses();

    if (this.initialState?.sort) {
      this.activeSort$.next(this.initialState.sort);
    }

    if (!this.serverSide) this.totalElements = this.data.length;
    this.rowsPerPage = this.initialState?.rowsPerPage ?? this.rowsPerPageOptions[0].value;
    this.totalPage = this.calculateTotalPage();
    this.currentPage = this.initialState?.page ?? 1;

    // Initialisation de l'état
    const initialState: DsfrTableState = {
      page: this.currentPage,
      rowsPerPage: this.rowsPerPage,
      sort: this.initialState?.sort ?? null,
    };
    this.tableState$ = new BehaviorSubject<DsfrTableState>(initialState);

    // Abonnement à la mise à jour des options  après changement d'état ou rafraichissement
    this.dataTableService.tableOptions$
      .pipe(skip(1), takeUntil(this.notifyOnDestroy))
      .subscribe((options) => this.applyOptionsToTable(options));

    // Abonnement à la mise a jour des données après changement d'état ou rafraichissement des données
    this.registerRefreshData();

    // Initialisation des données
    this.dataTableService.refreshData(this.data);

    if (!this.caption && !this.tableOptions?.ariaLabelledBy) {
      this.logger.warn('dsfr-datatable : Renseigner le titre du tableau est obligatoire (caption ou labelledBy).');
    }

    if ((this.rowsOptions || (this.pagination && this.tableOptions?.selectable)) && !this.dataKey) {
      this.logger.warn("dsfr-datatable : La propriété dataKey n'est pas définie.");
    }

    if (this.serverSide && this.serverResultsLength === undefined) {
      this.logger.warn('dsfr-datatable : En mode serveur serverResultsLength doit être renseigné.');
    }
  }

  /**
   * Initialisation et mise a jour des checkboxes sélectionnées à chaque rafraichissement des données
   * Cas ou le SLOT est utilisé pour les row (tableBodyTemplate)
   */
  public ngAfterContentInit() {
    if (this.selectedRows.length > 0) this.selectCellsAfterRender(this.cellCheckboxes);

    this.cellCheckboxes.changes.pipe(takeUntil(this.notifyOnDestroy)).subscribe(() => {
      this.selectCellsAfterRender(this.cellCheckboxes);
    });
  }

  /**
   * Initialisation et mise a jour des checkboxes sélectionnées à chaque rafraichissement des données
   * Cas ou on utilise le template par DEFAUT pour les rows (tableBodyTemplate non défini)
   */
  public ngAfterViewInit() {
    if (this.selectedRows.length > 0) this.selectCellsAfterRender(this.cellCheckboxesDefault);

    this.cellCheckboxesDefault.changes.pipe(takeUntil(this.notifyOnDestroy)).subscribe(() => {
      this.selectCellsAfterRender(this.cellCheckboxesDefault);
    });
  }

  /**
   * Au changement de data / rowsOptions / tableOptions
   * Rafraichir les données et la vue
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    //  Ignorer le premier changement à l'initialisation (pas de double appel)
    if ((changes.data && !changes.data.isFirstChange()) || (!changes.data && changes.rowsOptions)) {
      this.dataTableService.refreshData(this.data); // rafraichir les données affichées
    }

    // Changement de tableOptions, à l'initialisation ou dynamiquement
    if (changes.tableOptions && !changes.tableOptions.isFirstChange()) {
      this.dataTableService.setTableOptions(this.tableOptions);
    }
  }

  ngOnDestroy(): void {
    this.notifyOnDestroy.next();
    this.notifyOnDestroy.complete();
  }

  /**
   * @internal
   * Initialisation de la table si sélection
   * - calcul du nombre d'elements selectionnables
   * - application de la sélection initiale
   * - coche de isAllChecked si toutes les lignes sont sélectionnées
   */
  public initSelectableTable(rows: any[]): void {
    this.selectableRows = this.rowsOptions ? rows.filter((r) => !r.rowOptions || !r.rowOptions.disableSelect) : rows;
    if (this.initialSelection && this.selectableRows && this.selectedRows.length === 0) {
      if (this.dataKey) {
        this.selectedRows = this.selectableRows.filter((row) => this.initialSelection.includes(row[this.dataKey]));
      }
      this.isAllChecked$.next(this.selectedRows.length >= this.selectableRows.length);
    }

    if (this.selectedRows.length > 0) {
      // Lignes déja sélectionnées (rafraichissement de la vue)
      this.selectedRows = this.selectedRows.filter((selectedRow) =>
        this.selectableRows.find((r) => r[this.dataKey] === selectedRow[this.dataKey]),
      );
    }
  }

  /** @internal */
  public setTableClasses(): string[] {
    const classes: string[] = ['fr-table'];

    if (this.tableOptions.bordered) classes.push('fr-table--bordered');
    if (this.tableOptions.noScroll) classes.push('fr-table--no-scroll');
    if (this.tableOptions.bottomCaption) classes.push('fr-table--caption-bottom');
    if (this.tableOptions.captionSrOnly) classes.push('fr-table--no-caption');

    // Size class
    switch (this.tableOptions.cellSize) {
      case 'SM':
        classes.push('fr-table--sm');
        break;
      case 'LG':
        classes.push('fr-table--lg');
        break;

      default:
        break;
    }

    return classes;
  }

  /** @internal */
  public trackById(dataKey: string, index: number, item: any) {
    return item[dataKey] || index;
  }

  /** @internal */
  public onviewSelect(view: string): void {
    this.showTableView = view === this.headerViewsOptions[0].value;
    this.viewSelect.emit(view);
  }

  /** @internal */
  public onsearchChange(search: string): void {
    this.searchChange.emit(search);
  }

  /** @internal */
  public onsearchSelect(search: string): void {
    this.searchSelect.emit(search);
  }

  /**
   * Valeur de l'attribut aria-sort sur la colonne
   * @param field colonne triée
   * @returns undefined / ascending / descending
   * @internal
   */
  public getAttributAriaSort(field: string): string | undefined {
    if (this.activeSort && this.activeSort.field === field) {
      return this.activeSort.order === 'asc' ? 'ascending' : 'descending';
    }

    return undefined;
  }

  /** selection ou déselection d'une ligne
   * @internal  */
  public selectRow(row: any) {
    // recherche de la ligne selon si dataKey est défini ou par l'index sinon
    const i: number = this.dataKey
      ? this.selectedRows.findIndex((r) => r[this.dataKey] === row[this.dataKey])
      : this.selectedRows.findIndex((r) => r.index === row.index);

    if (i !== -1) {
      this.selectedRows.splice(i, 1); // suppression de la ligne
    } else {
      this.selectedRows.push(row); // ajout de la ligne
    }

    this.isAllChecked$.next(this.selectedRows.length >= this.selectableRows.length); // vérification de isAllChecked
    this.selectionChange.emit({ row: row, selectedRows: this.selectedRows });
  }

  /**
   * Selection ou déselection de toutes les lignes
   * @internal
   * */
  public toggleshowSelectAll(allSelected: boolean) {
    this.selectedRows = this.selectedRows.length === this.selectableRows.length ? [] : [...this.selectableRows];
    this.showSelectAllCells(allSelected);

    this.selectionChange.emit({ row: null, selectedRows: this.selectedRows });
    this.showSelectAllChange.emit(allSelected);
  }

  /**
   * Affichage du tableau selon la colonne concernées et mise a jour des données affichées
   * @internal
   **/
  public sortTable(colId: string) {
    const activeSort: DsfrSortColumn = {
      field: colId,
      order: this.activeSort?.field === colId && this.activeSort?.order === 'asc' ? 'desc' : 'asc',
    };

    this.activeSort$.next(activeSort); // emission de activeSort pour mettre a jour les directives sortColumn

    const currentState = this.tableState$.getValue();
    this.tableState$.next({ ...currentState, sort: activeSort }); // application du tri et mise a jour des données affichées

    this.sortChange.emit(activeSort);
  }

  /** Fonction de tri : sortFunction de la colonne si définie ou par défaut sur du contenu texte simple */
  private sortRows(rows: any[], state: DsfrTableState): any[] {
    if (!state.sort?.field) return rows;

    const { field, order } = state.sort;
    const column = this.columns.find((c) => c.field === field);

    // tri selon la fonction définie sur la colonne
    if (column && column.sortFunction && typeof column.sortFunction === 'function') {
      return rows.sort((a, b) => column.sortFunction!(a, b, order));
    }

    // tri par défaut
    return rows.sort((a, b) => this.sortByDefault(a, b, field, order));
  }

  /**
   * Abonnement à la mise a jour des données et au changement d'état pour rafraichir le tableau
   */
  private registerRefreshData(): void {
    if (!this.serverSide) {
      // Mise à jour des données lorsque refreshData appelé
      this.dataTableService.data$.pipe(takeUntil(this.notifyOnDestroy)).subscribe((data) => {
        this.dataChanged$.next(this.mapDataToRows(data));
      });

      // Mise à jour des lignes affichées si l'état a changé et/ou les données
      this.displayedRows$ = combineLatest([this.dataChanged$, this.tableState$]).pipe(
        map(([rows, state]) => this.applyStateToData(rows, state)),
      );
    } else {
      // si serveur side, abonnement uniquement au changement des données (état géré par l'utilisateur en distant)
      // et à dataTableService et non dataChanged pour ne pas déclencher deux fois l'event
      this.displayedRows$ = this.dataTableService.data$.pipe(
        takeUntil(this.notifyOnDestroy),
        map((data) => this.mapDataToRows(data)),
      );
      this.tableState$.pipe(takeUntil(this.notifyOnDestroy)).subscribe((state) => this.stateChange.emit(state));
    }
  }

  /**
   * Application de l'état sur les données affichées apres refresh data ou tri/pagination
   * @param data données modifiées
   * @param state état (tri, pagination, page size)
   * @returns données à jour triées et paginées
   */
  private applyStateToData(data: any[], state?: DsfrTableState): any[] {
    if (state) {
      // application du tri
      if (state.sort) {
        data = this.sortRows(data, state);
      }
      // application de la pagination
      if (this.pagination) {
        return this.paginateData(data, state);
      }
    }

    return data;
  }

  /**
   * Application des nouvelles options d'affichage du tableau
   * @param options du tableau modifiées
   */
  private applyOptionsToTable(options: DsfrTableOptions) {
    this.tableOptions = options;
    this.tableClasses = this.setTableClasses();
    // Indispensable en strategy.onPush dans ce cas précis pour forcer le rechargement de tout le tableau (body compris)
    this.cd.markForCheck();
  }

  /**
   * Retourne les options de la ligne passée en paramètre, selon l'identifiant ou l'index
   * */
  private getRowOption(row: any): DsfrRowOptions | undefined {
    if (!this.rowsOptions || !row) return undefined;
    return this.dataKey ? this.rowsOptions.find((r) => r.id === row[this.dataKey]) : undefined;
  }

  /**
   * Map data au format lignes affichables et application de la sélection
   * @param data
   * @returns data avec informations d'afffichage et index
   */
  private mapDataToRows(data: any[]): any[] {
    const rows = data.map((row, i) => ({
      ...row,
      index: i,
      rowOptions: this.getRowOption(row),
    }));
    if (this.tableOptions?.selectable) {
      this.initSelectableTable(rows);
    }

    return rows;
  }

  /**
   * Au clic sur tout sélectionner/tout déselectionner
   * Selection des composants de checkbox (cellCheckboxesDefault ou cellCheckboxes) pour application du script DSFR
   * */
  private showSelectAllCells(allSelected: boolean): void {
    const checkBoxes =
      this.cellCheckboxesDefault?.length > this.cellCheckboxes?.length
        ? this.cellCheckboxesDefault
        : this.cellCheckboxes;

    checkBoxes.forEach((cellCheckbox) => {
      cellCheckbox.simulateClick(allSelected);
    });
  }

  /**
   * Au nouveau rendu du composant tableau (initialisation, refreshData ou pagination)
   * Sélection des composants de checkbox correspond a selectedRows application du script DSFR (style)
   * */
  private selectCellsAfterRender(cellsCheckboxes: QueryList<DsfrSelectRowDirective>): void {
    const checkboxesToSelect = cellsCheckboxes.filter((cell) =>
      this.selectedRows.find((s) => s[this.dataKey] === cell.row[this.dataKey]),
    );

    checkboxesToSelect.forEach((cellCheckbox) => {
      cellCheckbox.simulateClick(true);
    });
  }

  /** Fonction de tri par défaut insensible à la casse et aux diacritiques */
  private sortByDefault(a: any, b: any, field: string, order: 'asc' | 'desc'): number {
    const valueA = a[field] ?? ''; // Remplacer undefined par chaîne vide
    const valueB = b[field] ?? '';

    return valueA === valueB ? 0 : valueA.localeCompare(valueB) * (order === 'asc' ? 1 : -1);
  }
}
