import { Component, SimpleChange, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DsfrTableComponent } from './table.component';
import { DsfrColumn, DsfrRowOptions, DsfrTableState } from './table.model';
require('../../../../../../node_modules/@gouvfr/dsfr/dist/dsfr.module.js');

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-datatable
    [caption]="'Titre'"
    dataKey="id"
    [columns]="columns"
    [rowsOptions]="rowsOptions"
    [tableOptions]="tableOptions"
    [data]="data"></dsfr-datatable>`,
})
class TestHostComponent {
  @ViewChild(DsfrTableComponent)
  public tableComponent: DsfrTableComponent;
  tableOptions = {};
  rowsOptions: DsfrRowOptions[] = [];

  columns: DsfrColumn[] = [
    { label: 'Nom', field: 'name', verticalAlign: 'top', sortable: true },
    { label: 'Prénom', field: 'firstName', verticalAlign: 'bottom', sortable: true },
    { label: 'Date de naissance', field: 'birthDate', textAlign: 'center' },
    { label: 'Ville', field: 'city', textAlign: 'right' },
  ];

  data = [
    { id: '1', name: 'Smith', firstName: 'John', birthDate: '1980-01-15', city: 'New York' },
    { id: '2', name: 'johnson', firstName: 'émma', birthDate: '1992-03-22', city: 'Los Angeles' },
    { id: '3', name: 'Williams', firstName: 'Aaliyah', birthDate: '1985-05-30', city: 'Chicago' },
    { id: '4', name: 'Brown', firstName: 'Emily', birthDate: '1990-07-07', city: 'Houston' },
  ];
}

describe('DsfrTableComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  function initializeTableComponent() {
    //@ts-ignore
    dsfr.start();
    testHostComponent.tableComponent.ngOnInit();
    testHostComponent.tableComponent.ngAfterContentInit();
    testHostComponent.tableComponent.ngAfterViewInit();
    fixture.detectChanges();
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrTableComponent],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put id test on table element ', () => {
    testHostComponent.tableComponent.tableId = 'test';

    testHostComponent.tableComponent.ngOnInit();
    testHostComponent.tableComponent.ngAfterContentInit();
    testHostComponent.tableComponent.ngAfterViewInit();
    fixture.detectChanges();

    const tableElement = fixture.nativeElement.querySelector('table');
    expect(tableElement.getAttribute('id')).toEqual('test');
  });

  it('should put titre on caption element ', () => {
    testHostComponent.tableComponent.ngOnInit();
    testHostComponent.tableComponent.ngAfterContentInit();
    testHostComponent.tableComponent.ngAfterViewInit();
    fixture.detectChanges();

    const caption = fixture.nativeElement.querySelector('caption');
    expect(caption.textContent).toEqual('Titre ');
  });

  it('should init table with caption as labelledBy', () => {
    testHostComponent.tableComponent.caption = '';
    testHostComponent.tableComponent.tableOptions = { ariaLabelledBy: 'comp' };
    fixture.detectChanges();
    initializeTableComponent();

    const tableElement = fixture.nativeElement.querySelector('table');
    expect(tableElement.getAttribute('aria-labelledby')).toEqual('comp');
    expect(fixture.nativeElement.querySelector('caption')).toBeFalsy();
  });

  it('should init table with 4 columns and 4 rows', () => {
    fixture.detectChanges();
    const header = fixture.nativeElement.querySelector('thead');
    const body = fixture.nativeElement.querySelector('tbody');
    initializeTableComponent();

    expect(header.querySelectorAll('th').length).toBe(4);
    expect(body.querySelectorAll('tr').length).toBe(4);
    expect(body.querySelector('dsfr-cell-checkbox')).toBeDefined();
  });

  it('should set correct classes from table options', () => {
    testHostComponent.tableComponent.tableOptions = { bordered: true, cellSize: 'LG', allowWrap: true };
    fixture.detectChanges();

    const tableWrapper = fixture.nativeElement.querySelector('.fr-table');
    const table = tableWrapper.querySelector('table');
    initializeTableComponent();

    expect(tableWrapper.classList).toContain('fr-table--bordered');
    expect(tableWrapper.classList).toContain('fr-table--lg');
    expect(table.classList).toContain('fr-cell--multiline');
  });

  it('should update table options and put correct classes', () => {
    initializeTableComponent();
    const tableWrapper = fixture.nativeElement.querySelector('.fr-table');

    expect(tableWrapper.className).toEqual('fr-table');

    testHostComponent.tableComponent.dataTableService.updateTableOptions('bordered', true);
    fixture.detectChanges();
    expect(tableWrapper.classList).toContain('fr-table--bordered');

    testHostComponent.tableComponent.dataTableService.updateTableOptions('cellSize', 'SM');
    fixture.detectChanges();
    expect(tableWrapper.classList).toContain('fr-table--sm');
  });

  it('should set correct classes from column options', () => {
    fixture.detectChanges();

    const tableWrapper = fixture.nativeElement.querySelector('.fr-table');
    const table = tableWrapper.querySelector('table');
    const firstHeaderRow = table.querySelector('thead').querySelector('tr');
    const firstRow = tableWrapper.querySelector('tbody').querySelector('tr');

    initializeTableComponent();

    expect(firstHeaderRow.querySelectorAll('th')[3].classList).toContain('fr-cell--right');
    expect(firstHeaderRow.querySelectorAll('th')[2].classList).toContain('fr-cell--center');
    expect(firstHeaderRow.querySelectorAll('th')[1].classList).toContain('fr-cell--bottom');

    expect(firstRow.querySelectorAll('td')[3].classList).toContain('fr-cell--right');
    expect(firstRow.querySelectorAll('td')[2].classList).toContain('fr-cell--center');
    expect(firstHeaderRow.querySelectorAll('th')[1].classList).toContain('fr-cell--bottom');
    expect(firstRow.querySelectorAll('td')[1].classList).not.toContain('fr-cell--right');
  });

  describe('Header', () => {
    it('should display default control views', () => {
      testHostComponent.tableComponent.showHeaderViews = true;
      fixture.detectChanges();
      initializeTableComponent();

      const header = fixture.nativeElement.querySelector('.fr-table__header');

      expect(header.querySelector('.fr-segmented').querySelector('legend').textContent).toContain("Type d'affichage");
      expect(header.querySelector('.fr-segmented').querySelector('input').getAttribute('checked')).toBe('true');
      expect(header.querySelector('.fr-segmented').querySelectorAll('input')[1].getAttribute('checked')).toBe(null);
    });

    it('should switch views on click control view', fakeAsync(() => {
      testHostComponent.tableComponent.showHeaderViews = true;
      fixture.detectChanges();
      initializeTableComponent();

      const header = fixture.nativeElement.querySelector('.fr-table__header');
      const viewListInput = header.querySelector('.fr-segmented').querySelectorAll('input')[1];
      viewListInput.click();
      tick(100);

      expect(header.querySelector('.fr-segmented').querySelector('input').checked).toBe(false);
      expect(viewListInput.checked).toBe(true);
    }));
  });

  describe('Footer', () => {
    it('should display number of rows', () => {
      testHostComponent.tableComponent.showFooterResult = true;
      testHostComponent.tableComponent.pagination = false;
      fixture.detectChanges();
      initializeTableComponent();

      const tableFooterStart = fixture.nativeElement.querySelector('.fr-table__footer--start');
      const tablePagination = fixture.nativeElement.querySelector('.fr-pagination');

      expect(tableFooterStart.textContent).toContain('4 lignes');
      expect(tablePagination).toBeNull();
    });

    it('should display empty messsage', () => {
      testHostComponent.tableComponent.showFooterResult = true;
      testHostComponent.tableComponent.data = [];
      fixture.detectChanges();
      initializeTableComponent();

      const tableFooterStart = fixture.nativeElement.querySelector('.fr-table__footer--start');

      expect(tableFooterStart.textContent).toContain('Aucun résultat');
    });

    it('should display custom empty messsage', () => {
      testHostComponent.tableComponent.showFooterResult = true;
      testHostComponent.tableComponent.data = [];
      testHostComponent.tableComponent.emptyResultsMessage = 'Pas de résultat trouvé';
      fixture.detectChanges();
      initializeTableComponent();

      const tableFooterStart = fixture.nativeElement.querySelector('.fr-table__footer--start');

      expect(tableFooterStart.textContent).toContain('Pas de résultat trouvé');
    });
  });

  describe('Selection', () => {
    it('should add select column when table is selectable', () => {
      testHostComponent.tableComponent.tableOptions = { selectable: true };

      initializeTableComponent();

      const header = fixture.nativeElement.querySelector('thead');
      const firstColumn = header.querySelector('th');
      const body = fixture.nativeElement.querySelector('tbody');

      expect(header.querySelectorAll('th').length).toBe(5);
      expect(body.querySelectorAll('tr').length).toBe(4);
      expect(firstColumn.className).toContain('fr-cell--fixed fr-col--xs');
      expect(firstColumn.getAttribute('role')).toEqual('columnheader');
      expect(firstColumn.textContent).toEqual(' Sélectionner');
      expect(firstColumn.querySelector('dsfr-cell-checkbox')).toBeFalsy();
    });

    it('should add select column with all selectable', () => {
      testHostComponent.tableComponent.tableOptions = { selectable: true, showSelectAll: true };
      fixture.detectChanges();
      initializeTableComponent();

      const header = fixture.nativeElement.querySelector('thead');
      const firstColumn = header.querySelector('th');

      expect(firstColumn.textContent).toEqual(' Tout sélectionner ');
      expect(firstColumn.querySelector('dsfr-cell-checkbox')).toBeDefined();
    });

    it('should have initial selection for specified rows', () => {
      testHostComponent.tableComponent.tableOptions = { selectable: true };
      testHostComponent.tableComponent.initialSelection = ['1', '3'];
      fixture.detectChanges();
      initializeTableComponent();

      const rows = fixture.nativeElement.querySelector('tbody').querySelectorAll('tr');
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(2);

      // la première et la troisième lignes sont sélectionnées
      const firstRowCheckbox = rows[0].querySelector('input[type="checkbox"]');
      const thirdRowCheckbox = rows[2].querySelector('input[type="checkbox"]');
      expect(firstRowCheckbox.checked).toBe(true);
      expect(thirdRowCheckbox.checked).toBe(true);

      // les autres lignes ne sont pas sélectionnées
      expect(rows[1].querySelector('input[type="checkbox"]').checked).toBe(false);
      expect(rows[3].querySelector('input[type="checkbox"]').checked).toBe(false);
    });

    it('should select clicked row', fakeAsync(() => {
      testHostComponent.tableComponent.tableOptions = { selectable: true };
      fixture.detectChanges();
      initializeTableComponent();

      const rows = fixture.nativeElement.querySelector('tbody').querySelectorAll('tr');
      const thirdRowCheckbox = rows[2].querySelector('input[type="checkbox"]');

      expect(thirdRowCheckbox.checked).toBe(false);
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(0);

      thirdRowCheckbox.click();
      tick(400);

      expect(thirdRowCheckbox.checked).toBe(true);
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(1);
      expect(rows[1].querySelector('input[type="checkbox"]').checked).toBe(false);
    }));

    it('should deselect double clicked row', fakeAsync(() => {
      testHostComponent.tableComponent.tableOptions = { selectable: true };
      fixture.detectChanges();
      initializeTableComponent();

      const rows = fixture.nativeElement.querySelector('tbody').querySelectorAll('tr');
      const thirdRowCheckbox = rows[2].querySelector('input[type="checkbox"]');
      thirdRowCheckbox.click();
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(1);
      expect(thirdRowCheckbox.checked).toBe(true);

      thirdRowCheckbox.click();
      tick(100);
      expect(thirdRowCheckbox.checked).toBe(false);
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(0);
    }));

    it('should select all on click on select header', fakeAsync(() => {
      testHostComponent.tableComponent.tableOptions = { selectable: true, showSelectAll: true };
      fixture.detectChanges();
      initializeTableComponent();

      const header = fixture.nativeElement.querySelector('thead');
      const rows = fixture.nativeElement.querySelector('tbody').querySelectorAll('tr');
      // ligne non selectionnée a l'initialisation
      expect(rows[3].querySelector('input[type="checkbox"]').checked).toBe(false);

      header.querySelector('.fr-table-checkbox').click();
      tick(200); // delay to let dsfr execute

      // selectedRows remplies et checkboxes cochées
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(4);
      expect(rows[3].querySelector('input[type="checkbox"]').checked).toBe(true);
    }));

    it('should select all on click on select header except disabled rows', fakeAsync(() => {
      testHostComponent.tableComponent.tableOptions = { selectable: true, showSelectAll: true };
      testHostComponent.tableComponent.rowsOptions = [
        { id: '2', disableSelect: true },
        { id: '3', disableSelect: true },
      ];

      fixture.detectChanges();
      initializeTableComponent();

      const header = fixture.nativeElement.querySelector('thead');
      const rows = fixture.nativeElement.querySelector('tbody').querySelectorAll('tr');
      // ligne non selectionnée a l'initialisation
      expect(rows[3].querySelector('input[type="checkbox"]').checked).toBe(false);
      expect(rows[2].querySelector('input[type="checkbox"]')).toBeNull();
      header.querySelector('.fr-table-checkbox').click();
      tick(200); // delay to let dsfr execute

      // selectedRows remplies et checkboxes cochées
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(2);
      expect(rows[3].querySelector('input[type="checkbox"]').checked).toBe(true);
    }));

    it('should refresh disableSelect for specifics rows', () => {
      testHostComponent.tableComponent.tableOptions = { selectable: true, showSelectAll: true };
      testHostComponent.tableComponent.rowsOptions = [
        { id: '1', disableSelect: true },
        { id: '2', disableSelect: true },
      ];

      fixture.detectChanges();
      initializeTableComponent();
      const rows = fixture.nativeElement.querySelector('tbody').querySelectorAll('tr');

      // lines not selected on init
      expect(rows[0].querySelector('input[type="checkbox"]')).toBeNull();
      expect(rows[1].querySelector('input[type="checkbox"]')).toBeNull();
      expect(rows[2].querySelector('input[type="checkbox"]').checked).toBe(false);
      expect(testHostComponent.tableComponent.selectableRows.length).toEqual(2);

      // update rowsOptions for select
      testHostComponent.tableComponent.rowsOptions = [
        { id: '1', disableSelect: false },
        { id: '2', disableSelect: true },
      ];
      // manually triggered ngOnChanges
      testHostComponent.tableComponent.ngOnChanges({
        rowsOptions: new SimpleChange(null, testHostComponent.tableComponent.rowsOptions, false),
      });

      fixture.detectChanges();
      // expect refreshData to have been called

      // selectedRows updated and checkboxes refreshed
      expect(testHostComponent.tableComponent.selectableRows.length).toEqual(3);
      expect(rows[1].querySelector('input[type="checkbox"]')).toBeNull();
      expect(rows[0].querySelector('input[type="checkbox"]').checked).toBe(false);
      expect(rows[2].querySelector('input[type="checkbox"]').checked).toBe(false);
    });
  });

  describe('Sort', () => {
    it('should apply initial sort on firstName column', (done) => {
      testHostComponent.tableComponent.initialState = {
        sort: { field: 'firstName', order: 'asc' },
        page: 1,
        rowsPerPage: 10,
      };

      const header = fixture.nativeElement.querySelector('thead');
      const firstNameColumn = header.querySelectorAll('th')[1];
      const nameColumn = header.querySelectorAll('th')[0];

      initializeTableComponent();

      // Récupérer les données triées
      testHostComponent.tableComponent.displayedRows$.subscribe((res) => {
        // Vérifier que les données sont triées par firstName dans l'ordre croissant
        const expectedFirstNames = ['Aaliyah', 'Emily', 'émma', 'John'];
        expect(res.map((row) => row.firstName)).toEqual(expectedFirstNames);
        done(); // for asynchrone test
      });
      expect(firstNameColumn.querySelector('button').getAttribute('aria-sorting')).toEqual('asc');
      expect(nameColumn.querySelector('button').getAttribute('aria-sorting')).toBeNull();
    });

    it('should apply sort on reverse order on double click', fakeAsync(() => {
      testHostComponent.tableComponent.initialState = {
        sort: { field: 'firstName', order: 'asc' },
        page: 1,
        rowsPerPage: 10,
      };

      const header = fixture.nativeElement.querySelector('thead');
      const firstNameColumn = header.querySelectorAll('th')[1];
      const nameColumn = header.querySelectorAll('th')[0];

      initializeTableComponent();

      nameColumn.querySelector('button').click();
      nameColumn.querySelector('button').click();
      tick(100);
      fixture.detectChanges();

      expect(firstNameColumn.querySelector('button').getAttribute('aria-sorting')).toBeNull();
      expect(nameColumn.querySelector('button').getAttribute('aria-sorting')).toEqual('desc');
      expect(fixture.nativeElement.querySelector('tbody').querySelector('td').textContent).toEqual(' Williams');
    }));
  });
  describe('Pagination', () => {
    function initPagination() {
      testHostComponent.tableComponent.data = [
        ...testHostComponent.tableComponent.data,
        ...testHostComponent.tableComponent.data,
        ...testHostComponent.tableComponent.data,
      ];
      testHostComponent.tableComponent.pagination = true;
    }

    it('should not add pagination component if number of page is < 1', () => {
      testHostComponent.tableComponent.pagination = true;
      fixture.detectChanges();
      initializeTableComponent();

      const pagination = fixture.nativeElement.querySelector('dsfr-pagination');
      expect(pagination).toBeNull();
      expect(testHostComponent.tableComponent.totalPage).toEqual(1);
    });

    it('should add pagination component with correct number of pages', () => {
      initPagination();
      initializeTableComponent();
      fixture.detectChanges();

      const pagination = fixture.nativeElement.querySelector('dsfr-pagination');
      expect(pagination.querySelector('nav')).toBeDefined();
      expect(testHostComponent.tableComponent.totalPage).toEqual(2);
    });

    it('should display page based on initial page', () => {
      initPagination();
      testHostComponent.tableComponent.initialState = { page: 2, sort: null, rowsPerPage: 10 };
      fixture.detectChanges();
      initializeTableComponent();
      const body = fixture.nativeElement.querySelector('tbody');

      // expect current page to be page 2
      expect(testHostComponent.tableComponent.currentPage).toEqual(2);
      // expect first result display to be first element of page 2
      expect(body.querySelector('td').textContent).toContain('Williams');
    });

    it('should set the initial page size and calculate the number of pages accordingly', () => {
      initPagination();
      testHostComponent.tableComponent.initialState = { rowsPerPage: 2, sort: null, page: 1 };
      testHostComponent.tableComponent.pagination = true;
      fixture.detectChanges();
      initializeTableComponent();

      expect(testHostComponent.tableComponent.totalPage).toEqual(6);
    });

    it('should update page size on click and recalculate number of pages', fakeAsync(() => {
      initPagination();
      testHostComponent.tableComponent.initialState = { rowsPerPage: 5, sort: null, page: 1 };
      initializeTableComponent();
      fixture.detectChanges();

      const mockOutput = jest.spyOn(testHostComponent.tableComponent.paginationChange, 'emit');
      testHostComponent.tableComponent.onRowsPerPageChange(30);
      const statePage = { currentPage: 1, rowsPerPage: 30 };

      tick();
      fixture.detectChanges();

      expect(mockOutput).toHaveBeenCalledWith(statePage);
      expect(testHostComponent.tableComponent.totalPage).toEqual(1);
    }));
  });

  describe('Server mode', () => {
    it('should send state after init', fakeAsync(() => {
      testHostComponent.tableComponent.serverSide = true;
      testHostComponent.tableComponent.serverResultsLength = 180;
      fixture.detectChanges();
      const mockStateChange = jest.spyOn(testHostComponent.tableComponent.stateChange, 'emit');

      // initial state
      const stateTable = { page: 1, rowsPerPage: 10, sort: null };

      initializeTableComponent();
      expect(mockStateChange).toHaveBeenCalledWith(stateTable);
    }));

    it('should send state after sort', fakeAsync(() => {
      testHostComponent.tableComponent.serverSide = true;
      testHostComponent.tableComponent.serverResultsLength = 180;
      fixture.detectChanges();

      const header = fixture.nativeElement.querySelector('thead');
      const buttonSortName = header.querySelectorAll('th')[0].querySelector('button');
      initializeTableComponent();

      // Update state with sort
      const stateTable: DsfrTableState = { page: 1, rowsPerPage: 10, sort: { field: 'name', order: 'asc' } };
      // Spy on state change emission
      const mockStateChange = jest.spyOn(testHostComponent.tableComponent.stateChange, 'emit');

      buttonSortName.click();
      tick();
      fixture.detectChanges();

      expect(mockStateChange).toHaveBeenCalledWith(stateTable);
    }));

    it('should init pagination with correct total pages based on serverResultLength', fakeAsync(() => {
      testHostComponent.tableComponent.serverSide = true;
      testHostComponent.tableComponent.serverResultsLength = 180;
      testHostComponent.tableComponent.pagination = true;
      fixture.detectChanges();

      initializeTableComponent();
      fixture.detectChanges();

      // initial state with correct pagination
      expect(testHostComponent.tableComponent.totalPage).toEqual(18);
    }));

    it('should send state with correct pagination after navigation to next page', fakeAsync(() => {
      testHostComponent.tableComponent.serverSide = true;
      testHostComponent.tableComponent.serverResultsLength = 180;
      testHostComponent.tableComponent.pagination = true;
      fixture.detectChanges();

      initializeTableComponent();
      fixture.detectChanges();

      // Spy on state change emission
      const mockStateChange = jest.spyOn(testHostComponent.tableComponent.stateChange, 'emit');
      const stateTable: DsfrTableState = { page: 2, rowsPerPage: 10, sort: null };

      // Click on next page
      fixture.nativeElement.querySelector('.fr-pagination__link--next').click();
      tick();
      fixture.detectChanges();

      // initial state with correct pagination
      expect(mockStateChange).toHaveBeenCalledWith(stateTable);
    }));

    it('should send state with correct pagination after navigation to last page', fakeAsync(() => {
      testHostComponent.tableComponent.serverSide = true;
      testHostComponent.tableComponent.serverResultsLength = 180;
      testHostComponent.tableComponent.pagination = true;
      fixture.detectChanges();

      initializeTableComponent();

      // Spy on state change emission
      const mockStateChange = jest.spyOn(testHostComponent.tableComponent.stateChange, 'emit');
      const stateTable: DsfrTableState = { page: 18, rowsPerPage: 10, sort: null };

      // Click on last page
      fixture.nativeElement.querySelector('.fr-pagination__link--last').click();
      tick();
      fixture.detectChanges();

      // initial state with correct pagination
      expect(mockStateChange).toHaveBeenCalledWith(stateTable);
    }));

    it('should select rows from initial selection ', fakeAsync(() => {
      testHostComponent.tableComponent.serverSide = true;
      testHostComponent.tableComponent.serverResultsLength = 180;
      testHostComponent.tableComponent.pagination = true;
      testHostComponent.tableComponent.tableOptions = { selectable: true };
      testHostComponent.tableComponent.initialSelection = ['1', '3'];

      fixture.detectChanges();
      initializeTableComponent();

      // manually triggered ngOnChanges for data to populate on serverSide version
      testHostComponent.tableComponent.ngOnChanges({
        data: new SimpleChange(null, testHostComponent.tableComponent.data, false),
      });
      fixture.detectChanges();

      const rows = fixture.nativeElement.querySelector('tbody').querySelectorAll('tr');
      expect(testHostComponent.tableComponent.selectedRows.length).toEqual(2);

      // la première ligne est sélectionnée
      const firstRowCheckbox = rows[0].querySelector('input[type="checkbox"]');
      expect(firstRowCheckbox.checked).toBe(true);
    }));
  });
});
