import { Directive, HostBinding, Input } from '@angular/core';
import { DsfrCellOptions } from '../table.model';

/**
 * Directive affichage d'une cellule (alignements)
 */
@Directive({
  selector: '[dsfrCell]',
  standalone: true,
})
export class DsfrCellDirective {
  @Input('dsfrCell') cell: DsfrCellOptions;

  @HostBinding('class.fr-cell--multiline')
  get isMultiline(): boolean | undefined {
    return this.cell?.allowWrap;
  }

  @HostBinding('class.fr-cell--top')
  get isTopCell(): boolean {
    return this.cell?.verticalAlign === 'top';
  }

  @HostBinding('class.fr-cell--bottom')
  get isBottomCell(): boolean {
    return this.cell?.verticalAlign === 'bottom';
  }

  @HostBinding('class.fr-cell--center')
  get isCenterCell(): boolean {
    return this.cell?.textAlign === 'center';
  }

  @HostBinding('class.fr-cell--right')
  get isRightCell(): boolean {
    return this.cell?.textAlign === 'right';
  }

  @HostBinding('class.fr-cell--fixed')
  get isFixed(): boolean | undefined {
    return this.cell?.fixed;
  }
}
