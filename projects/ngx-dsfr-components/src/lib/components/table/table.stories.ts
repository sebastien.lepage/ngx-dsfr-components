import { CommonModule } from '@angular/common';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormInputModule } from '../../forms';
import { DsfrButtonModule } from '../button';
import { DsfrButtonsGroupModule } from '../buttons-group';
import { DsfrTooltipButtonComponent } from '../tooltip';
import { DsfrDataTableModule } from './datatable.module';
import { DsfrTableComponent } from './table.component';
import { DsfrColumn } from './table.model';

const meta: Meta = {
  title: 'COMPONENTS/Table',
  component: DsfrTableComponent,
  parameters: {},
  decorators: [
    moduleMetadata({
      imports: [
        CommonModule,
        DsfrButtonModule,
        DsfrButtonsGroupModule,
        DsfrFormInputModule,
        DsfrDataTableModule,
        DsfrTooltipButtonComponent,
      ],
    }),
  ],
};
export default meta;

type Story = StoryObj<DsfrTableComponent>;

const randomData = [
  { id: '1', name: 'Smith', firstName: 'John', birthDate: '1980-01-15', city: 'New York' },
  { id: '2', name: 'Johnson', firstName: 'Emma', birthDate: '1992-03-22', city: 'Los Angeles' },
  { id: '3', name: 'Williams', firstName: 'Aaliyah', birthDate: '1985-05-30', city: 'Chicago' },
  { id: '4', name: 'Brown', firstName: 'Emily', birthDate: '1990-07-07', city: 'Houston' },
  { id: '5', name: 'Garcia', firstName: 'Carlos', birthDate: '1978-09-12', city: 'Phoenix' },
  { id: '6', name: 'Jones JonesJones', firstName: 'Sophia', birthDate: '1983-11-21', city: 'Philadelphia' },
  { id: '7', name: 'Miller', firstName: 'Michael', birthDate: '1975-12-05', city: 'San Antonio' },
  { id: '8', name: 'Davis', firstName: 'Chloe', birthDate: '1988-02-19', city: 'San Diego' },
  { id: '9', name: 'Martinez', firstName: 'Juan', birthDate: '1994-04-25', city: 'Dallas' },
  { id: '10', name: 'Taylor', firstName: 'Aisha', birthDate: '1982-08-16', city: 'San Jose' },
];

const randomDataLongLine = [
  ...randomData,

  {
    name: 'Lorem ipsum di Lorem ipsum dolor sit ame  Lorem ipsum di Lorem ipsum dolor sit ame  Lorem ipsum di Lorem ipsum dolor sit ame  Lorem ipsum di Lorem ipsum dolor sit ame   Lorem ipsum di Lorem ipsum dolor sit ame  Lorem ipsum di Lorem ipsum dolor sit ame  Lorem ipsum di Lorem ipsum dolor sit ame  ',
    firstName: 'Juan',
    birthDate: '1994-04-25',
    city: 'Dallas',
  },
  { name: 'Taylor', firstName: 'Aisha', birthDate: '1982-08-16', city: 'San Jose' },
];

const randomDataPagination = [
  { id: '1', name: 'Smith', firstName: 'John', birthDate: '1980-01-15', city: 'New York' },
  { id: '2', name: 'Johnson', firstName: 'Emma', birthDate: '1992-03-22', city: 'Los Angeles' },
  { id: '3', name: 'Williams', firstName: 'Aaliyah', birthDate: '1985-05-30', city: 'Chicago' },
  { id: '4', name: 'Brown', firstName: 'Emily', birthDate: '1990-07-07', city: 'Houston' },
  { id: '5', name: 'Garcia', firstName: 'Carlos', birthDate: '1978-09-12', city: 'Phoenix' },
  { id: '6', name: 'Jones', firstName: 'Sophia', birthDate: '1983-11-21', city: 'Philadelphia' },
  { id: '7', name: 'Miller', firstName: 'Michael', birthDate: '1975-12-05', city: 'San Antonio' },
  { id: '8', name: 'Davis', firstName: 'Chloe', birthDate: '1988-02-19', city: 'San Diego' },
  { id: '9', name: 'Martinez', firstName: 'Juan', birthDate: '1994-04-25', city: 'Dallas' },
  { id: '10', name: 'Taylor', firstName: 'Aisha', birthDate: '1982-08-16', city: 'San Jose' },
  { id: '11', name: 'Anderson', firstName: 'Liam', birthDate: '1993-06-12', city: 'Austin' },
  { id: '12', name: 'Thomas', firstName: 'Olivia', birthDate: '1987-11-05', city: 'Jacksonville' },
  { id: '13', name: 'Hernandez', firstName: 'Noah', birthDate: '1979-03-29', city: 'Fort Worth' },
  { id: '14', name: 'Moore', firstName: 'Isabella', birthDate: '1991-09-15', city: 'Columbus' },
  { id: '15', name: 'Martin', firstName: 'Ethan', birthDate: '1986-12-22', city: 'Charlotte' },
  { id: '16', name: 'Jackson', firstName: 'Ava', birthDate: '1981-07-18', city: 'San Francisco' },
  { id: '17', name: 'Thompson', firstName: 'James', birthDate: '1995-05-07', city: 'Indianapolis' },
  { id: '18', name: 'White', firstName: 'Mia', birthDate: '1984-04-20', city: 'Seattle' },
  { id: '19', name: 'Lopez', firstName: 'Alexander', birthDate: '1990-08-30', city: 'Denver' },
  { id: '20', name: 'Lee', firstName: 'Amelia', birthDate: '1989-10-10', city: 'Washington' },
  { id: '21', name: 'Gonzalez', firstName: 'Daniel', birthDate: '1982-11-15', city: 'Boston' },
  { id: '22', name: 'Harris', firstName: 'Evelyn', birthDate: '1994-01-25', city: 'El Paso' },
  { id: '23', name: 'Clark', firstName: 'Matthew', birthDate: '1977-06-17', city: 'Detroit' },
  { id: '24', name: 'Lewis', firstName: 'Sofia', birthDate: '1996-02-28', city: 'Nashville' },
  { id: '25', name: 'Robinson', firstName: 'David', birthDate: '1983-04-12', city: 'Memphis' },
  { id: '26', name: 'Walker', firstName: 'Ella', birthDate: '1980-09-25', city: 'Portland' },
  { id: '27', name: 'Perez', firstName: 'Lucas', birthDate: '1988-12-19', city: 'Oklahoma City' },
  { id: '28', name: 'Hall', firstName: 'Avery', birthDate: '1991-07-03', city: 'Las Vegas' },
  { id: '29', name: 'Young', firstName: 'Joseph', birthDate: '1975-08-07', city: 'Louisville' },
  { id: '30', name: 'Allen', firstName: 'Harper', birthDate: '1986-03-11', city: 'Baltimore' },
];

const defaultColumns: DsfrColumn[] = [
  { label: 'Nom', field: 'name', sortable: false },
  { label: 'Prénom', field: 'firstName', sortable: false, verticalAlign: 'bottom' },
  { label: 'Date de naissance', field: 'birthDate', sortable: false },
  { label: 'Ville', field: 'city' },
];

export const Default: Story = {
  args: {
    columns: defaultColumns,
    data: randomData,
    caption: 'Titre du tableau (caption)',
  },
};

export const TableSM: Story = {
  args: {
    columns: defaultColumns,
    tableOptions: { cellSize: 'SM' },
    data: randomData,
    caption: 'Tableau avec cellules en taille SM (MD par défaut)',
  },
};

export const WithVerticalBorders: Story = {
  args: {
    columns: defaultColumns,
    tableOptions: { bordered: true },
    data: randomData,
    caption: 'Tableau avec bordures verticales',
  },
};

export const NoScroll: Story = {
  args: {
    columns: [
      { label: 'Nom', field: 'name', sortable: true },
      { label: 'Prénom', field: 'firstName', sortable: true },
      { label: 'Date de naissance', field: 'birthDate', sortable: true },
      { label: 'Ville', field: 'city' },
    ],
    data: randomData,
    dataKey: 'id',
    tableOptions: { selectable: true, noScroll: true },
    caption: 'Tableau avec taille fixe (noScroll)',
  },
};

// TODO Colonnes de taille minimales

// todo expliciter alignement verticaux + horizontaux
export const WithSpecificAlignment: Story = {
  args: {
    columns: [
      { label: 'Nom', field: 'name', verticalAlign: 'top' },
      { label: 'Prénom', field: 'firstName', verticalAlign: 'bottom' },
      { label: 'Date de naissance', field: 'birthDate', textAlign: 'center' },
      { label: 'Ville', field: 'city', textAlign: 'right' },
    ],
    tableOptions: { bordered: true },
    data: randomData,
    caption: 'Tableau avec alignements de cellules ',
  },
};

// TODO : changer titre en retour a la ligne automatique ?
export const WithMultiline: Story = {
  args: {
    columns: defaultColumns,
    tableOptions: { bordered: true, allowWrap: true },
    data: randomDataLongLine,
    caption: 'Tableau avec retour à la ligne automatique des éléments (multiline)',
  },
};

// TODO : tableau avec titre invislbe

export const CaptionBottom: Story = {
  args: {
    columns: defaultColumns,
    data: randomData,
    tableOptions: { bottomCaption: true },
    caption: 'Titre du tableau en bas',
  },
};

export const Selectable: Story = {
  args: {
    pagination: true,
    columns: [
      { label: 'Nom', field: 'name', sortable: true },
      { label: 'Prénom', field: 'firstName', sortable: true },
      { label: 'Date de naissance', field: 'birthDate', sortable: true },
      { label: 'Ville', field: 'city' },
    ],
    data: randomDataPagination,
    tableOptions: { selectable: true, showSelectAll: true },
    dataKey: 'id',
    rowsOptions: [{ id: '5', disableSelect: true }],
    initialSelection: [randomData[2].id, randomData[3].id],
    caption: 'Titre du tableau',
  },
};

// TODO : tableau a double entrée

function sortByBirthDate(a: any, b: any, order: 'asc' | 'desc') {
  return (a.birthDate < b.birthDate ? -1 : 1) * (order === 'asc' ? 1 : -1);
}

export const Sortable: Story = {
  args: {
    columns: [
      { label: 'Nom', field: 'name', sortable: true },
      { label: 'Prénom', field: 'firstName', sortable: true },
      { label: 'Date de naissance', field: 'birthDate', sortable: true, sortFunction: sortByBirthDate },
      { label: 'Ville', field: 'city' },
    ],
    data: randomData,
    caption: 'Titre du tableau',
  },
};

export const Pagination: Story = {
  args: {
    columns: [
      { label: 'Nom', field: 'name', sortable: true },
      { label: 'Prénom', field: 'firstName', sortable: true },
      { label: 'Date de naissance', field: 'birthDate', sortable: true },
      { label: 'Ville', field: 'city' },
    ],
    pagination: true,
    data: randomDataPagination,
    initialState: { page: 2, rowsPerPage: 10, sort: null },
    caption: 'Tableau avec pagination',
  },
};

// With header
const templateHeader = `<dsfr-datatable
  [caption]="'Titre du tableau (caption)'"
  [columns]="columns"
  [showHeaderViews]="true"
  [tableOptions]="tableOptions"
  [showSearch]="true"
  (viewSelect)="onviewSelect($event)"
  (searchSelect)="onSearch($event)"
  [data]="data">
  <ng-template #headerActionsTemplate>
    <dsfr-buttons-group inline="always" textAlign="right">
      <dsfr-button label="Bouton 1"></dsfr-button>
      <dsfr-button variant="secondary" label="Bouton 2"></dsfr-button>
    </dsfr-buttons-group>
  </ng-template>
  <ng-template #alternativeViewTemplate> <p>Votre autre vue à définir</p> </ng-template>
</dsfr-datatable>`;

export const WithHeader: Story = {
  render: (args) => ({
    props: {
      ...args,
    },
    template: templateHeader,
  }),
  args: {
    columns: defaultColumns,
    data: randomData,
    tableOptions: { selectable: true },
    caption: 'Titre du tableau (caption)',
  },
};

// With footer
const templateFooter = `<dsfr-datatable
  [caption]="'Titre du tableau (caption)'"
  [pagination]="true"
  [columns]="columns"
  [data]="data">
  <ng-template #footerActionsTemplate>
    <dsfr-buttons-group inline="always" alignment="right">
      <dsfr-button label="Mon bouton 1"></dsfr-button>
      <dsfr-button variant="secondary" label="Mon bouton 2"></dsfr-button>
    </dsfr-buttons-group>
  </ng-template>
  <ng-template #footerResultsTemplate>
  {{data.length}} résultats
  </ng-template>
</dsfr-datatable>`;

export const WithFooter: Story = {
  render: (args) => ({
    props: {
      ...args,
    },
    template: templateFooter,
  }),
  args: {
    columns: defaultColumns,
    data: randomDataPagination,
    caption: 'Titre du tableau (caption)',
  },
};

const templateCol = `
<dsfr-datatable
  [caption]="'Titre du tableau (caption)'"
  [pagination]="true"
  [dataKey]="dataKey"
  [tableOptions]="tableOptions"
  [columns]="columns"
  [data]="data">
    <ng-template #tableHeaderTemplate>
    <tr>
      <th dsfrSelectAll></th>
      <th [dsfrColumn]="columns[0]" scope="col">
        <span> {{ columns[0].label }} </span>
      </th>
      <th [dsfrColumn]="columns[1]" scope="col">
        <span> {{ columns[1].label }} </span>
      </th>
      <th [dsfrColumn]="columns[2]" scope="col">
        <span> {{ columns[2].label }} </span>
      </th>
      <th [dsfrColumn]="columns[3]" scope="col">
        <span> {{ columns[3].label }} <dsfr-tooltip-button [tooltip]="'Aide'"></dsfr-tooltip-button> </span>
      </th>
    </tr>
    </ng-template>
</dsfr-datatable>`;

export const TemplateColumns: Story = {
  render: (args) => ({
    props: {
      ...args,
    },
    template: templateCol,
  }),
  args: {
    columns: defaultColumns,
    data: randomDataPagination,
    tableOptions: { selectable: true },
    dataKey: 'id',
    caption: 'Titre du tableau (caption)',
  },
};

const templateRows = `
<dsfr-datatable
  [caption]="'Titre du tableau (caption)'"
  [pagination]="true"
  [dataKey]="dataKey"
  [columns]="columns"
  [rowsOptions]="rowsOptions"
  [tableOptions]="tableOptions"
  [data]="data">
  <ng-template #tableBodyTemplate let-row let-rowOptions="rowOptions">
    <tr>
      <th [disableSelect]="rowOptions?.disableSelect" [dsfrSelectRow]="row">
       <span class="fr-sr-only" *ngIf="rowOptions?.disableSelect"> Non sélectionnable  </span>
      </th>
      <td>{{ row.name }}</td>
      <td>{{ row.firstName }} </td>
      <td>{{ row.birthDate }}</td>
      <td> <dsfr-button [label]="row.city" variant="secondary" size="SM"></dsfr-button> </td>
    </tr>
  </ng-template>
</dsfr-datatable>`;

export const TemplateRows: Story = {
  render: (args) => ({
    props: {
      ...args,
    },
    template: templateRows,
  }),
  args: {
    columns: defaultColumns,
    dataKey: 'id',
    tableOptions: { selectable: true, showSelectAll: true },
    data: randomDataPagination,
    rowsOptions: [{ id: '5', disableSelect: true }],
    caption: 'Titre du tableau (caption)',
  },
};

const dataComplex = [
  {
    id: 1,
    col0: 'Français',
    col1: 'Mathématiques',
    col2: 'Sciences',
    col3: 'LV1',
  },
  {
    id: 2,
    col0: 'Mathématiques',
    col1: 'Histoire - Géographie ',
    col2: 'LV2',
    col3: 'EPS',
  },
  {
    id: 3,
    col0: 'LV1',
    col1: 'Devoirs faits',
    col2: 'Français',
    col3: 'Devoirs faits',
  },
  {
    id: 4,
    col0: 'LV1',
    col1: 'Histoire - Géographie ',
    col2: 'Physique-Chimie',
    col3: 'Education musicale',
  },
  {
    id: 5,
    col0: 'Sciences',
    col1: 'Vie de classe',
    col2: 'Arts appliqués',
    col3: 'Histoire & Géographie',
  },
  {
    id: 6,
    col0: 'Sciences',
    col1: 'Physique-Chimie',
    col2: 'Arts appliqués',
    col3: 'Physique-Chimie',
  },
  {
    id: 7,
    col0: 'Histoire & Géographie',
    col1: 'Education musicale',
    col2: 'Mathématiques',
    col3: 'Mathématiques',
  },
];

const complexTable = `
<dsfr-datatable
  [caption]="caption"
  [tableOptions]="tableOptions"
  [showHeaderViews]="true"
  [showSearch]="true"
  [rowsOptions]="rowsOptions"
  [pagination]="true"
  [dataKey]="'id'"
  (viewSelect)="onviewSelect($event)"
  (searchSelect)="onSearchSubmit($event)"
  [columns]="columns"
  [data]="data">
  <ng-template #alternativeViewTemplate> <p>Votre autre vue à définir</p> </ng-template>
  <!-- Summary obligatoire pour un tableau complexe -->
  <ng-template #headerSummaryTemplate> <p>Le résumé de mon tableau complexe</p></ng-template>
  <ng-template #headerActionsTemplate>
    <dsfr-buttons-group inline="always" textAlign="right">
      <dsfr-button label="Action groupée 1"></dsfr-button>
      <dsfr-button variant="secondary" label="Action groupée 2"></dsfr-button>
    </dsfr-buttons-group>
  </ng-template>

  <!-- Définition du header -->
  <ng-template #tableHeaderTemplate>
    <tr>
      <th id="header-col-0" dsfrSelectAll rowspan="2"></th>
      <th id="header-col-1" [dsfrColumn]="columns[0]">
        {{ columns[0].label }}
      </th>
      <th id="header-col-2" [dsfrColumn]="columns[1]">
        {{ columns[1].label }}
      </th>
      <th id="header-col-3" [dsfrColumn]="columns[2]" colspan="2">
        <span> {{ columns[2].label }}</span>
        <div class="fr-cell__desc"> Exemple de 2 cellules fusionnées dans le Header </div>
      </th>
      <th id="header-col-4" [dsfrColumn]="columns[4]">
        <div>
          <div>{{ columns[4].label }} <dsfr-tooltip-button [tooltip]="'Aide'"></dsfr-tooltip-button></div>
        </div>
      </th>
    </tr>
    <tr>
      <th id="subheader-1" headers="header-col-1">Groupes 1 & 2</th>
      <th id="subheader-2" headers="header-col-2">Groupes 1 & 2</th>
      <th id="subheader-3" headers="header-col-3">Groupe 1</th>
      <th id="subheader-4" headers="header-col-3">Groupe 2</th>
      <th id="subheader-5" headers="header-col-4">Groupes 1 & 2</th>
    </tr>
  </ng-template>

  <!-- Définition des lignes -->
  <ng-template #tableBodyTemplate let-row let-rowIndex="index" let-rowOptions="rowOptions">
    <tr>
      <th id="complex-row-{{rowIndex}}" headers="header-col-0" [dsfrSelectRow]="row" [disableSelect]="rowOptions?.disableSelect"><span> Titre</span></th>
      <td headers="complex-row-{{rowIndex}} header-col-1 subheader-1">{{ row.col0 }}</td>
      <td headers="complex-row-{{rowIndex}} header-col-2 subheader-2"><dsfr-form-input label="Label input" [labelSrOnly]="true" [maxLength]="10" /></td>
      <!-- Fusion sur toute la ligne pour la ligne 3 -->
      <ng-container *ngIf="row.id === 4">
        <td headers="complex-row-{{rowIndex}} header-col-3 header-col-4 subheader-3 subheader-4 subheader-5" colspan="3"> Etude dirigée. 
Exemple de colspan sur toute la ligne </td>
      </ng-container>
      <ng-container *ngIf="row.id !== 4">
        <td headers="complex-row-{{rowIndex}} header-col-3 subheader-3">{{ row.col1 }}</td>
        <!-- Fusion avant-derniere cellule entre cellule de la ligne 5 et cellule de la ligne 6 -->
        <td *ngIf="row.id !== 6" 
        [attr.headers]="row.id === 5 ? 'complex-row-'+ rowIndex + ' complex-row-'+ (rowIndex+1) +' header-col-3 subheader-4' : 'complex-row-'+ rowIndex+ ' header-col-3 subheader-4'"  
        [attr.rowspan]="row.id === 5 ? 2 : undefined">
          {{ row.col2 }}
        </td>
        <td headers="complex-row-{{rowIndex}} header-col-4 subheader-5">{{ row.col3 }}</td>
      </ng-container>
    </tr>
  </ng-template>

  <ng-template #footerActionsTemplate>
    <dsfr-buttons-group inline="always" alignment="right">
      <dsfr-button label="Action tableau 1"></dsfr-button>
      <dsfr-button variant="secondary" label="Action tableau 2"></dsfr-button>
    </dsfr-buttons-group>
  </ng-template>
</dsfr-datatable>
`;

export const ComplexTable: Story = {
  render: (args) => ({
    props: {
      ...args,
    },
    template: complexTable,
  }),
  args: {
    columns: [
      { field: 'col0', label: 'Lundi', minWidth: 'MD' },
      { field: 'col1', label: 'Mardi', minWidth: 'SM' },
      { field: 'col2', label: 'Mercredi & Jeudi', minWidth: 'LG' },
      { field: 'col3', label: 'Mercredi & Jeudi', minWidth: 'LG' },
      { field: 'col4', label: 'Vendredi', minWidth: 'LG' },
    ],
    dataKey: 'id',
    tableOptions: { selectable: true, bordered: true },
    data: dataComplex,
    rowsOptions: [{ id: 1, disableSelect: true }],
    caption: 'Tableau complexe (avec fusion de cellules) ',
  },
};
