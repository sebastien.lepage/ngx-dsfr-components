import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, TemplateRef, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrSearchBarModule } from '../../search-bar';
import { DsfrSegmentedControlModule } from '../../segmented-control';
import { DsfrSegmentedControl } from '../../segmented-control/segmented-control.model';

/**
 * Composant en-tête du tableau.
 * Il peut contenir quatre éléments optionnel mais dans cet ordre :
 * - le nombre de lignes sélectionnées
 * - une barre de recherche
 * - des boutons d’actions liés à la sélection de lignes
 * - un contrôle segmenté.
 */
@Component({
  selector: 'dsfr-table-header',
  templateUrl: './table-header.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  styleUrls: ['table-header.component.scss'],
  imports: [CommonModule, FormsModule, DsfrSegmentedControlModule, DsfrSearchBarModule],
})
export class EdutableHeaderComponent {
  /** Template optionnel afficher des boutons d'actions dans footer end */
  @Input() headerActionsTemplate!: TemplateRef<any>;

  /** Affficher une barre de recherche dans le header */
  @Input() showSearch = false;

  /** Nombre de lignes sélectionnées */
  @Input() countSelectedRows: number | undefined;

  /** Afficher le changement de type d'affichage (tableau / liste) */
  @Input() showHeaderViews: boolean;

  /** Contrôle segmenté contenant les types d'affichages (tableau / liste par défaut)  */
  @Input() headerViewsOptions: DsfrSegmentedControl[];

  /** Changement de type de vue */
  @Output() viewSelect = new EventEmitter<string>();

  /** Emet le texte lors de l'évènement keyup sur l'input de recherche.  */
  @Output() searchChange = new EventEmitter<string>();

  /** Emet le texte lors du clic sur le bouton "rechercher".  */
  @Output() searchSelect: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Changement de type d'affichage (liste ou tableau)
   * @internal */
  public onViewSelect(value: string): void {
    this.viewSelect.emit(value);
  }

  /** @internal */
  onSearch(e: string) {
    this.searchChange.emit(e);
  }

  /** @internal */
  onSearchClick(e: string) {
    this.searchSelect.emit(e);
  }
}
