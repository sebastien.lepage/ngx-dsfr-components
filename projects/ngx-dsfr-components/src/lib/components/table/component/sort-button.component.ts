import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewEncapsulation, inject } from '@angular/core';
import { I18nService } from '../../../shared';
import { DsfrSortColumn } from '../table.model';

/**
 * Composant d'affichage du bouton de tri pour une colonne (icônes flêche)
 */
@Component({
  selector: 'edu-sort-button-header',
  standalone: true,
  encapsulation: ViewEncapsulation.None,
  imports: [CommonModule],
  styles: ['.fr-btn--sort {margin-left: 1rem}'], // fix margin alignement sort icon and text
  template: `
    <button
      (click)="sortTable()"
      type="button"
      [attr.aria-sorting]="activeSort && activeSort.field === field ? activeSort.order : undefined"
      class="fr-btn--sort fr-btn fr-btn--sm">
      {{ i18n.t('commons.sort') }}
    </button>
  `,
})
export class SortButtonHeaderComponent {
  @Input() field: string;
  @Output() sortChanged = new EventEmitter<string>();

  _activeSort: DsfrSortColumn | undefined;

  /** @internal */
  public i18n = inject(I18nService);

  get activeSort(): DsfrSortColumn | undefined {
    return this._activeSort;
  }

  /** Tri actif actuel sur le tableau : désactiver le bouton si un autre tri est effectué (pas de multisort) */
  @Input() set activeSort(activeSort: DsfrSortColumn | undefined) {
    this._activeSort = activeSort;
  }

  sortTable() {
    this.sortChanged.emit(this.field);
  }
}
