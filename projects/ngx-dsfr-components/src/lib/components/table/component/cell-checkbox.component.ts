import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewEncapsulation, inject } from '@angular/core';
import { I18nService } from '../../../shared';

/**
 * Composant de sélection pour une colonne (checkbox)
 * fixme : créer composant standalone checkbox lorsque le DSFR aura fix la version sans aria-label
 */
@Component({
  selector: 'dsfr-cell-checkbox',
  standalone: true,
  styleUrls: ['cell-checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  imports: [CommonModule],
  template: `
    <div (click)="onSelectRow()" class="fr-checkbox-group fr-checkbox-group--sm">
      <input
        class="fr-table-checkbox"
        name="row-select"
        id="table-select-checkbox-{{ tableId }}--{{ row?.index }}"
        type="checkbox" />
      <label class="fr-label" for="table-select-checkbox-{{ tableId }}--{{ row?.index }}">
        <span class="fr-sr-only">
          {{ showSelectAll ? 'Tout sélectionner' : i18n.t('table.selectRow') + ' ' + row?.index }}
        </span>
      </label>
    </div>
  `,
})
export class DsfrCellCheckboxComponent {
  @Input() tableId: string;
  @Input() row: any;
  @Input() showSelectAll: boolean;
  @Output() selectRow = new EventEmitter<boolean>();

  /** @internal */
  public i18n = inject(I18nService);

  private _checked: boolean | undefined;

  get checked(): boolean | undefined {
    return this._checked;
  }

  @Input() set checked(checked: boolean | undefined) {
    this._checked = checked;
  }

  @Input() set isAllChecked(isAllChecked: boolean) {
    if (this.row.index === 'all') {
      this.checked = isAllChecked;
    }
  }

  onSelectRow() {
    this.checked = !this.checked;
    this.selectRow.emit(this.checked);
  }
}
