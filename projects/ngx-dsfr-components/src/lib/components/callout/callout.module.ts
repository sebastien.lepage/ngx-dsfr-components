import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrCalloutComponent } from './callout.component';

@NgModule({
  declarations: [DsfrCalloutComponent],
  exports: [DsfrCalloutComponent],
  imports: [CommonModule],
})
export class DsfrCalloutModule {}
