import { argEventEmitter } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrSearchBarComponent } from './search-bar.component';

const meta: Meta = {
  title: 'COMPONENTS/SearchBar',
  component: DsfrSearchBarComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  argTypes: {
    searchChange: { control: argEventEmitter },
    searchSelect: { control: argEventEmitter },
  },
};
export default meta;

type Story = StoryObj<DsfrSearchBarComponent>;

export const Default: Story = {
  args: {},
};

export const Large: Story = {
  args: {
    large: true,
  },
};
