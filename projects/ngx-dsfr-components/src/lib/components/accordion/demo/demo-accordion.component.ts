import { CommonModule } from '@angular/common';
import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { DsfrAccordionComponent } from '../accordion.component';
import { DsfrAccordionModule } from '../accordion.module';

@Component({
  selector: 'demo-accordion',
  template: `
    <div class="sb-title">
      Modification dynamique de la propriété <code>expanded</code>
      <button class="fr-btn fr-btn--sm fr-btn--secondary" type="button" (click)="toggleExpanded()">
        Toggle expanded
      </button>
    </div>
    <dsfr-accordion heading="My accordion heading" #acc>
      <p>
        Donec fermentum, tortor ut ullamcorper porta, nisl mauris convallis odio, id tincidunt odio nibh eget sapien.
        Vestibulum sit amet nibh laoreet enim varius egestas quis ut erat. Proin scelerisque mi felis, ut posuere ligula
        sodales non. Praesent et mattis metus. Phasellus bibendum leo neque, eget aliquam leo finibus eu. Duis viverra
        magna ac felis iaculis, et elementum risus interdum. Maecenas finibus placerat libero a porta
      </p>
    </dsfr-accordion>
  `,
  styles: ['.sb-title button { margin-left: 30px; }'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrAccordionModule],
})
export class DemoAccordionComponent {
  @ViewChild('acc') acc!: DsfrAccordionComponent;

  toggleExpanded() {
    this.acc.expanded = !this.acc.expanded;
  }
}
