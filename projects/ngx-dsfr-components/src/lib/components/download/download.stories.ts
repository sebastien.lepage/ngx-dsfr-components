import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFileSizeUnitConst } from '../../shared';
import { DsfrTranslateModule } from '../translate';
import { DsfrDownloadVariantConst } from './download-variant';
import { DsfrDownloadComponent } from './download.component';
import { DsfrMimeTypeConst } from './dsfr-mime.type';

// TODO Revoir les stories à la mode MDX
const meta: Meta = {
  title: 'COMPONENTS/Download',
  component: DsfrDownloadComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrTranslateModule],
    }),
  ],
  argTypes: {
    mimeType: { control: { type: 'select' }, options: Object.values(DsfrMimeTypeConst) },
    sizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    variant: { control: { type: 'inline-radio' }, options: Object.values(DsfrDownloadVariantConst) },
  },
};
export default meta;

type Story = StoryObj<DsfrDownloadComponent>;

const componentLang = `<dsfr-translate [languages]="[{ value: 'fr', label: 'Français' },{ value: 'en', label: 'English' }]" currentCode="en"></dsfr-translate>`;
//const decoratorLang = componentWrapperDecorator((story) => `${componentLang}${story}`);
const decoratorLang = componentWrapperDecorator((story) => `${story}`);

export const Default: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024,
  },

  decorators: [decoratorLang],
};

export const DocEN: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024,
    langCode: 'en',
  },

  decorators: [decoratorLang],
};

export const NoFileSize: Story = {
  args: {
    link: '#',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  },
};

export const Direct: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024,
    directDownload: true,
  },
};

export const NewFileName: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024,
    directDownload: 'Nouveau nom',
  },
};

export const Bloc: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024,
    variant: DsfrDownloadVariantConst.BLOCK,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla est purus, ultrices in porttitor\n' +
      'in, accumsan non quam. Nam consectetur porttitor rhoncus. Curabitur eu est et leo feugiat\n' +
      'auctor vel quis lorem.',
    langCode: 'la', // attr.data-fr-assess-file
  },
};

export const BlocButton: Story = {
  args: {
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024 + 1224,
    variant: DsfrDownloadVariantConst.BLOCK,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla est purus, ultrices in porttitor\n' +
      'in, accumsan non quam. Nam consectetur porttitor rhoncus. Curabitur eu est et leo feugiat\n' +
      'auctor vel quis lorem.',
    langCode: 'la', // attr.data-fr-assess-file
  },
};
