export type DsfrMimeType = T[keyof T];
type T = typeof DsfrMimeTypeConst;

/** cf. https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types */
export namespace DsfrMimeTypeConst {
  // TEXT
  export const TEXT_CSS = 'text/css';
  export const TEXT_CSV = 'text/csv';
  export const TEXT_HTML = 'text/html';
  export const TEXT_CALENDAR = 'text/calendar';
  export const TEXT_PLAIN = 'text/plain';
  export const TEXT_XML = 'text/xml';

  // FONT
  export const FONT_OTF = 'font/otf';
  export const FONT_TTF = 'font/ttf';
  export const FONT_WOFF = 'font/woff';
  export const FONT_WOFF2 = 'font/woff2';

  // IMAGE
  export const IMAGE_BMP = 'image/bmp';
  export const IMAGE_GIF = 'image/gif';
  export const IMAGE_ICON = 'image/x-icon';
  export const IMAGE_JPEG = 'image/jpeg';
  export const IMAGE_PNG = 'image/png';
  export const IMAGE_SVG = 'image/svg+xml';
  export const IMAGE_TIFF = 'image/tiff';
  export const IMAGE_WEBP = 'image/webp';

  // AUDIO
  export const AUDIO_3GPP = 'audio/3gpp';
  export const AUDIO_3GPP2 = 'audio/3gpp2';
  export const AUDIO_ACC = 'audio/aac';
  export const AUDIO_OGG = 'audio/ogg';
  export const AUDIO_MIDI = 'audio/midi';
  export const AUDIO_WAV = 'audio/x-wav';
  export const AUDIO_WEBM = 'audio/webm';

  // VIDEO
  export const VIDEO_3GPP = 'video/3gpp';
  export const VIDEO_3GPP2 = 'video/3gpp2';
  export const VIDEO_MPEG = 'video/mpeg';
  export const VIDEO_OGG = 'video/ogg';
  export const VIDEO_WEBM = 'video/webm';

  // APPLICATION
  export const APPLICATION_ABIWORD = 'application/x-abiword';
  export const APPLICATION_CSH = 'application/x-csh';
  export const APPLICATION_EPUB = 'application/epub+zip';
  export const APPLICATION_FLASH = 'application/x-shockwave-flash';
  export const APPLICATION_FORM_URLENCODED = 'application/x-www-form-urlencoded';
  export const APPLICATION_JAVASCRIPT = 'application/javascript';
  export const APPLICATION_JSON = 'application/json';
  export const APPLICATION_OCTET_STREAM = 'application/octet-stream';
  export const APPLICATION_OGG = 'application/ogg';
  export const APPLICATION_PDF = 'application/pdf';
  export const APPLICATION_RTF = 'application/rtf';
  export const APPLICATION_TYPESCRIPT = 'application/typescript';
  export const APPLICATION_XML = 'application/xml';
  export const APPLICATION_XML_ATOM = 'application/atom+xml';
  export const APPLICATION_XML_SVG = 'application/svg+xml';
  export const APPLICATION_XML_XHTML = 'application/xhtml+xml';
  export const APPLICATION_XML_XUL = 'application/vnd.mozilla.xul+xml';

  // ARCHIVE
  export const ARCHIVE_7Z_COMPRESSED = 'application/x-7z-compressed';
  export const ARCHIVE_BZIP = 'application/x-bzip';
  export const ARCHIVE_BZIP2 = 'application/x-bzip2';
  export const ARCHIVE_JAVA = 'application/java-archive';
  export const ARCHIVE_GZIP = 'application/gzip';
  export const ARCHIVE_RAR_COMPRESSED = 'application/x-rar-compressed';
  export const ARCHIVE_TAR = 'application/x-tar';
  export const ARCHIVE_RAR = 'application/vnd.rar';
  export const ARCHIVE_ZIP = 'application/zip';
  export const ARCHIVE_ZIP_COMPRESSED = 'application/x-zip-compressed';

  // MICROSOFT
  export const MS_FONT_OBJECT = 'application/vnd.ms-fontobject';
  export const MS_EXCEL = 'application/vnd.ms-excel';
  export const MS_EXCEL_XML = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  export const MS_POWERPOINT = 'application/vnd.ms-powerpoint';
  export const MS_POWERPOINT_XML = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
  export const MS_VIDEO = 'video/x-msvideo';
  export const MS_VISIO = 'application/vnd.visio';
  export const MS_WORD = 'application/msword';
  export const MS_WORD_XML = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';

  // Autres
  export const MULTIPART_FORM_DATA = 'multipart/form-data';
  export const SCRIPT_SHELL = 'application/x-sh';
  export const OPEN_PRESENTATION = 'application/vnd.oasis.opendocument.presentation';
  export const OPEN_SPREADSHEET = 'application/vnd.oasis.opendocument.spreadsheet';
  export const OPEN_TEXT = 'application/vnd.oasis.opendocument.text';
}

export function convertMimeType2FileFormat(mineType: string): FileFormat | undefined {
  const element = mimeType2FileFormat.find((e) => e[0] === mineType);
  return element ? element[1] : undefined;
}

enum FileFormat {
  XLS = 'XLS',
  XLSX = 'XLSX',
  PPT = 'PPT',
  PPTX = 'PPTX',
  DOC = 'DOC',
  DOCX = 'DOCX',
  ODS = 'ODS',
  ODP = 'ODP',
  ODT = 'ODT',
  XML = 'XML',
  JPG = 'JPG',
  PNG = 'PNG',
  PDF = 'PDF',
  CSV = 'CSV',
  HTML = 'HTML',
  SVG = 'SVG',
  TXT = 'TXT',
  ZIP = 'ZIP',
  SEVEN_ZIP = '7Z',
  RAR = 'RAR',
  XHTML = 'XHTML',
  BMP = 'BMP',
  GIF = 'GIF',
  TIF = 'TIF',
  RTF = 'RTF',
  GZIP = 'GZIP',
  TAR = 'TAR',
}

const mimeType2FileFormat: [DsfrMimeType | string, FileFormat][] = [
  [DsfrMimeTypeConst.APPLICATION_PDF, FileFormat.PDF],
  [DsfrMimeTypeConst.APPLICATION_RTF, FileFormat.RTF],
  [DsfrMimeTypeConst.APPLICATION_XML, FileFormat.XML],
  [DsfrMimeTypeConst.APPLICATION_XML_ATOM, FileFormat.XML],
  [DsfrMimeTypeConst.APPLICATION_XML_XHTML, FileFormat.XHTML],

  [DsfrMimeTypeConst.ARCHIVE_7Z_COMPRESSED, FileFormat.SEVEN_ZIP],
  [DsfrMimeTypeConst.ARCHIVE_GZIP, FileFormat.GZIP],
  [DsfrMimeTypeConst.ARCHIVE_RAR, FileFormat.RAR],
  [DsfrMimeTypeConst.ARCHIVE_TAR, FileFormat.TAR],
  [DsfrMimeTypeConst.ARCHIVE_ZIP, FileFormat.ZIP],
  [DsfrMimeTypeConst.ARCHIVE_ZIP_COMPRESSED, FileFormat.ZIP],

  [DsfrMimeTypeConst.IMAGE_BMP, FileFormat.BMP],
  [DsfrMimeTypeConst.IMAGE_GIF, FileFormat.GIF],
  [DsfrMimeTypeConst.IMAGE_JPEG, FileFormat.JPG],
  [DsfrMimeTypeConst.IMAGE_PNG, FileFormat.PNG],
  [DsfrMimeTypeConst.IMAGE_SVG, FileFormat.SVG],
  [DsfrMimeTypeConst.IMAGE_TIFF, FileFormat.TIF],

  [DsfrMimeTypeConst.MS_EXCEL, FileFormat.XLS],
  [DsfrMimeTypeConst.MS_EXCEL_XML, FileFormat.XLSX],
  [DsfrMimeTypeConst.MS_POWERPOINT, FileFormat.PPT],
  [DsfrMimeTypeConst.MS_POWERPOINT_XML, FileFormat.PPTX],
  [DsfrMimeTypeConst.MS_WORD, FileFormat.DOC],
  [DsfrMimeTypeConst.MS_WORD_XML, FileFormat.DOCX],

  [DsfrMimeTypeConst.OPEN_PRESENTATION, FileFormat.ODP],
  [DsfrMimeTypeConst.OPEN_SPREADSHEET, FileFormat.ODS],
  [DsfrMimeTypeConst.OPEN_TEXT, FileFormat.ODT],

  [DsfrMimeTypeConst.TEXT_CSV, FileFormat.CSV],
  [DsfrMimeTypeConst.TEXT_HTML, FileFormat.HTML],
  [DsfrMimeTypeConst.TEXT_PLAIN, FileFormat.TXT],
  [DsfrMimeTypeConst.TEXT_XML, FileFormat.XML],
];
