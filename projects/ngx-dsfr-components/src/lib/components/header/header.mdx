import { Canvas, Controls, Meta } from '@storybook/blocks';
import * as Stories from './header.stories';

<Meta of={Stories} title="Header" />

# En-tête (header)

L’en-tête permet aux utilisateurs d’identifier sur quel site ils se trouvent. Il peut donner accès à la recherche et à
certaines pages ou fonctionnalités clés. Il permet également d'avoir un menu, simple ou avec sous-éléments.

[Cf. Système Design de l'État - Header](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/en-tete)
[Cf. Système Design de l'État - Menu](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/navigation-principale)

- _Module_ : `DsfrHeaderModule`
- _Composant_ : `DsfrHeaderComponent`
- _Tag_ : `dsfr-header`

## Fonctionnement

L'en-tête propose nativement plusieurs fonctionnalités en plus de son système de navigation :

- **Une barre de recherche.**
  Bien que son affichage soit dépendant de l'input `searchBar`, cet élément ne propose aucune fonctionnalité par elle même.
  Vous devrez donc prendre vous même en main les évènements émits par les outputs `searchChange` et `searchSelect` (voir API ci-dessous).
- **Un bouton de changement de langue.**
  Ce dernier est directement lié au service `i18nService` et se charge donc de la traduction en interne.
  Son affichage est conditionné par l'attribution de l'input `translate`.
- **Un bouton de changement de mode clair/sombre.**
  Il permet de passer du mode clair au mode sombre pour l'ensemble de l'application.
  Son fonctionnement en entièrement assuré par le composant `dsfr-header`.
  Son affichage est conditionné par l'input `display`.

## Aperçu

<Canvas of={Stories.Default} />

## API

<Controls />

## Exemples

### Menus

Définition des modèles pour le Menu :

```typescript
export interface DsfrHeaderMenuItem extends DsfrLink {
  expanded?: boolean;
  megaMenu?: MegaMenu;
  subItems?: DsfrHeaderMenuItem[];
}
export interface DsfrMegaMenuCategory {
  label?: string;
  subItems: DsfrLink[];
}

export interface DsfrMegaMenuLeader {
  title?: string;
  text?: string;
  link?: DsfrLink;
}

export interface DsfrMegaMenu {
  leader?: DsfrMegaMenuLeader;
  categories: DsfrMegaMenuCategory[];
}
```

### Menu complexe

<Canvas of={Stories.MegaMenu} />

Exemple de données en input de [menu] pour la création d'un menu avec menu déroulant et méga menu :

```typescript
const menuHeader: DsfrMenuItemHeader[] = [
  { label: 'Accès direct', href: '#' },
  { label: 'Menu déroulant', href: '#', subItems: [{ label: 'Accès routerLink' }, { label: 'Accès direct 3' }] },
  {
    label: 'Méga menu (actif)',
    active: true,
    megaMenu: {
      leader: {
        title: 'Titre éditorialisé',
        text: 'Lorem [...] elit ut.',
        link: { label: 'Voir toute la rubrique' },
      },
      categories: [
        { label: 'Catégorie 1', subItems: [{ label: 'Lien 1' }, { label: 'Lien 3' }] },
        {
          label: 'Catégorie 2',
          subItems: [
            { label: 'Lien 1' },
            { label: 'Lien actif', routerLink: '/', active: true, routerLinkActiveValue: 'class-active' },
            { label: 'Lien 3' },
          ],
        },
      ],
    },
  },
];
```

### Paramètres d'affichage

Pour activer les paramètres d'affichage vous pouvez activer l'input `display`.
Vous serez limité à 2 accès rapides (headerToolsLinks) et non 3.

<Canvas of={Stories.Display} />

Dans les cas où vous voulez :

- utiliser un libellé de lien personalisé
- modifier le positionnement du lien au niveau des accès rapides
- avoir le contrôle sur la localisation de la modale `dsfr-display`
- paramétrer l'identifiant de la modale

Vous pouvez reprendre la main :

- positionner le composant `dsfr-diplay` à l'endroit voulu dans le DOM
- ajouter un accès rapide :

```json
{
  mode: 'button',
  label: "Paramètres d'affichage",
  customClass: 'fr-icon-theme-fill',
  ariaControls: DISPLAY_MODAL_ID,
}
```

Attention, sous cette forme, le paramètre d'affichage est considéré comme un lien rapide, dont la limite à afficher est de 3.

<Canvas of={Stories.CustomDisplay} />

### Accès rapides : ouverture d'une modale avec un lien

Les liens d'accès rapide peuvent être considérés soit comme des liens soit comme des boutons.
Pour utiliser un lien d'accès rapide (toolLink) par exemple pour contrôler l'ouverture d'une modale. :

- Définir le mode du lien en tant que `button`. Le DOM généré correspondra alors à un bouton pour l'accessibilité.
- Si il doit contrôler l'ouverture d'une modale, définir le `ariaControls` correspondant à l'id de la modale.
- (optionnel) renseigner l'attribut `route` pour contrôler son action programmatiquement avec l'évènement linkSelect global du menu

```javascript
const headerToolsLinks: DsfrLink[] = [
  {
    label: 'Se déconnecter',
    icon: 'fr-icon-logout-box-r-line',
    route: 'logout',
    ariaControls: 'logoutModal',
    mode: 'button',
  },
];
```

Cf. Story [Accessibility](/story/components-header--accessibility)
Cf. [Documentation de la modale](/docs/components-modal--docs#ouverture-avec-un-lien)

### Slot supplémentaire

Dans certains cas particuliers, on peut souhaiter afficher une information supplémentaire, non prévue par défaut, dans le header.
Deux points d'injections (slot) vous permettent de personnaliser une partie du header, en version desktop et en version mobile.

Ces slots sont nommés :

- `headerTools` en version desktop,
- `headerToolsMobile` en version mobile.

Ils doivent être tous les deux remplis afin d'assurer un comportement cohérent en cas de changement de taille d'écran.

_Exemple_

```html
<ng-template #headerTools>
  <dsfr-link [route]="'/test'" label="Lien custom 1" (linkSelect)="onLinkSelect($event)"></dsfr-link>
</ng-template>
<ng-template #headerToolsMobile>
  <dsfr-link [route]="'/test'" label="Lien custom mobile" (linkSelect)="onLinkSelect($event)"></dsfr-link>
</ng-template>
```

⚠️ ⚠️ Cet élément n'est pas un élément officiel du DSFR. Il est donc de votre responsabilité d'assurer son accessibilité.
