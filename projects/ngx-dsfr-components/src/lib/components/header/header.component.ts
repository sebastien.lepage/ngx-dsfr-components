import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { DsfrLink, DsfrNavigation, I18nService, newUniqueId } from '../../shared';
import { DSFR_CONFIG_TOKEN } from '../../shared/config/config-token';
import { DsfrConfig } from '../../shared/config/config.model';
import { DISPLAY_MODAL_ID } from '../display';
import { DsfrHeaderMenuItem, DsfrHeaderTranslate } from './header.model';

@Component({
  selector: 'dsfr-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrHeaderComponent implements OnInit, AfterViewInit {
  /**@internal */
  @ViewChild('toolLinksMobile') toolLinksMobile: ElementRef;
  /**@internal */
  @ViewChild('toolLinks') toolLinks: ElementRef;
  /**@internal */
  @ViewChild('translateComponent', { read: ElementRef }) translateComponent: ElementRef;

  /** Slot custom de toolLinks supplémentaire sur le header pour la version desktop */
  @ContentChild('headerTools', { static: true }) headerTools: TemplateRef<any>;

  /** Slot custom de toolLinks supplémentaire sur le header pour la version mobile */
  @ContentChild('headerToolsMobile', { static: true }) headerToolsMobile: TemplateRef<any>;

  /**
   * Boolean pour afficher le tag Beta (pour un site en beta).
   */
  @Input() beta = false;

  /**
   * Identifiant de la navigation principale, généré par défaut
   */
  @Input() navigationId: string;

  /**
   * Chemin vers le répertoire exposant les pictogrammes illustratifs DSFR.
   */
  @Input() artworkDirPath: string;

  /**
   * Label associé au bloc marque (Marianne). Respectez la structure avec les <br>.
   * (Ministère, gouvernement, république française)
   */
  @Input() logoLabel = 'République<br/>Française';

  /**
   * Tooltip associé au lien 'retour accueil' du logo de la Marianne.
   */
  @Input() logoTooltipMessage: string;

  /** Path pour src d'image d'illustration. */
  @Input() operatorImagePath: string;
  /**
   * Text alternatif à utiliser uniquement si l'image à une information à passer.
   */
  @Input() operatorImageAlt: string;

  /** Boolean pour passer le logo opérateur en mode vertical.  */
  @Input() operatorImageVertical = false;

  /** Nom du service. */
  @Input() serviceTitle: string;

  /** Tagline du service. */
  @Input() serviceTagline: string;

  /** Tableau d'items du menu. */
  @Input() menu: DsfrHeaderMenuItem[] = [];

  /**
   * Permet d'afficher la barre de recherche.
   */
  @Input() searchBar = false;

  /**
   * Permet d'afficher le selecteur de langue pour l'internationalisation.
   *
   * @see DsfrHeaderTranslate
   */
  @Input() translate: DsfrHeaderTranslate;

  /** Renvoi la valeur de l'input de la barre de recherche au changement.  */
  @Output() readonly searchChange: EventEmitter<string> = new EventEmitter();

  /** Renvoi la valeur de l'input de la barre de recherche au clic sur rechercher.  */
  @Output() readonly searchSelect: EventEmitter<string> = new EventEmitter();

  /** Evènement émis au clic sur un lien en mode `route`. */
  @Output() readonly linkSelect = new EventEmitter<DsfrLink>();

  /** Evenement émis au changement de langue (si showTranslate). Il contient le code de la langue. */
  @Output() readonly langChange = new EventEmitter<string>();

  /** @internal */
  public searchInputId = newUniqueId();

  /**
   * Nombre maximum de liens d'accès rapides (3)
   *
   * @internal
   */
  maxToolsLinks = 3;

  /**
   * @internal
   */
  _useDeprecatedPictoPath = false;

  /**
   * @internal
   */
  showDisplay: boolean;

  private _headerToolsLinks: DsfrLink[]; // ne pas initialiser à [] pour StoryBook
  private _logoLink: DsfrNavigation = { link: '' }; // valeur par défaut pour la rétrocompatibilité

  /** @internal */
  constructor(
    @Inject(DSFR_CONFIG_TOKEN) private config: DsfrConfig,
    public i18n: I18nService,
    private renderer: Renderer2,
  ) {}

  get display(): boolean {
    return this.showDisplay;
  }

  get headerToolsLinks(): DsfrLink[] {
    return this._headerToolsLinks;
  }

  get displayModalId() {
    return DISPLAY_MODAL_ID;
  }

  get pictoPath(): string {
    return this.artworkDirPath;
  }

  /** @internal */
  get logoNavigation(): DsfrNavigation {
    return this._logoLink;
  }

  /**
   * Url du lien 'retour accueil' du logo de la Marianne.
   */
  @Input() set logoLink(value: string | DsfrNavigation) {
    if (typeof value === 'string' || !value) {
      this._logoLink = { link: value };
    } else {
      this._logoLink = value;
    }
  }

  /** Affichage du lien 'Paramètre d'affichage' pour gérer les modes clair/sombre. */
  @Input() set display(value: boolean) {
    this.showDisplay = value;
    this.maxToolsLinks = this.showDisplay ? 2 : 3;
  }

  /**
   * Tableau de lien d'accès rapide.
   * Les icônes acceptés sont ceux du DSFR y compris ceux du tableau `DsfrBtnIcon`.
   */
  @Input() set headerToolsLinks(links: DsfrLink[]) {
    if (links && links.length > this.maxToolsLinks) {
      if (this.showDisplay) {
        console.warn(
          "Avec l'utilisation du paramètre d'affichage, le nombre d'accès rapides (tools links) est limité à " +
            this.maxToolsLinks,
        );
      } else {
        console.warn("Le nombre d'accès rapides (tools links) est limité à " + this.maxToolsLinks);
      }
    }
    this._headerToolsLinks = links;
    this.duplicateToolsLinksMobile();
  }

  /**
   * Chemin des pictogrammes (du composant display) renseigné par le développeur.
   *
   * Note: ce chemin doit permettre de récupérer directement les fichiers SVG suivants : moon.svg, sun.svg, system.svg
   *
   * @deprecated Use `artworkDirPath` instead.
   */
  @Input() set pictoPath(path: string) {
    this.artworkDirPath = path;
    this._useDeprecatedPictoPath = true;
  }

  /** internal */
  ngOnInit(): void {
    if (this.artworkDirPath === undefined) {
      this.artworkDirPath = this.config.artworkDirPath;
    }
    this.navigationId ??= newUniqueId();
  }

  /** internal */
  ngAfterViewInit(): void {
    this.duplicateToolsLinksMobile();
  }

  /**
   * Fix: evenements non dupliqués par le script DSFR pour les liens (toolslinks) version mobile
   * On prend la main sur le clic de la div parente en cas de lien 'route' pour envoyer l'event linkSelect
   * @param event element cliqué
   * @internal
   */
  onSelectLinkMobile(event: Event) {
    if (!this._headerToolsLinks) return;
    const parent = (event.target as HTMLElement)?.parentElement;
    const item = this._headerToolsLinks.find((l) => l.label === parent?.getAttribute('data-item'));
    if (item && item.route && !item.routerLink) {
      event.preventDefault();
      this.linkSelect.emit(item);
    }
  }

  /** @internal */
  hasToolsLinks(): boolean {
    return this.headerToolsLinks?.length > 0 || this.translate?.languages?.length > 0 || this.display;
  }

  /** @internal */
  onLink(item: DsfrLink) {
    this.linkSelect.emit(item);
  }

  /** @internal */
  onSearchSubmit(text: string) {
    this.searchSelect.emit(text);
  }

  /** @internal */
  onSearchChange(text: string) {
    this.searchChange.emit(text);
  }

  /** @internal */
  onLanguageChange(codeLang: string) {
    this.langChange.emit(codeLang);
  }

  /** @internal */
  onMegaMenuClose(item: DsfrHeaderMenuItem) {
    item.expanded = false;
  }

  /** @internal */
  onMenuItemClick(item: DsfrHeaderMenuItem) {
    this.menu.forEach((i) => (i.expanded = false));
    item.expanded = true;
  }

  /** @internal */
  getCustomClassHeaderToolsLink(item: DsfrLink) {
    const customClass = ['fr-btn'];
    if (item.icon) customClass.push(item.icon);
    if (item.customClass) customClass.push(item.customClass);
    return customClass.join(' ');
  }

  /**
   * Dupliquer les liens toolsLinks pour le menu mobile
   * /!\ interaction script DSFR : le script copie les liens la premiere fois, mais n'écoute pas les changements
   * Les liens à l'interieur de fr-header__tools-links sont dupliqués dans fr-header__menu-links
   */
  private duplicateToolsLinksMobile() {
    if (this.toolLinksMobile?.nativeElement) {
      // timeout pour laisser le temps du rendu des éléments  dans toolsLinks
      setTimeout(() => {
        const childElements: HTMLCollection = this.toolLinksMobile.nativeElement.children;
        // suppression des noeuds précédent
        Array.from(childElements).forEach((e) => {
          this.renderer.removeChild(this.toolLinksMobile.nativeElement, e);
        });
        // copie des liens de menu
        if (this.toolLinks?.nativeElement) {
          this.renderer.appendChild(this.toolLinksMobile.nativeElement, this.toolLinks.nativeElement.cloneNode(true));
        }
        // Dupliquer le composant translate
        if (this.translate) {
          if (this.translateComponent) {
            this.renderer.appendChild(this.toolLinksMobile.nativeElement, this.translateComponent.nativeElement);
            this.translateComponent.nativeElement.style.display = 'block';
          }
        }
        if (this.headerToolsMobile) {
          this.renderer.appendChild(
            this.toolLinksMobile.nativeElement,
            this.headerToolsMobile.elementRef.nativeElement,
          );
        }
      });
    }
  }
}
