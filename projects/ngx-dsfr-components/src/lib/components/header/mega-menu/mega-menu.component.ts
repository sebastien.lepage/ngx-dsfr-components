import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrLink, I18nService } from '../../../shared';
import { DsfrLinkModule } from '../../link';
import { ItemLinkComponent } from '../../link/item-link.component';
import { DsfrMegaMenu } from './mega-menu.model';

@Component({
  selector: 'dsfr-mega-menu',
  templateUrl: './mega-menu.component.html',
  styleUrls: ['./mega-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
  imports: [CommonModule, FormsModule, ItemLinkComponent, DsfrLinkModule],
  standalone: true,
})
export class DsfrMegaMenuComponent {
  @Input() megaMenu: DsfrMegaMenu;
  @Input() idMenu: number;

  /**Emet l'évènement Event du DOM à la sélection d'un lien */
  @Output() linkSelect = new EventEmitter<DsfrLink>();

  /**Emet l'évènement Event du DOM à la fermeture du méga menu */
  @Output() closeSelect = new EventEmitter<Event>();

  constructor(public i18n: I18nService) {}

  onLink(event: DsfrLink) {
    this.linkSelect.emit(event);
  }

  onClose() {
    this.closeSelect.emit();
  }
}
