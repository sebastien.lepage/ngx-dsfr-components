import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrLinkModule } from '../link';
import { DsfrBackToTopComponent } from './backtotop.component';

@NgModule({
  declarations: [DsfrBackToTopComponent],
  exports: [DsfrBackToTopComponent],
  imports: [CommonModule, DsfrLinkModule],
})
export class DsfrBackToTopModule {}
