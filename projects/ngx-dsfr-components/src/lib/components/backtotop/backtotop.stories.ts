import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrLinkModule } from '../link';
import { DsfrBackToTopComponent } from './backtotop.component';

const meta: Meta = {
  title: 'COMPONENTS/Back to Top',
  component: DsfrBackToTopComponent,
  decorators: [moduleMetadata({ imports: [DsfrLinkModule, RouterTestingModule] })],
};
export default meta;
type Story = StoryObj<DsfrBackToTopComponent>;

export const Default: Story = {
  args: {},
};
