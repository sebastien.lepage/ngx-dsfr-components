import { Component, ViewEncapsulation } from '@angular/core';
import { I18nService } from '../../shared';

@Component({
  selector: 'dsfr-backtotop',
  templateUrl: './backtotop.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBackToTopComponent {
  /** @internal */
  backToTopLabel = this.i18n.t('backtotop.topOfPage');

  constructor(private i18n: I18nService) {}
}
