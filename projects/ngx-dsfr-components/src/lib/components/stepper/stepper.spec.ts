import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DsfrHeadingLevelConst } from '../../shared';
import { DsfrStepperComponent } from './stepper.component';
import { DsfrStepperModule } from './stepper.module';

describe('DsfrStepperComponent', () => {
  let component: DsfrStepperComponent;
  let fixture: ComponentFixture<DsfrStepperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrStepperModule],
    }).compileComponents();

    fixture = TestBed.createComponent(DsfrStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have h3 heading level', () => {
    component.headingLevel = DsfrHeadingLevelConst.H3;
    component.currentStepTitle = 'Titre de l’étape en cours';
    fixture.detectChanges();
    const headingElt = fixture.nativeElement.querySelector('h3.fr-stepper__title');
    expect(headingElt).not.toBeNull();
  });
});
