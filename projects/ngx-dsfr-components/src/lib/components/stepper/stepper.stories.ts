import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrHeadingLevelConst, HeadingModule } from '../../shared';
import { DemoToolbarComponent } from '../../shared/components/demo/demo-toolbar.component';
import { DsfrStepperComponent } from './stepper.component';
const meta: Meta = {
  title: 'COMPONENTS/Stepper',
  component: DsfrStepperComponent,
  decorators: [moduleMetadata({ imports: [DemoToolbarComponent, HeadingModule] })],
};
export default meta;

const steps = ['Formation et enseignements', 'Informations responsable', 'Informations élève', 'Validation'];
const currentStep = 1;
const nextStepTitle = currentStep < steps.length ? steps[currentStep] : undefined;

export const Default: StoryObj<DsfrStepperComponent> = {
  render: (args) => ({
    props: args,
  }),

  args: {
    totalSteps: steps.length,
    currentStep: currentStep,
    currentStepTitle: steps[currentStep - 1],
    nextStepTitle: nextStepTitle,
  },
};

export const HeadingLevel: StoryObj<DsfrStepperComponent> = {
  args: {
    totalSteps: steps.length,
    currentStep: currentStep,
    currentStepTitle: steps[currentStep - 1],
    nextStepTitle: nextStepTitle,
    headingLevel: DsfrHeadingLevelConst.H3,
  },
  render: (args) => ({
    props: args,
    template: `
      <demo-toolbar></demo-toolbar>
      <dsfr-stepper
        [totalSteps]="totalSteps"
        [currentStep]="currentStep"
        [currentStepTitle]="currentStepTitle"
        [nextStepTitle]="nextStepTitle"
        [headingLevel]="headingLevel">
        </dsfr-stepper>
    `,
  }),
};
