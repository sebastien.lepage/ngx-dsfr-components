import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel, DsfrHeadingLevelConst, I18nService } from '../../shared';

@Component({
  selector: 'dsfr-stepper',
  templateUrl: './stepper.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrStepperComponent {
  /**
   * Nombre total d'étapes.
   */
  @Input() totalSteps = 1;

  /**
   * Index de l'étape courante.
   */
  @Input() currentStep = 1;

  /**
   * Titre de l'étape courante.
   */
  @Input() currentStepTitle: string;

  /**
   * Titre de l'étape suivante.
   */
  @Input() nextStepTitle: string;

  /**
   * Le niveau du titre dans la structure, ne change pas l'apparence, `<h2>` par défaut.
   *
   * @since 1.11.8
   */
  @Input() headingLevel: DsfrHeadingLevel = DsfrHeadingLevelConst.H2;

  /** @internal */
  constructor(public i18n: I18nService) {}
}
