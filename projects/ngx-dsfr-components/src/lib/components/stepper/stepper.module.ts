import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeadingModule } from '../../shared';
import { DsfrStepperComponent } from './stepper.component';

@NgModule({
  declarations: [DsfrStepperComponent],
  exports: [DsfrStepperComponent],
  imports: [CommonModule, HeadingModule],
})
export class DsfrStepperModule {}
