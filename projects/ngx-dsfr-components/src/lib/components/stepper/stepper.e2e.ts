import { expect, Locator, test } from '@playwright/test';

test('stepper.heading-level', async ({ page }) => {
  await page.goto('?path=/story/components-stepper--heading-level');
  const frame = page.frameLocator('#storybook-preview-iframe');

  let dropdown: Locator = await frame.locator('.fr-collapse');
  await expect(dropdown).not.toBeVisible();

  const stepperTitle: Locator = await frame.locator('h3.fr-stepper__title');
  await expect(stepperTitle).toBeVisible();

  const translateBtn: Locator = await frame.locator('.fr-translate__btn');
  let lang = await translateBtn.textContent();
  lang = String(lang).trim().substring(0, 2);
  let langAlt = lang === 'FR' ? 'en' : 'fr';
  langAlt = 'en';
  console.log('langAlt', langAlt);
  await translateBtn.click();

  await expect(dropdown).toBeVisible();

  const langLink = await dropdown.locator(`a.fr-translate__language[lang="${langAlt}"]`);
  await expect(langLink).toBeAttached();
  //   await expect(langLink).toBeVisible();
  //   await langLink.click();
  //TODO: (RPA) le code ci-dessus ne fonctionne pas car le lien n'est jamais visible et donc non clickable (??)
  // du coup je suis passé en dispatchant l'event et là ça fonctionne
  await langLink.dispatchEvent('click');

  const expectedStepLabel = langAlt === 'fr' ? 'Étape' : 'Step';
  const stepperStateElt = await frame.locator('.fr-stepper__state');
  await expect(stepperStateElt).toContainText(expectedStepLabel);

  const expectedDetailsLabel = langAlt === 'fr' ? 'Étape suivante :' : 'Next step:';
  const stepperDetailsElt = await frame.locator('.fr-stepper__details > span');
  await expect(stepperDetailsElt).toContainText(expectedDetailsLabel);
});
