import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { EduPageLinkComponent } from './page-link.component';
import { DsfrPaginationComponent } from './pagination.component';

const meta: Meta = {
  title: 'COMPONENTS/Pagination',
  component: DsfrPaginationComponent,
  decorators: [moduleMetadata({ declarations: [EduPageLinkComponent] })],
  argTypes: {
    // https://storybook.js.org/docs/angular/essentials/controls#conditional-controls
    currentPage: { if: { arg: 'previousOnly', truthy: false } },
    pageCount: { if: { arg: 'previousOnly', truthy: false } },
    pageSelect: { control: argEventEmitter },
    pageSelectEvent: { control: argEventEmitter },
    backSelect: { control: argEventEmitter },
    backEvent: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrPaginationComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Page précédente'),
  args: {
    previousOnly: true,
    currentPage: 1,
    pageCount: 132,
  },
};

export const FirstPage: Story = {
  decorators: dsfrDecorator('Première page'),
  args: {
    ariaLabel: 'Pagination exemple première page',
    previousOnly: false,
    currentPage: 1,
    pageCount: 132,
  },
};

export const MiddlePage: Story = {
  decorators: dsfrDecorator('Page intermédiaire'),
  args: {
    previousOnly: false,
    currentPage: 128,
    pageCount: 132,
  },
};

export const LastPage: Story = {
  decorators: dsfrDecorator('Dernière page'),
  args: {
    previousOnly: false,
    currentPage: 132,
    pageCount: 132,
  },
};
