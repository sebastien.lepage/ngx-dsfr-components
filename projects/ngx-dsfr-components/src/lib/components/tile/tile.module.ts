import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LinkDownloadComponent, PictogramModule } from '../../shared';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrLinkModule } from '../link';
import { DsfrTagsGroupModule } from '../tags-group';
import { DsfrTileComponent } from './tile.component';

@NgModule({
  declarations: [DsfrTileComponent],
  exports: [DsfrTileComponent],
  imports: [
    CommonModule,
    DsfrBadgesGroupModule,
    DsfrTagsGroupModule,
    DsfrLinkModule,
    LinkDownloadComponent,
    PictogramModule,
  ],
})
export class DsfrTileModule {}
