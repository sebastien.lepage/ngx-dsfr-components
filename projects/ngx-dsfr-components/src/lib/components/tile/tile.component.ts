import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from '../card';
import { BasePanelComponent } from '../card/base-panel.component';

@Component({
  selector: 'dsfr-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrTileComponent extends BasePanelComponent {
  /**
   * Chemin vers le répertoire racine contenant les pictogrammes illustratifs DSFR.
   */
  @Input() artworkDirPath = 'artwork';

  /**
   * Chemin relatif à artworkDirPath dénotant le fichier d'illustration à utiliser.
   */
  @Input() artworkFilePath: string;

  /** @deprecated (@since 1.5.0) */
  @Input() imageAlt: string;

  /** @deprecated (@since 1.5.0) utiliser `artworkFilePath`. Path pour src d'image d'illustration. */
  @Input() imagePath: string;

  /**
   * Ce boolean permet de ne pas afficher d'icône (flèche).
   */
  @Input() noIcon = false;

  /**
   * Rotation d'une tuile verticale sur breakpoint 'MD' ou 'LG' (au redimensionnement du contenu)
   */
  @Input() rotateOn: 'MD' | 'LG' | undefined = undefined;

  /** @deprecated utiliser `customBackground`. Active le fond de la tuile en gris clair. */
  @Input() useGreyBackground: boolean = false;

  /**
   * Permet la gestion programmatique d'une navigation initiée au click sur le tile si l'input 'route' est valorisé.
   * La valeur de la propriété 'route' sera transmise.
   */
  @Output() tileSelect = new EventEmitter<string>();

  /** @internal */
  protected readonly backgroundConst = DsfrPanelBackgroundConst;
  /** @internal */
  protected readonly borderConst = DsfrPanelBorderConst;

  constructor() {
    super();
  }

  /** @deprecated (@since 1.9.0) utiliser `detailBottom` à la place */
  @Input() set detail(value: string) {
    this.detailBottom = value;
  }

  /**
   * Dans le cas d'une route, un événement `(routeSelect)` est émis avec la valeur de la route et
   * l'événement initial n'est pas propagé.
   *
   * @internal
   */
  onLinkSelect() {
    // on propage l'output, pas besoin de gérer ici un éventuel preventDefault si usage de route, c'est géré en amont
    if (this.route) {
      this.tileSelect.emit(this.route);
    }
  }
}
