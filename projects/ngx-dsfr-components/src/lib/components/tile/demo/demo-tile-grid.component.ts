import { CommonModule } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrBadge } from '../../badge';
import { DsfrTag } from '../../tag';
import { DsfrTileModule } from '../tile.module';

@Component({
  selector: 'demo-tile-grid',
  templateUrl: './demo-tile-grid.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrTileModule],
})
export class DemoTileGridComponent {
  @Input() horizontal = false;
  @Input() size: 'SM' | 'MD' | undefined = 'MD';

  /** @internal */
  readonly artworkFilePath = 'artwork/pictograms/buildings/city-hall.svg';
  /** @internal */
  readonly heading = 'Intitulé de la tuile';
  /** @internal */
  readonly description = 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididu';
  /** @internal */
  readonly detail = 'Détail (optionnel)';
  /** @internal */
  readonly route = '/maroute';
  /** @internal */
  readonly badges: DsfrBadge[] = [{ label: 'Label Badge', customClass: 'fr-badge--purple-glycine' }];
  /** @internal */
  readonly tags: DsfrTag[] = [{ label: 'label tag' }];

  get tileGridClass(): string {
    return '<div class="fr-col-12 fr-col-sm-6 fr-col-md-3 fr-col-lg-2">';
  }
}
