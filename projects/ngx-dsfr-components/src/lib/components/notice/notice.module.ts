import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrNoticeComponent } from './notice.component';

@NgModule({
  declarations: [DsfrNoticeComponent],
  exports: [DsfrNoticeComponent],
  imports: [CommonModule],
})
export class DsfrNoticeModule {}
