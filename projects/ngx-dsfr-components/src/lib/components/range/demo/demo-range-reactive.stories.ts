import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrRangeComponent } from '../range.component';
import { DemoRangeReactiveComponent } from './demo-range-reactive.component';

const meta: Meta = {
  title: 'COMPONENTS/Range',
  component: DemoRangeReactiveComponent,
  decorators: [moduleMetadata({ imports: [DsfrRangeComponent] })],
};
export default meta;
type Story = StoryObj<DemoRangeReactiveComponent>;

const label = 'Range avec reactiveForm';

export const ReactiveForm: Story = {
  args: {
    label: label,
    value: 33,
  },
};
