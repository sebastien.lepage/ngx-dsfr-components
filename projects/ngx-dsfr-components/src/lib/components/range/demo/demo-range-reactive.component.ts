import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrRangeComponent } from '../range.component';

@Component({
  selector: 'demo-range-reactive',
  templateUrl: './demo-range-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrRangeComponent],
})
export class DemoRangeReactiveComponent implements OnChanges {
  @Input() label: string;
  @Input() value: number;

  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      range: undefined,
    });
  }

  ngOnChanges({ value }: SimpleChanges) {
    if (value && value.currentValue !== value.previousValue) {
      this.formGroup.controls['range']?.setValue(this.value);
    }
  }

  getControlValue() {
    return this.formGroup.controls['range']?.value;
  }

  getControlType(): string {
    const value = this.getControlValue();
    return typeof value;
  }
}
