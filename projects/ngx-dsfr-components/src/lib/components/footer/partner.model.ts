/**
 * Représentation du bloc partenaire
 */
export interface DsfrPartner {
  /** Source de l'image */
  imagePath: string;
  /** L’alternative de l’image (attribut alt) doit impérativement être renseignée et reprendre le texte visible dans l’image **/
  imageAlt: string;
  /** Lien */
  link: string;
  /** Modifier la hauteur en rem */
  customHeight?: string;
}
