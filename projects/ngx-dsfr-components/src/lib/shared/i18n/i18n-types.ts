export interface DsfrI18nBaseBundle {
  commons: object;
  alert: object;
  backtotop: object;
  breadcrumb: object;
  consent: object;
  date: object;
  display: object;
  email: object;
  follow: object;
  footer: object;
  franceConnect: object;
  header: object;
  link: object;
  login: object;
  modal: object;
  notice: object;
  pageResponse: object;
  pagination: object;
  password: object;
  response: object;
  select: object;
  share: object;
  stepper: object;
  table: object;
  tag: object;
  tel: object;
  toggle: object;
  tooltip: object;
  transcription: object;
  translate: object;
  upload: object;
}

/**
 * La définition du type ci-dessous est tronquée car Typescript 5.1-5.6 ne contrôle pas le type sortant d'une
 * déstructuration. Exemple simple avec le code ci-dessous (syntaxe utilisée au sein de I18nService#extendsLabelsBundle)
 *
 * type Foo = { foo : string };
 * const foo = { foo:'toto' };
 * const qux:Foo = { ...{ foo:'toto' }, ...{bar:'tutu'} };
 */
export type DsfrI18nBundle = DsfrI18nBaseBundle; // & { [key: string]: object };

/**
 * Type permettant de contraindre les bundles d'extension. Permet d'empêcher l'écrasement des libellés existants.
 *
 * Explication : on utilise uen syntaxe en mapped types pour définir un type dont les clés sont des string et les
 * valeurs des objets (structure JSON) puis on ajoute que si la clé appartient aux cléés existantes alors le type
 * spécifié est never et comme never n'est pas un type possible en TS cela provoque une erreur de compilation.
 * Enfin on ajoute le point d'interrogation pour rendre optionnelle cette seconde contrainte sur le type des clés car
 * sinon le compilateur affiche une erreur car il considère que l'objet à assigner doit contenir la totalité des
 * propriétés de I18nBundle déclarées en type never (commons:never, alert:never, etc.)
 *
 * Compilation OK :
 * const ext: I18nExtensionBundle = { foo: { bar: 'qux' } };
 * Compilation KO :
 * const ext: I18nExtensionBundle = { commons: { bar: 'qux' } };
 */
export type DsfrI18nExtensionBundle = { [key: string]: object } & { [key in keyof DsfrI18nBaseBundle]?: never };
