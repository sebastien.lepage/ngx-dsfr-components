import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { DSFR_CONFIG_TOKEN } from './config-token';
import { DsfrConfig } from './config.model';

@NgModule({
  imports: [CommonModule],
})
export class DsfrConfigModule {
  static forRoot(config: DsfrConfig): ModuleWithProviders<DsfrConfigModule> {
    return {
      ngModule: DsfrConfigModule,
      providers: [{ provide: DSFR_CONFIG_TOKEN, useValue: config }],
    };
  }
}
