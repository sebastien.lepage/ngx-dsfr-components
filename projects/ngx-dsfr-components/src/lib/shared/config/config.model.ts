import { DsfrI18nConfig } from './i18n-config';
import { DsfrLogConfig } from './log-config';

/**
 * Ce modèle permet de configurer la librairie dans la méthode `DsfrConfigModule.forStatic()`.
 */
export interface DsfrConfig {
  /** Permet de renseigner globalement le chemin vers le répertoire exposant les pictogrammes illustratifs DSFR. */
  // TODO Rendre artworkDirPath optionnel si on utilise pas le composant display
  artworkDirPath: string;

  /** Permet d'ajouter un fichier de traduction. */
  i18n?: DsfrI18nConfig;

  /** Permet de préciser le niveau de log. */
  log?: DsfrLogConfig;
}
