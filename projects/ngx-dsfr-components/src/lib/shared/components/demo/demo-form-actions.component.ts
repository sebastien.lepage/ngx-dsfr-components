import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { DsfrButtonModule, DsfrButtonsGroupModule } from '../../../components';

@Component({
  selector: 'demo-form-actions',
  templateUrl: './demo-form-actions.component.html',
  standalone: true,
  imports: [CommonModule, DsfrButtonsGroupModule, DsfrButtonModule],
})
export class DemoFormActionsComponent {
  @Input() valid: boolean;
}
