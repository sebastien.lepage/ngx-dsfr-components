import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { DISPLAY_MODAL_ID, DsfrDisplayModule, DsfrTranslateModule } from '../../../components';

@Component({
  selector: 'demo-toolbar',
  templateUrl: './demo-toolbar.component.html',
  standalone: true,
  imports: [CommonModule, DsfrTranslateModule, DsfrDisplayModule],
})
export class DemoToolbarComponent {
  @Output() displayChange = new EventEmitter<string>();
  @Output() langChange = new EventEmitter<string>();

  protected readonly displayModalId = DISPLAY_MODAL_ID;

  protected readonly languages = [
    { value: 'fr', label: 'Français' },
    { value: 'en', label: 'English' },
  ];

  /** @Internal */
  onLangChange(codeLang: string) {
    this.langChange.emit(codeLang);
  }

  /** @Internal */
  onDisplayChange(display: string) {
    this.displayChange.emit(display);
  }
}
