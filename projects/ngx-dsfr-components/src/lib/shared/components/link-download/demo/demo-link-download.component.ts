import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { DsfrLink, DsfrPosition } from '../../../models';
import { LinkDownloadComponent } from '../link-download.component';

@Component({
  selector: 'demo-item-link',
  templateUrl: './demo-link-download.component.html',
  styles: ['edu-link-download {margin-right: 1rem}'],
  standalone: true,
  imports: [CommonModule, LinkDownloadComponent],
})
export class DemoLinkDownloadComponent {
  @Input() icon: string;
  @Input() iconPosition: DsfrPosition;
  @Input() label: string;
  @Input() route: string;
  @Input() mode: 'link' | 'button';

  /** @internal */
  get item(): DsfrLink {
    return {
      icon: this.icon,
      iconPosition: this.iconPosition,
      label: this.label,
      route: this.route,
      mode: this.mode,
    };
  }
}
