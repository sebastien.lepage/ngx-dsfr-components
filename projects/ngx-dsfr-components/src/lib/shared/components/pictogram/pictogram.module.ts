import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PictogramComponent } from './pictogram.component';

@NgModule({
  declarations: [PictogramComponent],
  exports: [PictogramComponent],
  imports: [CommonModule, RouterModule],
})
export class PictogramModule {}
