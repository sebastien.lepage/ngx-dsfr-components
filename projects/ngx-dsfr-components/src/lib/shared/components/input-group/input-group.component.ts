import { CommonModule } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrSeverity, DsfrSeverityConst } from '../../models';
import { isStringEmptyOrNull } from '../../utils';

/**
 * InputGroupComponent contient :
 * - Le label et l'aide textuel de l'input,
 * - Les messages de l'input
 * @since 1.11.0
 * @author pfontanet
 */
@Component({
  selector: 'edu-input-group',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './input-group.component.html',
  standalone: true,
  imports: [CommonModule],
})
export class InputGroupComponent {
  /** Libellé du champ. */
  @Input() label: string;

  /** Elément sur lequel porte le label. */
  @Input() inputId: string;

  /** Texte additionnel décrivant le champ.  */
  @Input() hint: string | undefined;

  /**
   * Permet de désactiver le champ.
   */
  @Input() disabled = false;

  /**
   * Message d'information lié au composant
   */
  @Input() message: string | undefined = undefined;

  /**
   * Représente la sévérité du message
   */
  @Input() severity: DsfrSeverity;

  /**
   * Id de la div affichant les messages d'erreur ou de validation.
   */
  @Input() messagesGroupId: string;

  // Afin de pouvoir utiliser le type dans l'HTML
  /** @internal */
  protected readonly severityConst = DsfrSeverityConst;

  /** @internal */
  protected hasMessage(severity: DsfrSeverity): boolean {
    return !isStringEmptyOrNull(this.message) && severity === this.severity;
  }
}
