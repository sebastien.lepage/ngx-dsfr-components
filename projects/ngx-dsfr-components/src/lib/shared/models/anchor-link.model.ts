/** Modèle de lien d'accès rapide pour accèder à des zones de la page */
export interface DsfrAnchorLink {
  /** Nom du lien */
  label: string;
  /** Lien href (exclusif avec fragment). */
  link?: string;
  /** Lien du fragment pour routerLink angular (exclusif avec link / route et prioritaire). */
  fragment?: string;
  /** Permet de gérer programmatiquement l'action lors d'un click sur un lien d'évitement. (prioritaire sur link) */
  route?: string;
}
