/**
 * Les tailles de texte sous forme énumérées.
 */
export namespace DsfrTextSizeConst {
  export const XS = 'XS';
  export const SM = 'SM';
  export const MD = 'MD';
  export const LG = 'LG';
  export const XL = 'XL';
}

/**
 * Les tailles DSFR.
 * @see https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-de-l-identite-de-l-etat/typographie > Corps de texte - Body
 */
export type DsfrTextSize = (typeof DsfrTextSizeConst)[keyof typeof DsfrTextSizeConst];
