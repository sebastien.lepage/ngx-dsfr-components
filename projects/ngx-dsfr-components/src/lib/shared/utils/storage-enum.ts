export enum StorageEnum {
  SCHEME = 'scheme', // display type
  REMEMBER_ME = 'rememberMe',
  LOGIN = 'login', // remember me dans la page de login
  // 'lang' pour la langue (dans _commons)
}
