/**
 * @author pfontanet
 * @since 0.7
 */
export class DateUtils {
  private constructor() {}

  /**
   * Transforme un string représentant une date au format `'dd/mm/yyyy'` en objet `Date`.
   * Le siècle peut-être sur 2 digits, dans ce cas, cela correspond à `'19yy'`.
   * @returns Date ou undefined
   */
  static parseDateFr(dateStr: string): Date | undefined {
    const regexp = /^(?<day>3[01]|0?[1-9]|[12][0-9])\/(?<month>1[0-2]|0?[1-9])\/(?<year>[0-9]{4})$/;
    return DateUtils.parseDate(dateStr, regexp);
  }

  /**
   * Transforme un string représentant une date au format ISO 8601 en objet `Date`.
   * @returns Date ou undefined
   */
  static parseDateIso(dateStr: string): Date | undefined {
    // https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s07.html
    const regexp =
      /^(?<year>-?(?:[1-9][0-9]*)?[0-9]{4})-(?<month>1[0-2]|0[1-9])-(?<day>3[01]|0[1-9]|[12][0-9])(T(?<hour>2[0-4]|[01][0-9]):(?<minute>[0-5][0-9]):(?<second>[0-5][0-9])(?<ms>\.[0-9]+)?(?<timezone>Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?)?$/;

    return DateUtils.parseDate(dateStr, regexp);
  }

  /**
   * Retourne une Date UTC, sans heure, minute seconde, à partir d'une date
   * @param date
   */
  static date2Utc(date: Date): Date {
    return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
  }

  /**
   * Retourne une Date UTC, sans heure, minute seconde, selon plusieurs formats en entrée
   * @param value
   */
  static dateUtcOf(value: Date | string | number | undefined | null): Date | undefined {
    let date: Date | undefined = undefined;

    if (typeof value === 'string') date = DateUtils.parseDateIso(value);
    else if (typeof value === 'number') date = DateUtils.numberToDateUtc(value);
    else if (value) date = DateUtils.date2Utc(value);

    return date;
  }

  private static parseDate(dateStr: string, regexp: RegExp): Date | undefined {
    if (!dateStr) return undefined;

    const execArr = regexp.exec(dateStr);
    const groups = execArr?.groups;
    const valid = !!groups && !!groups['year'] && !!groups['month'] && !!groups['day'];

    return !valid
      ? undefined
      : new Date(Date.UTC(Number(groups['year']), Number(groups['month']) - 1, Number(groups['day'])));
  }

  private static numberToDateUtc(n: number): Date {
    const d = new Date(n);
    return new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate()));
  }
}
