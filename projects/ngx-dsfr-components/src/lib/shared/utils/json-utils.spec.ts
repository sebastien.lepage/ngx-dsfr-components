import { describe, test } from '@jest/globals';
import { jsonPath2Value } from './json-utils';

const messages = {
  commons: {
    accept: 'Accepter',
    close: 'Fermer',
    sub: { disabled: 'Désactivé' },
  },
  labels: ['label0', 'label1', 'label2'],
};

describe('JSON', () => {
  test('value', () => {
    expect(jsonPath2Value(messages, 'commons.close')).toBe('Fermer');
    expect(jsonPath2Value(messages, 'commons.sub.disabled')).toBe('Désactivé');
    expect(jsonPath2Value(messages, 'labels[1]')).toBe('label1');
  });

  test('path not exists', () => {
    expect(jsonPath2Value(messages, 'commons.notakey')).toBeUndefined();
    expect(jsonPath2Value(messages, 'commons.notakey.disabled')).toBeUndefined();
  });

  test('array', () => {
    const labels = jsonPath2Value(messages, 'labels');
    expect(labels).toStrictEqual(['label0', 'label1', 'label2']);
  });
});
