import { describe, test } from '@jest/globals';
import { DateUtils } from './date-utils';

// L'argument de 'expect' doit être la valeur que votre code produit, et le paramètre du comparateur doit être la valeur correcte.
describe('DateUtils', () => {
  test('parseDateFr', () => {
    expect(DateUtils.parseDateFr('31/08/2024')).toEqual(new Date(Date.UTC(2024, 7, 31)));
    expect(DateUtils.parseDateFr('31/8/2024')).toEqual(new Date(Date.UTC(2024, 7, 31)));
    expect(DateUtils.parseDateFr('01/1/2025')).toEqual(new Date(Date.UTC(2025, 0, 1)));
    expect(DateUtils.parseDateFr('1/1/2025')).toEqual(new Date(Date.UTC(2025, 0, 1)));

    expect(DateUtils.parseDateFr('31/08/24')).toBeUndefined();
    expect(DateUtils.parseDateFr('32/1/2025')).toBeUndefined();
    expect(DateUtils.parseDateFr('1/13/2025')).toBeUndefined();
  });

  test('parseDateIso', () => {
    const august31 = new Date('2024-08-31T00:00:00.000Z');
    expect(DateUtils.parseDateIso('2024-08-31T00:00:00.000Z')).toEqual(august31);
    expect(DateUtils.parseDateIso('2024-08-31T23:00:00.000Z')).toEqual(august31);
    expect(DateUtils.parseDateIso('2024-08-31T24:00:00.000Z')).toEqual(august31);
    expect(DateUtils.parseDateIso('1995-12-17T00:00:00')).toEqual(new Date(Date.UTC(1995, 11, 17)));
    expect(DateUtils.parseDateIso('2024-08-31')).toEqual(august31); // @since 1.10

    expect(DateUtils.parseDateIso('2024-08-31T25:00:00.000Z')).toBeUndefined();
  });

  test('dateUtcOf', () => {
    const august31 = new Date('2023-08-31T00:00:00.000Z');

    // Chaine non valide
    expect(DateUtils.dateUtcOf('December 17, 1995 00:00:00')).toBeUndefined();

    // UTC string (non valide)
    expect(DateUtils.dateUtcOf('Tue, 31 Dec 2024 00:00:00 GMT')).toBeUndefined();

    // Chaine valide
    const actual = DateUtils.dateUtcOf('1995-12-17T00:00:00');
    const expected = new Date(Date.UTC(95, 11, 17));
    expect(actual).toEqual(expected);

    // ISO string (le Z est facultatif)
    expect(DateUtils.dateUtcOf('2023-08-31T00:00:00.000')).toEqual(august31);

    expect(DateUtils.dateUtcOf('2023-08-31T23:00:00.000Z')).toEqual(august31);

    // Le format court est accepté.
    expect(DateUtils.dateUtcOf('2023-08-31')).toEqual(august31);

    // on autorise l'heure à 24, mais on tronque
    expect(DateUtils.dateUtcOf('2023-08-31T24:00:00.000Z')).toEqual(august31);

    // Millisecondes
    const dateUtc = new Date(Date.UTC(2024, 1, 1));
    const ms = dateUtc.getTime();
    expect(DateUtils.dateUtcOf(ms)).toEqual(dateUtc);
  });
});

describe('Date', () => {
  test('constructor', () => {
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/Date
    const date1 = new Date('December 17, 1995 03:24:00');
    // Sun Dec 17 1995 03:24:00 GMT...

    const date2 = new Date('1995-12-17T03:24:00');
    // Sun Dec 17 1995 03:24:00 GMT...

    expect(date1).toEqual(date2);
  });

  test('UTC', () => {
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/UTC

    let utcDate = new Date(Date.UTC(96, 1, 2, 3, 4, 5));
    expect(utcDate.toUTCString()).toEqual('Fri, 02 Feb 1996 03:04:05 GMT');

    // Date locale avec décalage d'1 heure
    const dateLocal = new Date(96, 1, 2, 3, 4, 5);
    expect(dateLocal.toUTCString()).toEqual('Fri, 02 Feb 1996 02:04:05 GMT');
    expect(dateLocal).not.toEqual(utcDate);

    utcDate = new Date(Date.UTC(0, 0, 0, 0, 0, 0));
    expect(utcDate.toUTCString()).toEqual('Sun, 31 Dec 1899 00:00:00 GMT');
  });

  test('toDateString', () => {
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/toDateString
    const event = new Date(1993, 6, 28, 14, 39, 7);
    expect(event.toString()).toEqual('Wed Jul 28 1993 14:39:07 GMT+0200 (heure d’été d’Europe centrale)');
    expect(event.toDateString()).toEqual('Wed Jul 28 1993');
  });

  test('toUTCString', () => {
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/toUTCString
    const event = new Date('14 Jun 2017 00:00:00 PDT');
    expect(event.toUTCString()).toEqual('Wed, 14 Jun 2017 07:00:00 GMT');
    expect(event.toString()).toEqual('Wed Jun 14 2017 09:00:00 GMT+0200 (heure d’été d’Europe centrale)');
    expect(event.toLocaleString()).toEqual('14/06/2017 09:00:00');
    expect(event.toDateString()).toEqual('Wed Jun 14 2017');
    expect(event.toLocaleDateString()).toEqual('14/06/2017');
  });

  test('toISOString', () => {
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString
    const event = new Date('05 October 2011 14:48 UTC');
    expect(event.toISOString()).toEqual('2011-10-05T14:48:00.000Z');
  });
});
