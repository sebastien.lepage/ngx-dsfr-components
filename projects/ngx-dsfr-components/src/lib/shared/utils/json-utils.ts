/**
 * https://www.w3resource.com/JSON/JSONPath-with-JavaScript.php
 * @param obj : object|array This parameter represents the Object representing the JSON structure.
 * @param path : string This parameter represents JSONPath expression string.
 * @return une valeur extrait de l'objet Json ou undefined si le path est erroné
 */
export function jsonPath2Value(obj: any, path: string): any {
  const keys = path.split('.');
  // value est soit un objet JSON, soit une valeur terminale
  let value = obj;
  const breakException = {};
  try {
    keys.forEach((key) => {
      if (!value) throw breakException;
      // key, ex: 'labels[20]'
      const matchArr = key.match(/([^\[]*)\[(\d*)\]/);
      const k = matchArr ? matchArr[1] : undefined; // clé sans l'indice, ex : 'labels'
      const i = matchArr ? matchArr[2] : undefined; // indice, ex : 20
      value = k && i ? value[k][i] : value[key];
    });
  } catch (e) {
    // console.error(`Le chemin '${path}' ne correspond pas à une valeur`);
  }
  return value;
}
