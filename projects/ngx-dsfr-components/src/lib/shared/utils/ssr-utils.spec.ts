import { describe, test } from '@jest/globals';
import { isOnBrowser } from './ssr-utils';

describe('SSR', () => {
  test.skip('isOnBrowser', () => {
    expect(isOnBrowser()).toBe(false);
  });
});
