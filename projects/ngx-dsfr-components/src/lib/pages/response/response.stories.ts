import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrButtonModule } from '../../components/button';
import { DsfrButtonsGroupModule } from '../../components/buttons-group';
import { DsfrResponseComponent } from './response.component';

const meta: Meta = {
  title: 'PAGES/Response',
  component: DsfrResponseComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrButtonModule, DsfrButtonsGroupModule],
    }),
  ],
  argTypes: {
    contactSelect: { control: argEventEmitter },
    backToHomeSelect: { control: argEventEmitter },
  },
};
export default meta;

type Story = StoryObj<DsfrResponseComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Erreur 500 avec contact'),
  args: {},
};

export const Error502WithoutContact: Story = {
  args: {
    error: 502,
    showContact: false,
  },

  decorators: dsfrDecorator('Erreur 502 sans contact'),
};

export const Error404Full: Story = {
  args: {
    heading: 'Page non trouvée',
    error: 404,
    description: 'La page que vous cherchez est introuvable. Excusez-nous pour la gène occasionnée.',
    detail:
      "Si vous avez tapé l'adresse web dans le navigateur, vérifiez qu'elle est correcte. La page n’est peut-être plus disponible. Dans ce cas, pour continuer votre visite vous pouvez consulter notre page d’accueil. Sinon contactez-nous pour que l’on puisse vous rediriger vers la bonne information.",
    showBackToHome: true,
  },

  decorators: dsfrDecorator('Erreur 404 full'),
};
