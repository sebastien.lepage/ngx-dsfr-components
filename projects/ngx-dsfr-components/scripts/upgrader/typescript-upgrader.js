const { RULES } = require('./typescript-rules');

function TypescriptUpgrader() {
  /* --- Contenu TypeScript ----------------------------------------------------------------------------------------- */
  this.upgradeContent = (content) => {
    let result = content;
    result = upgradeRules(result, RULES.TYPES, replaceType);
    result = upgradeRules(result, RULES.PROPERTIES, replaceProperties);
    return result;
  };

  function upgradeRules(content, rules, fnUpgrade) {
    let result = content;
    rules.forEach((rule) => {
      result = fnUpgrade(result, rule);
    });
    return result;
  }

  /* --- Type TypeScript -------------------------------------------------------------------------------------- */
  /**
   * Remplace toutes les occurrences du type (mot entier) rule[1] par rule[2]
   */
  function replaceType(content, rule) {
    const generic = '([\\s:<])(TYPE)([=\\s\\[>])';
    const regexpStr = generic.replace('TYPE', rule[1]);
    const regexp = new RegExp(regexpStr, 'sg');
    return content.replaceAll(regexp, `$1${rule[2]}$3`);
  }

  /* --- Propriété JSON -------------------------------------------------------------------------------------- */
  /**
   * Remplace toutes les occurrences de la propriété rule[2] du type rule[1] par rule[3]
   */
  function replaceProperties(content, rule) {
    function fnProperty(match) {
      return replaceProperty(match, rule[2], rule[3]);
    }

    const generic = 'TYPE[^=]*?[^;]*?;';
    const regexpStr = generic.replace('TYPE', rule[1]);
    const regexp = new RegExp(regexpStr, 'sg');
    return content.replaceAll(regexp, fnProperty);
  }

  function replaceProperty(content, property, newName) {
    const generic = `([{\s,"']*?)(PROPERTY)(:.*?})`;
    const regexpStr = generic.replace('PROPERTY', property);
    const regexp = new RegExp(regexpStr, 'sg');
    return content.replaceAll(regexp, `$1${newName}$3`);
  }

  /* --- Tests ------------------------------------------------------------------------------------------------------ */
  this.testReplaceType = (content, rule) => {
    return replaceType(content, rule);
  };
  this.testReplaceProperties = (content, rule) => {
    return replaceProperties(content, rule);
  };
  this.testReplaceProperty = (jsonString, property, newName) => {
    return replaceProperty(jsonString, property, newName);
  };
}

module.exports = { TypescriptUpgrader };
