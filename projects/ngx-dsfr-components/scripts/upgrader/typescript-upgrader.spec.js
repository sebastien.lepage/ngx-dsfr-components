const { beforeAll } = require('@jest/globals');
const { TypescriptUpgrader } = require('./typescript-upgrader');

describe('TypescriptUpgrader', () => {
  let upgrader;

  beforeAll(() => {
    upgrader = new TypescriptUpgrader();
  });

  /* --- Expression ------------------------------------------------------------------------------------------------- */
  test('ReplaceType', () => {
    const actual = `  public tags2: Array<Tag> = [>`;
    const expected = actual.replace('Tag', 'DsfrTag');
    let received = upgrader.testReplaceType(actual, [1.0, `Tag`, 'DsfrTag']);
    received = upgrader.testReplaceType(received, [1.0, `Tag`, 'DsfrTag']);
    expect(received).toBe(expected);
  });

  test('ReplaceType2', () => {
    const actual = `import { DownloadComponent } from "./download/download.component";`;
    const expected = actual;
    const received = upgrader.testReplaceType(actual, [1.0, `Download`, 'DsfrDownload']);
    expect(received).toBe(expected);
  });

  /* --- Propriété JSON -------------------------------------------------------------------------------------- */
  test('replaceProperties', () => {
    const actual = `links : DsfrLink[] = [
            { texte: "value 1", href: "#"},
            { texte: "value 2", href: "#"}
          ];`;
    const expected = actual.replaceAll('texte', 'label');
    const received = upgrader.testReplaceProperties(actual, [1.0, 'DsfrLink', 'texte', 'label']);
    expect(received).toBe(expected);
  });

  test('replaceProperty', () => {
    const actual = `links : DsfrLink[] = [
            { texte: "value 1", href: "#"},
            { texte: "value 2", href: "#"}
          ];`;
    const expected = actual.replaceAll('texte', 'label');
    const received = upgrader.testReplaceProperty(actual, 'texte', 'label');
    expect(received).toBe(expected);
  });
});
