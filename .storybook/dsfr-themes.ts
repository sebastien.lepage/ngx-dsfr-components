/** Themes issus du DSFR, pour l'instant uniquement utilisés par les stories. */

/* -- BADGE ---------------------------------------------------------------- */
export const DsfrBadgeClasses = [
  'fr-badge--beige-gris-galet',
  'fr-badge--blue-cumulus',
  'fr-badge--blue-ecume',
  'fr-badge--brown-cafe-creme',
  'fr-badge--brown-caramel',
  'fr-badge--brown-opera',
  'fr-badge--green-archipel',
  'fr-badge--green-bourgeon',
  'fr-badge--green-menthe',
  'fr-badge--green-tilleul-verveine',
  'fr-badge--green-emeraude',
  'fr-badge--orange-terre-battue',
  'fr-badge--pink-macaron',
  'fr-badge--pink-tuile',
  'fr-badge--purple-glycine',
  'fr-badge--yellow-tournesol',
  'fr-badge--yellow-moutarde',
];

/* -- HIGHLIGHT ------------------------------------------------------------ */
export const DSFR_HIGHLIGHT = {
  BEIGE_GRIS_GALET: 'fr-highlight--beige-gris-galet',
  BLUE_CUMULUS: 'fr-highlight--blue-cumulus',
  BLUE_ECUME: 'fr-highlight--blue-ecume',
  BROWN_CAFE_CREME: 'fr-highlight--brown-cafe-creme',
  BROWN_CARAMEL: 'fr-highlight--brown-caramel',
  BROWN_OPERA: 'fr-highlight--brown-opera',
  GREEN_ARCHIPEL: 'fr-highlight--green-archipel',
  GREEN_MENTHE: 'fr-highlight--green-menthe',
  ORANGE_TERRE_BATTUE: 'fr-highlight--orange-terre-battue',
  PINK_MACARON: 'fr-highlight--pink-macaron',
  PINK_TUILE: 'fr-highlight--pink-tuile',
  PURPLE_GLYCINE: 'fr-highlight--purple-glycine',
  YELLOW_MOUTARDE: 'fr-highlight--yellow-moutarde',
  YELLOW_TOURNESOL: 'fr-highlight--yellow-tournesol',
};

/* -- TABLE ---------------------------------------------------------------- */
export const DSFR_TABLE = {
  GREEN_TILLEUL_VERVEINE: 'fr-table--green-tilleul-verveine',
  GREEN_BOURGEON: 'fr-table--green-bourgeon',
  GREEN_EMERAUDE: 'fr-table--green-emeraude',
  GREEN_MENTHE: 'fr-table--green-menthe',
  GREEN_ARCHIPEL: 'fr-table--green-archipel',
  BLUE_ECUME: 'fr-table--blue-ecume',
  BLUE_CUMULUS: 'fr-table--blue-cumulus',
  PURPLE_GLYCINE: 'fr-table--purple-glycine',
  PINK_MACARON: 'fr-table--pink-macaron',
  PINK_TUILE: 'fr-table--pink-tuile',
  YELLOW_TOURNESOL: 'fr-table--yellow-tournesol',
  YELLOW_MOUTARDE: 'fr-table--yellow-moutarde',
  TERRE_BATTUE: 'fr-table--orange-terre-battue',
  CAFE_CREME: 'fr-table--brown-cafe-creme',
  BROWN_CARAMEL: 'fr-table--brown-caramel',
  BROWN_OPERA: 'fr-table--brown-opera',
  GRIS_GALET: 'fr-table--beige-gris-galet',
};

/* -- TAG ----------------------------------------------------------------- */
export const DSFR_TAG = {
  GREEN_TILLEUL_VERVEINE: 'fr-tag--green-tilleul-verveine',
  GREEN_BOURGEON: 'fr-tag--green-bourgeon',
  GREEN_EMERAUDE: 'fr-tag--green-emeraude',
  GREEN_MENTHE: 'fr-tag--green-menthe',
  GREEN_ARCHIPEL: 'fr-tag--green-archipel',
  BLUE_ECUME: 'fr-tag--blue-ecume',
  BLUE_CUMULUS: 'fr-tag--blue-cumulus',
  PURPLE_GLYCINE: 'fr-tag--purple-glycine',
  PINK_MACARON: 'fr-tag--pink-macaron',
  PINK_TUILE: 'fr-tag--pink-tuile',
  YELLOW_TOURNESOL: 'fr-tag--yellow-tournesol',
  YELLOW_MOUTARDE: 'fr-tag--yellow-moutarde',
  ORANGE_TERRE_BATTUE: 'fr-tag--orange-terre-battue',
  BROWN_CAFE_CREME: 'fr-tag--brown-cafe-creme',
  BROWN_CARAMEL: 'fr-tag--brown-caramel',
  BROWN_OPERA: 'fr-tag--brown-opera',
  BEIGE_GRIS_GALET: 'fr-tag--beige-gris-galet',
};

/* -- CALLOUT ------------------------------------------------------------- */
export const DSFR_CALLOUT = {
  GREEN_TILLEUL_VERVEINE: 'fr-callout--green-tilleul-verveine',
  GREEN_BOURGEON: 'fr-callout--green-bourgeon',
  GREEN_EMERAUDE: 'fr-callout--green-emeraude',
  GREEN_MENTHE: 'fr-callout--green-menthe',
  GREEN_ARCHIPEL: 'fr-callout--green-archipel',
  BLUE_ECUME: 'fr-callout--blue-ecume',
  BLUE_CUMULUS: 'fr-callout--blue-cumulus',
  PURPLE_GLYCINE: 'fr-callout--purple-glycine',
  PINK_MACARON: 'fr-callout--pink-macaron',
  PINK_TUILE: 'fr-callout--pink-tuile',
  YELLOW_TOURNESOL: 'fr-callout--yellow-tournesol',
  YELLOW_MOUTARDE: 'fr-callout--yellow-moutarde',
  ORANGE_TERRE_BATTUE: 'fr-callout--orange-terre-battue',
  BROWN_CAFE_CREME: 'fr-callout--brown-cafe-creme',
  BROWN_CARAMEL: 'fr-callout--brown-caramel',
  BROWN_OPERA: 'fr-callout--brown-opera',
  BEIGE_GRIS_GALET: 'fr-callout--beige-gris-galet',
};
