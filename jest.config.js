module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
  modulePathIgnorePatterns: ['<rootDir>/dist/'],
  coverageReporters: ['json', 'html'],
  transformIgnorePatterns: [
    'node_modules/(?!(@sco|@lit|lit|lit-element|lit-html|ngx-cookie|uuid|text-zoom-event|ngx-logger|@a11y|@angular|primeng))',
  ],
};
