// https://pakstech.com/blog/npx-script/
// https://medium.com/netscape/a-guide-to-create-a-nodejs-command-line-package-c2166ad0452e
const argsParser = require('args-parser');
const { dsfrAnalyzerMain } = require('./dsfr-analyzer/main');

function execute() {
  // https://www.npmjs.com/package/args-parser
  const args = argsParser(process.argv);
  // console.log('args: ' + JSON.stringify(args));
  dsfrAnalyzerMain(args);
}

module.exports = { execute };
