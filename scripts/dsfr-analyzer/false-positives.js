// les classes ci-dessous sont utilisées dans le stemplates DSFR mais n'existent pas dans leur code
const FALSE_POSITIVES = [
  // consent @since 1.9.3
  'fr-consent-banner__content',
  // address
  'fr-fieldset__element--inline@md',
  // login
  'fr-background-alt--grey',
  // footer @since 1.9.3
  'fr-grid-row--start',
  // footer @since 1.9.3
  'fr-footer__content-item',
  // header @since 1.9.3
  'fr-header__body',
  // tile (classe existante côté DSFR mais non extraite par le parseur react-dsfr)
  'fr-tile--vertical@md',
  // tile (classe existante côté DSFR mais non extraite par le parseur react-dsfr)
  'fr-tile--vertical@lg',
  // consent-manager @since 1.9.3
  'fr-consent-manager',
  // display @since 1.11
  'fr-display',
];

module.exports = { FALSE_POSITIVES };
